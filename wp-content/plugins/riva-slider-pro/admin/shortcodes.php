<?php

/*
  Shortcode functionality
*/

function riva_slider_pro_shortcode( $atts ) {
  
  extract( shortcode_atts( array(
    "id" => ''
  ), $atts ) );
  
  ob_start();
  riva_slider_pro( $id );
  $output = ob_get_contents();
  ob_end_clean();
  
  return $output;

}

//add_shortcode( 'slideshow', 'riva_slider_pro_shortcode' );
add_shortcode( 'rivasliderpro', 'riva_slider_pro_shortcode' );

?>