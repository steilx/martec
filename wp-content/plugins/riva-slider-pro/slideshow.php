<?php

/*
  This function contains the slideshow's physical output.
  It is crucial to the plugin and advisable that you do not edit it (unless you know what you're doing)
*/

function riva_slider_pro( $id = 1 ) {
  
  global $rs_pro_loaded, $rs_pro_scripts;

  // Get some plugin info  
  $info = riva_slider_pro_info();
  
  // And the slideshow(s) and global settings
  $slideshows = get_option( $info[ 'shortname' ] .'_slideshows' );
  $settings = get_option( $info[ 'shortname' ] .'_global_settings' );
  
  // Firstly check to see if the slideshow exists
  $slideshow_exists = riva_slider_pro_search( $slideshows, 'index', $id, 'boolean' );
  
  if ( $slideshow_exists == true ) {
    
    $slideshow = riva_slider_pro_search( $slideshows, 'index', $id, 'array' );
    
    // If user is using images from posts
    if ( $slideshow[ 'image-source' ] == 'images_from_posts' ) {
      
      $i = 0;
      
      $slideshow[ 'images' ] = array();
      $image_category = explode( ', ', $slideshow[ 'image-category' ] );
      
      if ( $image_category == '0' ) {
	$query = array(
	  'post_type' => 'post'
	);
      } else {
	$query = array(
	  'post_type' => 'post',
	  'cat' => $image_category[ 0 ]
	);
      }
      
      $posts = new WP_Query( $query );
      
      while ( $posts -> have_posts() ) : $posts -> the_post();
	global $post;
	$image = get_post_meta( $post -> ID, '_rivasliderpro', true );
	$i++;
	if ( $i <= $slideshow[ 'max-images' ] ) {
	  if ( isset( $image[ 'get_post_info' ] ) && $image[ 'get_post_info' ] == 'true' ) {
	    $image[ 'content-title' ] = get_the_title();
	    $image[ 'content-text' ] = get_the_excerpt();
	    $image[ 'image-link' ] = 'webpage';
	    $image[ 'webpage-url' ] = get_permalink();
	  }
	  if ( $image[ 'image-url' ] != '' )
	    $slideshow[ 'images' ] = array_merge( $slideshow[ 'images' ], array( $image ) );
	}
      endwhile;
      wp_reset_query();
    }
  
    // Checking if user wants images to be randomized
    if ( isset( $slideshow ) && $slideshow[ 'random_order' ] == 'true' )
      shuffle( $slideshow[ 'images' ] );
  
    // Display the slideshow
    if ( count( $slideshow[ 'images' ] ) > 0 ) {
	
      // Icon navigation index
      if ( $slideshow[ 'control_nav' ] == 'enable' )
	$index = 0;
      
      // Random number variable so slideshow don't get mixed up
      $rand = rand( 0, 10000 );
      
      // Load plugin scripts is not already loaded
      if ( function_exists( 'riva_slider_pro_scripts' ) )
	riva_slider_pro_scripts();
      
      // Load this slideshows function onto the page
      if ( function_exists( 'riva_slider_pro_slideshow_script' ) )
	$rs_pro_scripts .= riva_slider_pro_slideshow_script( $id, $slideshow, $rand );
      
      ?>
      
      <div id="riva-slider-<?php echo esc_attr( $id ); ?>-shell">
      
      <!--[if lte IE 7]>
      <div class="slider-id-<?php echo esc_attr( $id ); ?> rs-ie7 riva-slider-holder" id="riva-slider-<?php echo esc_attr( $id ); ?>-<?php echo esc_attr( $rand ); ?>">
      <![endif]-->
      <!--[if IE 8]>
      <div class="slider-id-<?php echo esc_attr( $id ); ?> rs-ie8 riva-slider-holder" id="riva-slider-<?php echo esc_attr( $id ); ?>-<?php echo esc_attr( $rand ); ?>">
      <![endif]-->
      <!--[if IE 9]>
      <div class="slider-id-<?php echo esc_attr( $id ); ?> rs-ie9 riva-slider-holder" id="riva-slider-<?php echo esc_attr( $id ); ?>-<?php echo esc_attr( $rand ); ?>">
      <![endif]-->
      <!--[if !IE]><!-->
      <div class="slider-id-<?php echo esc_attr( $id ); ?> riva-slider-holder" id="riva-slider-<?php echo esc_attr( $id ); ?>-<?php echo esc_attr( $rand ); ?>">
      <!--<![endif]-->
      
      <?php if ( $slideshow[ 'preload' ] != 'false') { ?><div class="riva-slider-preload"></div><?php } ?>
	
	<ul class="riva-slider">
	<?php foreach ( $slideshow[ 'images' ] as $image ) {
    
	  // Text & Title container class
	  if ( $image[ 'content-title' ] && $image[ 'content-text' ] )
	    $text_class = 'both';
	  elseif ( $image[ 'content-title' ] && empty( $image[ 'content-text' ] ) )
	    $text_class = 'title-only';
	  elseif ( empty( $image[ 'content-title' ] ) && $image[ 'content-text' ] )
	    $text_class = 'text-only';
	  
	  // Determine video type and URL
	  if ( $image[ 'image-link' ] == 'video' && $image[ 'video-url' ] != '' ) {
	    $image[ 'video-url' ] = riva_slider_pro_video_url( $image[ 'video-url' ], $image[ 'related-videos' ], $image[ 'autoplay-video' ] );
	  } ?>
	  
	  <li class="<?php if ( $image[ 'image-link' ] == 'video' && $image[ 'video-url' ] != '' ) echo 'rs-video '; echo 'rs-'. esc_attr( str_replace( '_', '-', $image[ 'play-button' ] ) ); ?>">
	    
	    <?php if ( ( isset( $image[ 'image-link' ] ) && $image[ 'image-link' ] == 'webpage' && isset( $image[ 'webpage-url' ] ) && $image[ 'webpage-url' ] != '' ) || ( isset( $image[ 'image-link' ] ) && $image[ 'image-link' ] == 'video' && isset( $image[ 'video-url' ] ) && $image[ 'video-url' ] != '' ) ) { ?><a href="<?php if ( $image[ 'image-link' ] == 'webpage' ) { echo esc_attr( $image[ 'webpage-url' ] ); } elseif ( $image[ 'image-link' ] == 'video' ) { echo esc_attr( $image[ 'video-url' ] ); } ?>" target="_<?php echo esc_attr( $image[ 'link-target' ] ); ?>"><?php } ?>
	    <img src="<?php echo esc_url_raw( riva_slider_pro_image( null, $image[ 'image-url' ], $slideshow[ 'width' ], $slideshow[ 'height' ] ) ); ?>" <?php if ( $settings[ 'resize' ] == 'true' ) { ?>width="<?php echo esc_attr( $slideshow[ 'width' ] ); ?>" height="<?php echo esc_attr( $slideshow[ 'height' ] ); ?>"<?php } ?> alt="<?php if ( $image[ 'image-alt' ] != '' ) echo esc_attr( $image[ 'image-alt' ] ); ?>" title="<?php if ( $image[ 'image-title' ] != '' ) echo esc_attr( $image[ 'image-title' ] ); ?>" class="rs-image"/>
	    <?php if ( ( isset( $image[ 'image-link' ] ) && $image[ 'image-link' ] == 'webpage' && isset( $image[ 'webpage-url' ] ) && $image[ 'webpage-url' ] != '' ) || ( isset( $image[ 'image-link' ] ) && $image[ 'image-link' ] == 'video' && isset( $image[ 'video-url' ] ) && $image[ 'video-url' ] != '' ) ) { ?></a><?php } ?>
	  
	    <?php if ( ( isset( $image[ 'content-title' ] ) && $image[ 'content-title' ] != '' ) || ( isset( $image[ 'content-text' ] ) &&  $image[ 'content-text' ] != '' ) ) { ?>
	    <div class="rs-content rs-<?php echo esc_attr( $text_class ); ?> rs-text-<?php echo esc_attr( $image[ 'content-position' ] ); ?>">
	      <div class="rs-content-holder" style="background-color: #<?php echo esc_attr( $image[ 'content-bg-colour' ] ); ?>; opacity: <?php if ( $image[ 'content-bg-opacity' ] == '100' ) echo '1'; else echo '0.'.esc_attr( $image[ 'content-bg-opacity' ] ); ?>; filter: alpha(opacity=<?php echo esc_attr( $image[ 'content-bg-opacity' ] ); ?>);">
		<?php if ( isset( $image[ 'content-title' ] ) && $image[ 'content-title' ] != '' ) { ?><h2 style="color: #<?php echo esc_attr( $image[ 'content-text-colour' ] ); ?>;"><?php echo esc_html( $image[ 'content-title' ] ); ?></h2><?php } ?>
		<?php if ( isset( $image[ 'content-text' ] ) && $image[ 'content-text' ] != '' ) { ?><span style="color: #<?php echo esc_attr( $image[ 'content-text-colour' ] ); ?>;"><?php echo str_replace( '\\', '', $image[ 'content-text' ] ); ?></span><?php } ?>
	      </div>
	    </div>
	    <?php } ?>
	    
	  </li>
	  
	<?php } ?>
	</ul>
	
	<?php if ( $slideshow[ 'control_nav' ] == 'enable' ) { ?>
	<ul class="rs-control-nav">
	  <?php foreach ( $slideshow[ 'images' ] as $image ) { $index++; ?>
	  <li class="<?php if ( $slideshow[ 'control_nav_index' ] == 'icons' ) { echo 'rs-icons'; } elseif ( $slideshow[ 'control_nav_index' ] == 'numbers' ) { echo 'rs-numbers'; } elseif ( $slideshow[ 'control_nav_index' ] == 'thumbnails' ) { echo 'rs-thumbnails'; } ?>" <?php if ( $slideshow[ 'control_nav_index' ] == 'thumbnails' ) { echo 'style="background: url('. esc_url_raw( riva_slider_pro_image( null, $image[ 'image-url' ], $slideshow[ 'thumb_width' ], $slideshow[ 'thumb_height' ] ) ) .') top left no-repeat !important;"'; } ?>>
	    <?php if ( $slideshow[ 'control_nav_index' ] == 'numbers' ) { ?><?php echo $index; ?><?php } ?>
	  </li>
	  <?php } ?>
	</ul>
	<?php } ?>
	
	<?php if ( $slideshow[ 'pause_button' ] == 'enable' ) { ?>
	<div class="rs-pause-button<?php if ( esc_attr( $slideshow[ 'pause_button_hover' ] ) == 'false' ) echo ' no-hide'; ?>"></div>
	<?php } ?>
	
	<?php if ( $slideshow[ 'direction_nav' ] == 'enable' ) { ?>
	<div class="rs-next <?php echo esc_attr( $slideshow[ 'direction_nav_pos' ] ); if ( esc_attr( $slideshow[ 'direction_nav_hover' ] ) == 'false' ) echo ' no-hide'; ?>"></div>
	<div class="rs-prev <?php echo esc_attr( $slideshow[ 'direction_nav_pos' ] ); if ( esc_attr( $slideshow[ 'direction_nav_hover' ] ) == 'false' ) echo ' no-hide'; ?>"></div>
	<?php } ?>
	
      </div>
      
      <?php if ( ( $slideshow[ 'skin' ] != 'custom' && $slideshow[ 'shadow' ] != 'disable' ) || ( $slideshow[ 'skin' ] == 'custom' && $slideshow[ 'shadow_image' ] != '' && $slideshow[ 'shadow' ] != 'disable' ) ) { ?>
      <div class="slider-shadow-<?php echo esc_attr( $id ); ?>"></div>
      <?php } ?>
      
    </div>
      
    <?php } elseif ( count( $slideshow[ 'images' ] ) == 0 ) { ?>
      <div><strong><?php _e( 'This slideshow has no images set to it.', 'riva_slider_pro' ); ?></strong></div>
    <?php }
    
  } else { ?>
    <div><strong><?php _e( 'The slideshow you have selected does not exist (ID: '. $id .'). Please try a slideshow with a different ID.', 'riva_slider_pro' ); ?></strong></div>
  <?php }

}  

?>