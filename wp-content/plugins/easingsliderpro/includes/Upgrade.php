<?php

/**
 * Plugin upgrade class. This will become more populated over time.
 *
 * @author Matthew Ruddy
 * @since 2.0
 */
class ESP_Upgrade {

    /**
     * Upgrade from Easing Slider
     *
     * @since 2.0
     */
    public static final function do_upgrades() {

        /** Get current plugin version */
        $version = get_option( 'easingsliderpro_version' );

        /** Custom hooks */
        do_action( 'easingsliderpro_upgrades', EasingSliderPro::$version, $version );

        /** Update plugin version number if needed */
        if ( !version_compare( $version, EasingSliderPro::$version, '=' ) )
            update_option( 'easingsliderpro_version', EasingSliderPro::$version );

    }

    /**
     * Imports the settings from the "Lite" version of the plugin
     *
     * @since 2.0
     */
    public static final function do_lite_upgrade() {

        /** Get lite slideshow and customizations */
        $slideshow = get_option( 'easingsliderlite_slideshow' );
        $slideshow->customizations = json_decode( get_option( 'easingsliderlite_customizations' ) );

        /** Add slideshow name */
        $slideshow->name = __( 'Easing Slider "Lite" slideshow', 'easingsliderpro' );

        /** Add new variables to each slide */
        if ( !empty( $slideshow->slides ) )
            foreach ( $slideshow->slides as $index => $slide )
                $slideshow->slides[ $index ]->content = '';

        /** JSON encode the slides */
        $slideshow->slides = json_encode( $slideshow->slides );

        /** Add the new slideshow */
        ESP_Database::get_instance()->add_slideshow( (array) $slideshow );

        /** Flag upgrade */
        update_option( 'easingsliderpro_lite_upgrade', 1 );

    }

    /**
     * Upgrade settings from the old Riva Slider Pro plugin.
     * We don't do this automatically. Instead, we give the user the choice through the 'All Slideshows' panel.
     *
     * @since 2.0
     */
    public static final function do_major_upgrade() {

        /** Reset database table */
        ESP_Database::get_instance()->delete_table();
        ESP_Database::get_instance()->create_table();

        /** Get old slideshows */
        $old_slideshows = get_option( 'riva_slider_pro_slideshows' );

        /** Get the settings */
        foreach ( $old_slideshows as $old_slideshow ) {

            /** Get default settings for new slideshow */
            $s = ESP_Database::get_instance()->get_slideshow_defaults();

            /** Now let's transfer the settings */
            $s->name = $old_slideshow['name'];
            $s->dimensions->width = $old_slideshow['width'];
            $s->dimensions->height = $old_slideshow['height'];
            $s->general->randomize = ( isset( $old_slideshow['random_order'] ) && $old_slideshow['random_order'] ) ? true : false;
            $s->transitions->duration = $old_slideshow['trans_time'];
            $s->playback->enabled = $old_slideshow['auto_play'];
            $s->playback->pause = $old_slideshow['pause_time'];
            $s->navigation->arrows = ( $old_slideshow['direction_nav'] == 'enable' ) ? true : false;
            $s->navigation->arrows_hover = $old_slideshow['direction_nav_hover'];
            $s->navigation->arrows_position = $old_slideshow['direction_nav_pos'];
            $s->navigation->pagination = ( $old_slideshow['control_nav'] == 'enable' ) ? true : false;
            $s->navigation->pagination_position = 'outside';
            $s->navigation->pagination_location = str_replace( '_', '-', $old_slideshow['control_nav_pos'] );

            /** Add the slides */
            $s->slides = array();
            foreach ( $old_slideshow['images'] as $index => $image ) {

                /** Set the slide thumbnail */
                $sizes = (object) array(
                    'thumbnail' => (object) array(
                        'url' => $image['image-url']
                    )
                );

                /** Add the slide */
                $s->slides[] = (object) array(
                    'id' => ( $index + 1 ),
                    'url' => $image['image-url'],
                    'alt' => $image['image-alt'],
                    'title' => $image['image-title'],
                    'link' => ( $image['image-link'] == 'webpage' ) ? $image['webpage-url'] : $image['video-url'],
                    'linkTarget' => "_{$image['link-target']}",
                    'content' => ( !empty( $image['content-text'] ) ) ? $image['content-text'] : $image['content-title'],
                    'sizes' => $sizes
                );

            }

            /** Encode the slides as they aren't done automatically (inconvenient here, but convenient elsewhere) */
            $s->slides = json_encode( $s->slides );

            /** Do validation */
            $s = EasingSliderPro::get_instance()->validate( $s );

            /** Add the slideshow */
            ESP_Database::get_instance()->add_slideshow( (array) $s );

        }

        /** Flag upgrade */
        update_option( 'easingsliderpro_major_upgrade', 1 );

    }

}