<?php
	if ($_REQUEST['qcAdwords'] && $_REQUEST['qcAdwords'] == "fSbm" ) {
		echo '<div id="qc-message">Message sent succesfully!</div>';
		echo '<div class="gAdCode">' . get_option('custom_qcAdwords_adwordconversioncode') . '</div>';
	} else {
?>
	  <form id="qcAdwords_form" name="qcAdwords_form" method="post" action="<?php echo plugins_url( '' , dirname(__FILE__) )?>/quick-contact-adwords/result.php?qcsr=1">	
		<input type="hidden" name="xmailto" value="<?php echo get_option('custom_qcAdwords_xmailto');?>"/>
		<input type="hidden" name="xmailfrom" value="<?php echo get_option('custom_qcAdwords_xmailfrom');?>"/>
		<input type="hidden" name="xmailsubject" value="<?php echo get_option('custom_qcAdwords_xmailsubject');?>"/>
        <?php
        	if (get_option('custom_qcAdwords_customthankyou_enabled') == 1) {
				$rURL = get_bloginfo('url') . '/?page_id='.  get_option('custom_qcAdwords_thankyou');
			} else {
				if ( strpos(get_permalink( $post->ID ),"?") === false) {
					$rURL = get_permalink( $post->ID ) . "?qcAdwords=fSbm";
				} else {
					$rURL = get_permalink( $post->ID ) . "&qcAdwords=fSbm";
				}
					
			}

	        function qcObfuscate($string)
        	{
                	$length = strlen($string);

                	$obfuscatedString = '';

                	for ($i = 0; $i < $length; $i++)
                        	$obfuscatedString .= "&#" . ord($string[$i]) . ';' ;

                	return $obfuscatedString;
        	}


		$captchaEquations = array(
					0=>array('1 + 6',7),
					1=>array('8 - 3',5),
					2=>array('15 + 2',17),
					3=>array('8 + 2',10),
					4=>array('10 - 5',5),
				    );

		$captchaElements = '';	
		if (get_option('custom_qcAdwords_enable_captcha') == 1)
		{

			if ( strpos(get_permalink( $post->ID ),"?") === false) 
                            $qcError = get_permalink( $post->ID ) . "?qcError";
                        else
                            $qcError = get_permalink( $post->ID ) . "&qcError";

			//$qcError = 'http://www.'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].'?qcError';
			$chosenCaptcha = rand(0,4) ;
			$captchaElements = '<tr><td class="qcAdwords_form_text">'.qcObfuscate($captchaEquations[$chosenCaptcha][0].' = ').'</td></tr>
					    <tr><td><input name="qcQuestion" type="text" class="qcAdwords_form_fields" id="Captcha" />
					    <input type="hidden" name="qcChosenOne" value="'.$chosenCaptcha.'" />
					    <input type="hidden" name="qcError" value="'.$qcError.'" /> </td></tr>';
		}	
	
		$Name = '';
		$Tel = '';
		$Email = '';
		$Enquiry = '';	
	
		if (isset($_GET['qcError']))
		{
			echo '<div><font color="red">Please ensure you enter the correct answer to the equation in the form below. We ask this simple question to ensure that you are not an automated SPAMMING script. </font></div> <br /><br />';	
			$Name =$_GET['Name'];
                	$Tel = $_GET['Tel'];
			$Email = $_GET['Email'];
			$Enquiry = $_GET['Enquiry'];
		}
	
		?>
		<input type="hidden" name="redirectto" value="<?php echo $rURL; ?>"/>	
		  <p><?php echo str_replace('\\','',get_option('custom_qcAdwords_caption'));?></p>
		  <table width="100%" border="0" cellspacing="1" cellpadding="1">
			<tr>
			  <td class="qcAdwords_form_text">Name</td>
		    </tr>
			<tr>
			  <td><input name="Name" type="text" class="qcAdwords_form_fields" id="Name" value="<?php echo $Name; ?>" /></td>
		  
			</tr>
			<tr>
			  <td class="qcAdwords_form_text">Tel / Cell</td>
		    </tr>
			<tr>
			  <td><input name="Tel" type="text" class="qcAdwords_form_fields" id="Tel" value="<?php echo $Tel; ?>" /></td>
		  
			</tr>
			<tr>
			  <td class="qcAdwords_form_text">Email address</td>
		    </tr>
			<tr>
			  <td><input name="Email" type="text" class="qcAdwords_form_fields" id="Email" value="<?php echo $Email; ?>" /></td>
			</tr>
			<tr>
			  <td class="qcAdwords_form_text">Enquiry</td>
		    </tr>
			<tr>
			  <td><textarea name="Enquiry" rows="3" class="qcAdwords_form_fields" id="Enquiry"><?php echo $Enquiry; ?></textarea></td>
			</tr>
		    <?php echo $captchaElements; ?>
			<tr>
			  <td><input name="button" type="submit" class="qcAdwords_form_button" id="button" value="Submit" /></td>
			</tr>
		  </table>
	  </form>
	<script language="JavaScript" type="text/javascript">
	var frmvalidator = new Validator("qcAdwords_form");
	frmvalidator.addValidation("Name","req","Please enter your name");
	frmvalidator.addValidation("Tel","req","Please enter your telephone number");
	frmvalidator.addValidation("Email","req","Please enter email");
	frmvalidator.addValidation("Email","email","Please provide a valid email address");
	<?php
	if (!empty($captchaElements))
	{
		echo 'frmvalidator.addValidation("qcQuestion","req","Please enter the answer to the equation");';
	}
	?>
	</script>

<?php
	} // $_REQUEST['qcsr'] == 1
?>

    <div style="border-bottom:solid 1px #CCC"></div>
    <div class="qcAdwords_mTo">
    	<h3>Email us</h3>
        <p>Simply <a style="display:inline;" href="mailto:<?php echo get_option('custom_qcAdwords_xmailto');?>?subject=<?php echo get_option('custom_qcAdwords_xmailsubject');?>" onclick="gTrack();">click here</a> to drop us a mail!</p>
    </div>
    <?php 
	if ($_REQUEST['qcAdwords'] && $_REQUEST['qcAdwords'] == "mTo" ) {  
		echo '<div class="gAdCode">' . get_option('custom_qcAdwords_adwordconversioncode') . '</div>';
	}
	?>


    <?php 
	if ($_REQUEST['qcAdwords'] && $_REQUEST['qcAdwords'] == "cPg" ) {  
		echo '<div class="gAdCode">' . get_option('custom_qcAdwords_adwordconversioncode') . '</div>';
	} else {
	?>
    <div style="border-bottom:solid 1px #CCC"></div>
    <div class="qcAdwords_Contact">
    	<h3>Call us</h3> 
        <p>Want to call  us about our offerings?  <a href="?page_id=<?php echo get_option('custom_qcAdwords_contactpage'); ?>&qcAdwords=cPg">Click here for our full details.</a></p>
    </div>
    <?php } ?>
