<?php
/*
Plugin Name: Quick Contact with AdWords
Version: 1.0
Description: Create a widget that has a quick contact form with Conversion tracking, mailto tracking and contact page tracking.
Author: Paul Combrink (Interface)
Author URI: www.interface.co.za
*/
function qcAdwords_headaction()
{
	echo '<script type="text/javascript" src="'.get_bloginfo('wpurl').'/'.PLUGINDIR.'/'.dirname( plugin_basename(__FILE__) ).'/gen_validatorv31.js"></script>';	
	echo '<link rel="stylesheet" id="quick-contact-css"  href="'.get_bloginfo('wpurl').'/'.PLUGINDIR.'/'.dirname( plugin_basename(__FILE__) ).'/qcAdwords_style.css" type="text/css" media="all" />';
	echo '<script type="text/javascript"> function gTrack(){ window.setTimeout(function () { location.href="?qcAdwords=mTo" }, 0); } </script>';		
}
 
 

function qcAdwords_control() {
	
	if ($_POST['qc-widget-submit']){
		
		$custom_qcAdwords_title = $_POST['custom_qcAdwords_title'];
		update_option('custom_qcAdwords_title', $custom_qcAdwords_title);	

		$custom_qcAdwords_caption = str_replace('\\','',$_POST['custom_qcAdwords_caption']);
		update_option('custom_qcAdwords_caption', $custom_qcAdwords_caption);			
		
		$custom_qcAdwords_xmailto = $_POST['custom_qcAdwords_xmailto'];
		update_option('custom_qcAdwords_xmailto', $custom_qcAdwords_xmailto);

		$custom_qcAdwords_xmailfrom = $_POST['custom_qcAdwords_xmailfrom'];
		update_option('custom_qcAdwords_xmailfrom', $custom_qcAdwords_xmailfrom);

		$custom_qcAdwords_xmailsubject = $_POST['custom_qcAdwords_xmailsubject'];
		update_option('custom_qcAdwords_xmailsubject', $custom_qcAdwords_xmailsubject);
		
		$custom_qcAdwords_customthankyou_enabled = $_POST['custom_qcAdwords_customthankyou_enabled'];
		update_option('custom_qcAdwords_customthankyou_enabled', $custom_qcAdwords_customthankyou_enabled);			

		$custom_qcAdwords_thankyou = $_POST['custom_qcAdwords_thankyou'];
		update_option('custom_qcAdwords_thankyou', $custom_qcAdwords_thankyou);
		
		$custom_qcAdwords_adwordconversioncode = str_replace('\\','',$_POST['custom_qcAdwords_adwordconversioncode']);
		update_option('custom_qcAdwords_adwordconversioncode', $custom_qcAdwords_adwordconversioncode);
		
		$custom_qcAdwords_contactpage = $_POST['custom_qcAdwords_contactpage'];
		update_option('custom_qcAdwords_contactpage', $custom_qcAdwords_contactpage);		
	
		// custom_qcAdwords_enable_captcha
		$custom_qcAdwords_enable_captcha = $_POST['custom_qcAdwords_enable_captcha'];
                update_option('custom_qcAdwords_enable_captcha', $custom_qcAdwords_enable_captcha);
	
	} // if Quick contact options-submit
?>	
<p>
	<label for="custom_qcAdwords_title"><?php _e('Contact box title:'); ?>
		<input class="widefat" id="custom_qcAdwords_title" name="custom_qcAdwords_title" type="text" value="<?php if (get_option('custom_qcAdwords_title')) { echo get_option('custom_qcAdwords_title'); } else { echo "Get in touch"; } ?>"	/>
	</label>
</p>
<p>
	<label for="custom_qcAdwords_caption"><?php _e('Contact box caption:'); ?>
	    <textarea class="widefat" id="custom_qcAdwords_caption" name="custom_qcAdwords_caption" rows="3"><?php if (get_option('custom_qcAdwords_caption')) { echo get_option('custom_qcAdwords_caption'); } else { echo "Please complete the form below and we'll get in touch."; } ?></textarea>
	</label>
</p>
<p>
	<label for="custom_qcAdwords_xmailto"><?php _e('Client email address:'); ?>
		<input class="widefat" id="custom_qcAdwords_xmailto" name="custom_qcAdwords_xmailto" type="text" value="<?php echo get_option('custom_qcAdwords_xmailto')?>"	/>
	</label>
    (comma seperated)
</p>
<p>
	<label for="custom_qcAdwords_xmailsubject"><?php _e('Subject line:'); ?>
		<input class="widefat" id="custom_qcAdwords_xmailsubject" name="custom_qcAdwords_xmailsubject" type="text" value="<?php if ( get_option('custom_qcAdwords_xmailsubject') ) {  echo get_option('custom_qcAdwords_xmailsubject'); } else { echo bloginfo('name') . ' - Website Response Form';	} ?>"	/>
	</label>
</p>
<p>
	<label for="custom_qcAdwords_xmailfrom"><?php _e('From email:'); ?>    
    <select class="widefat" id="custom_qcAdwords_xmailfrom" name="custom_qcAdwords_xmailfrom">
      <option>-- please select --</option>
      <option <?php if (get_option('custom_qcAdwords_xmailfrom') == "clientservices@iface.co.za") { echo "selected";} ?> value="clientservices@iface.co.za">Search Networx Clients</option>
      <option <?php if (get_option('custom_qcAdwords_xmailfrom') == "clients@interface.co.za") { echo "selected";} ?> value="clients@interface.co.za">Webmail Clients</option>
    </select>
    </label>
</p>
<hr />
<label for="custom_qcAdwords_enable_captcha"><input name="custom_qcAdwords_enable_captcha" type="checkbox" value="1" <?php if (get_option('custom_qcAdwords_enable_captcha') == 1) { echo 'checked="checked"';} ?>" /></label>
<?php _e('Enable Captcha?'); ?>
<hr />
<p>
	<label for="custom_qcAdwords_customthankyou_enabled"><input name="custom_qcAdwords_customthankyou_enabled" type="checkbox" value="1" <?php if (get_option('custom_qcAdwords_customthankyou_enabled') == 1) { echo 'checked="checked"';} ?>" /></label>
  <label for="custom_qcAdwords_thankyou"><?php _e('Custom Thankyou page:'); ?>
	<?php $custom_qcAdwords_thankyou_args = array(
        'depth'            => 0,
        'child_of'         => 0,
        'selected'         => get_option('custom_qcAdwords_thankyou'),
        'echo'             => 1,
        'name'             => 'custom_qcAdwords_thankyou'); ?>
    <?php wp_dropdown_pages( $custom_qcAdwords_thankyou_args ); ?>
    <?php echo 'p_id='.(get_option('custom_qcAdwords_thankyou')); ?> 
	</label>
</p>
<hr>
<p>
  <label for="custom_qcAdwords_contactpage"><?php _e('Contact page:'); ?>
	<?php $custom_qcAdwords_contactpage_args = array(
        'depth'            => 0,
        'child_of'         => 0,
        'selected'         => get_option('custom_qcAdwords_contactpage'),
        'echo'             => 1,
        'name'             => 'custom_qcAdwords_contactpage'); ?>
    <?php wp_dropdown_pages( $custom_qcAdwords_contactpage_args ); ?>
    <?php echo 'p_id='.(get_option('custom_qcAdwords_contactpage')); ?> 
	</label>
</p>
<hr>
<p>
	<label for="custom_qcAdwords_adwordconversioncode"><?php _e('Google Adwords Conversion Code:'); ?>
	    <textarea class="widefat" id="custom_qcAdwords_adwordconversioncode" name="custom_qcAdwords_adwordconversioncode" rows="10"><?php if (get_option('custom_qcAdwords_adwordconversioncode')) { echo get_option('custom_qcAdwords_adwordconversioncode'); } else { echo "Paste the Google AdWords Conversion Code here"; } ?></textarea>
	</label>
</p>

<input type="hidden" id="qc-widget-submit" name="qc-widget-submit" value="1"/>
<?php
}


function qcAdwords_widget($args) {
	extract($args); 	
	echo $before_widget;
	echo $before_title . get_option('custom_qcAdwords_title') . $after_title;	
	include(PLUGINDIR.'/'.dirname( plugin_basename(__FILE__) ).'/qcAdwords_widget.php');
	echo $after_widget;
}


function qcAdwords_init() /* register */
{
	register_sidebar_widget(__('Quick Contact AdWords'),'qcAdwords_widget');
	register_widget_control(__('Quick Contact AdWords'),'qcAdwords_control');
}
add_action("plugins_loaded", "qcAdwords_init");
add_action("wp_head","qcAdwords_headaction");

?>
