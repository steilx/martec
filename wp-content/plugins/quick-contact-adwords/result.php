<?php
// check if form has captcha before anything else
if (isset($_POST['qcChosenOne']))
{
      $captchaEquations = array(
                              0=>array('1 + 6',7),
                              1=>array('8 - 3',5),
                              2=>array('15 + 2',17),
                              3=>array('8 + 2',10),
                              4=>array('10 - 5',5),
                         );

     $qcChosen = $_POST['qcChosenOne'];
     $qcGivenAnswer = (int) trim($_POST['qcQuestion']);
     $qcAnswer = (int) $captchaEquations[$qcChosen][1];
     if ($qcGivenAnswer !==  $qcAnswer)
     {
        header('Location: '.$_POST['qcError'].build_query_get_str());
        exit;
     }
}


$mail = '';
$mailto = '';
$mailfrom = '';
$subject = '';

function build_query_get_str() 
{
   $query_str = '';
   foreach($_POST as $key => $value)
   {
      if (substr($key, 0, 1) <> 'x' && substr($key, 0, 2) <> 'qc' && $key != 'redirectto'){
         $query_str .= '&' . urlencode($key). '='. urlencode($value);
      }
   }
   return $query_str;
}

function strClean($s)
{
        $s = trim($s);
        if(strlen($s)>128) return false;
        $a = explode("\n", $s);
        if(count($a)>1) return false;
        return $s;
}

function strMail($s)
{
        if(substr_count($s, '@')>4) return false;
        return strClean($s);
}


function build_query_str($header=false) 
{
   $query_str = '';
   foreach($_POST as $key => $value)
   {
      if (substr($key, 0, 1) <> "x"){
         $query_str .= ',"' . (($header===false) ? $value : $key) . '"';
      }
   }
   return $query_str;
}

$cansend=true;
foreach($_POST as $key => $value) {

        switch($key) {
                case 'xmailto':
                        $mailto = strMail($value);
                        break;

                case 'xmailsubject':
                        $subject = strClean($value);
                        break;

                case 'xmailfrom':
                        $mailfrom = strMail($value);
                        break;

                case 'PHPSESSID':
                        break;

                default:
                        $mail .= $key.': '.$value."\n";
        }
}


if(!$subject) {
        $subject = 'Form submission';
}

if(!$mailfrom) {
        $mailfrom = 'production@webmail.co.za';
}

$replyTo = trim($_POST['Email']);

if($cansend)
{

	$headers = 'From: <'.$mailfrom.'>' . "\r\n";
	$headers .= 'Reply-To: <'.$replyTo.'>' . "\r\n";

	$rc = mail($mailto, $subject, $mail, $headers, "-f $mailfrom" );

}

if(!empty($_POST["redirectto"])){
        Header('Location: '.$_POST["redirectto"]);
	exit;
}


?><html>

<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<meta name="GENERATOR" content="Microsoft FrontPage 5.0">
<meta name="ProgId" content="FrontPage.Editor.Document">
<title>Thank You For Submitting your Form</title>
<base target="_self">
</head>

<body bgcolor="#FFFFFF">

  <div align="center">
    <center>
    <table border="0" cellpadding="0" cellspacing="0" width="705">
      <tr>
        <td>
          <div align="center">
            <table border="0" cellspacing="1" width="100%">
              <tr>
                <td width="100%">

<p align="center"><b><font face="Arial" color="#000000" size="2"><br>
<br>
Thank You For Submitting
Your Details</font></b></p>
                </td>
              </tr>
              <tr>
                <td width="100%">
<p align="center"><font face="Arial" size="2">We will contact you as soon as
possible</font></p>

                </td>
              </tr>
            </table>
          </div>
        </td>
      </tr>
    </table>
    </center>
  </div>

</body>

</html>
