jQuery(document).ready(function($) {

        if (woocommerce_params.option_ajax_add_to_cart=='yes') 
	{
		$('body').bind('added_to_cart', function(event, params) {
                        
			var data = {
                                action: 'iface_mini_cart_update',
                        };

   			// Ajax action
   			$.post( woocommerce_params.ajax_url, data, function(response) {
			
				if (response < 1)
					return true;
					
				$('#iface-mini-cart').html(response);

			});

		});	
	
	}

	return true;
});
