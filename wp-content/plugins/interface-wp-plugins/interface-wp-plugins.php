<?php
/*
Plugin Name: Interface Holdings General WordPress Plugin
Plugin URI: http://interface.co.za/
Description: Collection of plugins and widgets used specifically by Interface Holdings websites.
Version: 1.0
Author: Dieter Konigsberger
Author URI: http://interface.co.za/
License: Proprietry
*/

if (!defined('ABSPATH')) 
	exit;

require_once( ABSPATH . 'wp-admin/includes/plugin.php' );

//if (class_exists('IfaceWP'))
//	return;


class IfaceWP
{

	private $pluginUrl = '';

	/*	
	*  Construct the IfaceWP object here
	*/
	public function __construct()
	{
		$this->includes();		

	}

	/*
	* Include all required files which comprise this plugin
	*/
	private function includes()
	{
		if (is_admin()) 
			$this->adminIncludes();
                if (!is_admin()) 
			$this->frontEndIncludes();

		// Initiate and register all widgets in this file
		require_once('widgets/widget-iface-init.php');	
	
		// Woocommerce quote gateway
		require_once('classes/class-wc-quote.php');
		// Woocommerce enquiry form utillsing Gravity forms
		require_once('classes/class-wc-product-enquiry-form.php');		
		// Woocommerce dynamic pricing extension. Used to show the dynamic price on product detail pages
		require_once('classes/class-wc-dynamic-pricing.php');		

	
		add_action( 'init', array( &$this, 'init' ), 0 );

	}
	
	/*
	* Initiate everything that needs to be created at WordPress initialisation.
	*/
	public function init()
	{
		if (!is_admin() || defined('DOING_AJAX')) 
		{
			add_action( 'wp_enqueue_scripts', array(&$this, 'registerAssets') );	
	
		}
	}

	/*
	* Include all admin section related files here
	*/
	private function adminIncludes()
	{
		require_once('classes/class-iface-admin-options.php');

	}

	/*
	* Include all frontend related files here
	*/
	private function frontEndIncludes()
	{
		require_once('classes/class-iface-frontend-options.php');
	}

	/*
	* Register all assets here (css,js etc)
	*/
	public function registerAssets()
	{
		if ( is_active_widget( false, false, 'iface-mini-cart-widget' , true ) )
			wp_enqueue_script( 'iface-widget-mini-cart', $this->pluginUrl() . '/assets/js/iface-widget-mini-cart.js', array('jquery'), '1.0', true );		
	}

        /**
         * Get the plugin url
         */
        public function pluginUrl() 
	{
                if ( $this->pluginUrl ) return $this->pluginUrl;
                return $this->pluginUrl = plugins_url( basename( plugin_dir_path(__FILE__) ), basename( __FILE__ ) );
        }


}

/**
 * Init IfaceWp class
 */
$GLOBALS['ifaceWp'] = new IfaceWP;
