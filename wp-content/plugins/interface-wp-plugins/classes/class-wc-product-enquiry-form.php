<?php

if (is_plugin_active('woocommerce/woocommerce.php') || is_plugin_active_for_network('woocommerce/woocommerce.php'))
{
	/**
	 * interfaceWcProductEnquiryForm class
	 **/
	if (!class_exists('interfaceWcProductEnquiryForm'))
	{
	 
		class interfaceWcProductEnquiryForm 
		{
			
			public $settings;
			public $isGravityActive=false;
			
			public function __construct() 
			{ 
					
				if (is_plugin_active('gravityforms/gravityforms.php') || is_plugin_active_for_network('gravityforms/gravityforms.php'))
					$this->isGravityActive = true;	
				
				// Init settings
				$this->settings = array(
					array( 'name' => 'Product Enquiries', 'type' => 'title', 'desc' => '', 'id' => 'interface_wc_product_enquiry_form' ),
					array(  
						'name' 		=> 'Disable enquiry form?',
						'desc' 		=> 'Disable the enquiry form on all product pages.',
						'id' 		=> 'interface_wc_product_enquiry_form_global_disable',
						'type' 		=> 'checkbox',
						'std'		=> ''
					),
					
					array(  
						'name' 		=> 'Gravity Form',
						'desc' 		=> 'Select the Gravity Form to use for the product enquiry.',
						'id' 		=> 'interface_wc_product_enquiry_form_gravity_form',
						'type' 		=> 'select',
						'std'		=> '',
						'options'	=> $this->getGravityForms(),
					),
					array( 'type' => 'sectionend', 'id' => 'interface_wc_product_enquiry_form'),
				);
				
				// Settings
				add_action('woocommerce_settings_general_options_after', array(&$this, 'adminSettings'));
				add_action('woocommerce_update_options_general', array(&$this, 'saveAdminSettings'));
				
			   	// Frontend
				add_action('woocommerce_product_tabs', array(&$this, 'productEnquiryTab'), 25);
				add_action('woocommerce_product_tab_panels', array(&$this, 'productEnquiryTabPanel'), 25);
				
				// AJAX
				//add_action('wp_ajax_woocommerce_product_enquiry_post', array(&$this, 'processForm'));
				//add_action('wp_ajax_nopriv_woocommerce_product_enquiry_post', array(&$this, 'processForm'));
				
				// Write panel
				add_action('woocommerce_product_options_general_product_data', array(&$this, 'writePanel'));
				add_action('woocommerce_process_product_meta', array(&$this, 'writePanelSave'));
				
		        } 
		    
	        	/*-----------------------------------------------------------------------------------*/
			/* Class Functions */
			/*-----------------------------------------------------------------------------------*/ 
			public function getGravityForms()
			{
				$ret = array('',);
				if ($this->isGravityActive)
				{
					$forms = RGFormsModel::get_forms(1, 'title');	
					if (!empty($forms))
					{
						foreach ($forms as $form)
							$ret[$form->id] = $form->title;
					}
				}
				return $ret;
			}
 
			public function productEnquiryTab() 
			{
				if (!$this->isGravityActive)
                                        return;

				if (get_option('interface_wc_product_enquiry_form_global_disable')=='yes') return;	
				if (get_post_meta($post->ID, 'interfac_wc_disable_product_enquiry_form', true)=='yes') return;
				?>
				<li><a href="#interface-wc-tab-enquiry">Product Enquiry</a></li>
				<?php
			}
			
			
			public function productEnquiryTabPanel() 
			{
				if (!$this->isGravityActive)
                                        return;
				
				global $post, $woocommerce;
			
				if (get_option('interface_wc_product_enquiry_form_global_disable')=='yes') return;	
                                if (get_post_meta($post->ID, 'interfac_wc_disable_product_enquiry_form', true)=='yes') return;
				
				$formId = get_option('interface_wc_product_enquiry_form_gravity_form');

				if (empty($formId))
					return;	
	
				if (is_user_logged_in()) :
					$current_user = get_user_by('id', get_current_user_id());
				endif;
			
				?>
				<div class="panel" id="interface-wc-tab-enquiry">
				<h2>Product Enquiry</h2>	
				<?php 
					$formMarkup = RGForms::get_form($formId, false, false, false, null, false);				
					echo $formMarkup;
				?>

				</div>
				<?php
			}
			
			
			public function processForm() 
			{
				global $woocommerce;
				
			}

			public function adminSettings() 
			{
				woocommerce_admin_fields( $this->settings );
			}
			
			public function saveAdminSettings() 
			{
				woocommerce_update_options( $this->settings );
			}
			
		        public function writePanel() 
			{
		    		echo '<div class="options_group">';
		    		woocommerce_wp_checkbox( array( 'id' => 'interfac_wc_disable_product_enquiry_form', 'label' => 'Disable enquiry form on this product?') );
		  		echo '</div>';
		    	}
		    
		    	public function writePanelSave( $post_id ) 
			{
		    		$woocommerce_disable_product_enquiry = isset($_POST['interfac_wc_disable_product_enquiry_form']) ? 'yes' : 'no';
		    		update_post_meta($post_id, 'interfac_wc_disable_product_enquiry_form', $woocommerce_disable_product_enquiry);
		    	}
			
		}
		
		$GLOBALS['interfaceWcProductEnquiryForm'] = new interfaceWcProductEnquiryForm;
	}
}
