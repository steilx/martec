<?php

if (is_plugin_active('woocommerce/woocommerce.php') || is_plugin_active_for_network('woocommerce/woocommerce.php') && is_plugin_active('woocommerce-dynamic-pricing/woocommerce_pricing.php'))
{
	/**
	 * InterfaceWcDynamicPricing class
	 **/
	if (!class_exists('InterfaceWcDynamicPricing'))
	{
	 
		class InterfaceWcDynamicPricing 
		{
			public static $addedRoles = 1;	
			
			public function __construct() 
			{ 
				add_filter('woocommerce_get_price', array(&$this, 'dynamicPrice'), 10, 2);
		        } 
		    
	        	/*-----------------------------------------------------------------------------------*/
			/* Class Functions */
			/*-----------------------------------------------------------------------------------*/ 
			public function dynamicPrice($price, $product)
			{

   				if (!is_user_logged_in()) {
					return $price;
				}

        			$user = wp_get_current_user();

    				if ( empty( $user ) ) {
        				return $price;
				}

				$rules = $product->product_custom_fields['_pricing_rules'];
				// This only checks for simple fixed price based role prices!!	
				if (!empty($rules) && is_array($rules)) {
					$ruleSet = maybe_unserialize($rules[0]);
				
					foreach ($ruleSet as $set) {
						if (isset($set['conditions'][1]['args']['roles'][0])) {
							$role = $set['conditions'][1]['args']['roles'][0];
							if (in_array( $role, (array) $user->roles ) && $set['rules'][1]['type'] == 'fixed_price') {
								$price = $set['rules'][1]['amount'];	
							}
						}
					}
					unset($ruleSet,$set);	
				}

    				return $price;
			}
		}
		
		$GLOBALS['InterfaceWcDynamicPricing'] = new InterfaceWcDynamicPricing;
	}
}
