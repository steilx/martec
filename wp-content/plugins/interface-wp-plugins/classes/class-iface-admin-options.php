<?php

/**
 * interfaceAdminOptions class
 **/
if (!class_exists('interfaceAdminOptions'))
{
	 
	class interfaceAdminOptions 
	{
			
		public $settings;
			
		public function __construct() 
		{ 
					
			// Menu
			add_action('admin_menu', array(&$this, 'setAdminMenu'));
					
		} 
		    
		/*-----------------------------------------------------------------------------------*/
		/* Class Functions */
		/*-----------------------------------------------------------------------------------*/ 
		public function setAdminMenu()
		{
			//add_options_page('IFACE Options', 'IFACE Options', 8, 'iface-options-page-slug', array(&$this,'adminPage'));
			add_options_page('Google Analytics', 'Google Analytics', 9, 'iface-options-ga-slug', array(&$this,'googleAnalytics'));
		}

		public function adminPage()
		{

		}

		public function googleAnalytics()
		{
			
			$ua_code = '';
			$universal_tracking = 0;
			$demographic_tracking = 0;


			if (isset($_POST['analytics_update']))
			{
				$ga = array(
						'ua_code' => $_POST['iface_options_google_analytics'],
                                                'universal_tracking' => (isset($_POST['iface_options_universal_tracking']) ? 1 : 0 ),
                                                'demographic_tracking' => (isset($_POST['iface_options_demographic_tracking']) ? 1 : 0 ),
					);

				update_option('iface_options_google_analytics', $ga );

				echo '<p><strong><font color="green">Settings saved.</font></strong></p>';
			}


			$data = get_option('iface_options_google_analytics');

			if (!empty($data)) {
				if (is_array($data)) {
					$ua_code = $data['ua_code'];
					$universal_tracking = $data['universal_tracking'];
					$demographic_tracking = $data['demographic_tracking'];
				} else {
					$ua_code = $data;
				}
			}

			?>

			<div class="wrap">
			<div id="icon-options-general" class="icon32"><br /></div><h2>Google Analytics Settings</h2>

			<form method="post" action="<?php echo $_SERVER["REQUEST_URI"]; ?>">
    			<input type="hidden" name="analytics_update" id="analytics_update" value="true" />


	                <h3>Analytics Options</h3>
                        <div>
                        <table class="form-table">

                        <tr valign="top" class="alternate">
                                        <th scope="row" style="width:32%;"><label>1) <b>Google</b> Analytics ID (leave blank to disable analytics)</label></th>
                                <td>
                                        <input name="iface_options_google_analytics" type="text" size="55" value="<?php echo $ua_code;  ?>" autocomplete="off" />
                                        <br />(example: <font color="red"><code>UA-8123456-1</code></font>)<br />
                                </td>
                        </tr>

                        <tr valign="top" class="alternate">
                                        <th scope="row" style="width:32%;"><label>2) <b>Enable</b> Universal Tracking Code </label></th>
                                <td>
                                        <input name="iface_options_universal_tracking" type="checkbox" value="1" <?php echo ($universal_tracking == 1) ? 'CHECKED' : '' ; ?> /> 
                                </td>
                        </tr>
		
                        <tr valign="top" class="alternate">
                                        <th scope="row" style="width:32%;"><label>3) <b>Enable</b> Demographic Tracking</label></th>
                                <td>
                                        <input name="iface_options_demographic_tracking" type="checkbox" value="1"  <?php echo ($demographic_tracking == 1) ? 'CHECKED' : '' ; ?> />
                                </td>
                        </tr>
	
			</table>
			
			</div>

			</div>

			<p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes"  /></p>

			</form>
			<?php

		}
			
	}
		
	$GLOBALS['interfaceAdminOptions'] = new interfaceAdminOptions;
}
