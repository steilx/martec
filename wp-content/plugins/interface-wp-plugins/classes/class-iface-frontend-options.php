<?php

/**
 * interfaceFrontendOptions class
 **/
if (!class_exists('interfaceFrontendOptions'))
{
	 
	class interfaceFrontendOptions 
	{
			
		public $settings;
		
		public function __construct() 
		{ 
			// Header
			add_action('wp_head',array(&$this, 'setHeader'));	
			
			// Gravity check
			add_action('gform_after_submission', array(&$this,'checkGravitySubmission'), 10, 2);
		
		} 
		    
		/*-----------------------------------------------------------------------------------*/
		/* Class Functions */
		/*-----------------------------------------------------------------------------------*/ 
		public function setHeader()
		{
			$ga = get_option('iface_options_google_analytics');

			$out = '';

			if (!empty($ga))
			{

				$ua_code = '';
				$universal_tracking = 0;
				$demographic_tracking = 0;

				if (is_array($ga)) {
					$ua_code = $ga['ua_code'];
                        		$universal_tracking = $ga['universal_tracking'];
                        		$demographic_tracking = $ga['demographic_tracking'];
				} else {
					$ua_code = $ga;
				}

				if (empty($ua_code)) {
					return;
				}			
	
		                $out =  '<script type="text/javascript">'."\n";

				if ($universal_tracking == 0) {

		                	$out .= 'var _gaq = _gaq || [];'."\n";
                			$out .= '_gaq.push([\'_setAccount\', \'' . $ua_code . '\']);'."\n";
                			$out .= '_gaq.push([\'_trackPageview\']);'."\n";
                			$out .= '_gaq.push([\'_trackPageLoadTime\']);'."\n";
				

					// Analytics Goals and Events

					/* old code
					// Quick contact submit 
					if ($_REQUEST['qcAdwords'] && $_REQUEST['qcAdwords'] == 'fSbm') {
						$out .=  "_gaq.push(['_trackEvent', 'CallToAction', 'fSbm']);"."\n";
					}

					// Mail to link clicked
					if ($_REQUEST['qcAdwords'] && $_REQUEST['qcAdwords'] == 'mTo') {
						$out .= "_gaq.push(['_trackEvent', 'CallToAction', 'mTo']);"."\n";
					}

					// Contact Page
					if ($_REQUEST['qcAdwords'] && $_REQUEST['qcAdwords'] == 'cPg') {
						$out .= "_gaq.push(['_trackEvent', 'CallToAction', 'cPg']);"."\n";
					}		
					*/

                                        if ($_REQUEST['qcAdwords'] && !empty($_REQUEST['qcAdwords']) ) {
                                                $out .= "_gaq.push(['_trackEvent', 'CallToAction', '".$_REQUEST['qcAdwords']."']);"."\n";
                                        }

					if (isset($_REQUEST['gform_submit'])) {
						$out .= "_gaq.push(['_trackEvent', 'CallToAction', 'gForm', '".$_REQUEST['gform_submit']."']);"."\n";
					}

                			$out .= '(function() {'."\n";
                			$out .= 'var ga = document.createElement(\'script\'); ga.type = \'text/javascript\'; ga.async = true;'."\n";
                			if ($demographic_tracking == 0) {
						$out .= 'ga.src = (\'https:\' == document.location.protocol ? \'https://ssl\' : \'http://www\') + \'.google-analytics.com/ga.js\';'."\n";
					} else {
						$out .= 'ga.src = (\'https:\' == document.location.protocol ? \'https://\' : \'http://\') + \'stats.g.doubleclick.net/dc.js\';'."\n";
					}
                			$out .= 'var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(ga, s);'."\n";
                			$out .= ' })();'."\n";

				} else { // universal tracking enabled
				
 					$out .= '(function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){'."\n";
 					$out .= '(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),'."\n";
					$out .= 'm=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)'."\n";
 					$out .= '})(window,document,\'script\',\'//www.google-analytics.com/analytics.js\',\'ga\');'."\n";

 					$out .= 'ga(\'create\', \'' . $ua_code . '\', \'auto\');'."\n";

					if ($demographic_tracking == 1) {
						$out .= 'ga(\'require\', \'displayfeatures\');'."\n";
					}

                                        // QC Adwords Contact gotm
                                        if ($_REQUEST['qcAdwords'] && !empty($_REQUEST['qcAdwords'])) {
                                                $out .= "ga('send', 'event', 'CallToAction', '".$_REQUEST['qcAdwords']."');"."\n";
                                        }

                                        if (isset($_REQUEST['gform_submit'])) {
                                                $out .= "ga('send', 'event', 'CallToAction', 'gForm', '".$_REQUEST['gform_submit']."');"."\n";
                                        }

 					$out .= 'ga(\'send\', \'pageview\');'."\n";

				}                		


				$out .= '</script>'."\n";
			
			}

			echo $out;

		}
 
			

		public function checkGravitySubmission($entry, $form)
		{
			if (isset($form['notification']['to']) && $form['notification']['to']=='{admin_email}')
			{
				$headers = 'From: WMX <noreply@wmx.co.za>' . "\r\n" ;
			        mail('tessa.venter@iface.co.za','GRAVITY TO missing',$entry['source_url'],$headers);
			}
	
			return;
		}

	}
		
	$GLOBALS['interfaceFrontendOptions'] = new interfaceFrontendOptions;
}
