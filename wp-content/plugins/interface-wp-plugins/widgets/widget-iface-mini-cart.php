<?php

/*
Mini cart widget for Woocommerce.
*/

class IfaceWidgetMiniCart extends WP_Widget
{
	
	public function __construct() 
	{
		// widget actual processes
		parent::__construct(
				'iface-mini-cart-widget',
				'Interface Mini Cart widget',
				array(
					'description'=>'A Widget to display a mini cart based on the WooCommerce WordPress Plugin',
				)
			);
	}

 	public function form($instance) 
	{
		// outputs the options form on admin
	}

	public function update($newInstance, $oldInstance) 
	{
		// processes widget options to be saved
		return $newInstance;
	}

	public function widget($args, $instance) 
	{
		// outputs the content of the widget
		global $woocommerce;
		echo $args['before_widget'];
			
		?>
		<div id="mini-cart">
		<b>Items in cart:</b>
		<a class="cart-contents" id="iface-mini-cart" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'woothemes'); ?>"><?php echo sprintf(_n('%d item', '%d items', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);?> - <?php echo $woocommerce->cart->get_cart_total(); ?></a>
		</div>
		<?php
		echo $args['after_widget'];
		
	}


}

add_action('wp_ajax_iface_mini_cart_update', 'ifaceWidgetMiniCartAjax');
add_action('wp_ajax_nopriv_iface_mini_cart_update', 'ifaceWidgetMiniCartAjax');

function ifaceWidgetMiniCartAjax() 
{
	global $woocommerce;
	echo sprintf(_n('%d item', '%d items', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count).'  - '.$woocommerce->cart->get_cart_total(); 
	die(); // this is required to return a proper result
}
