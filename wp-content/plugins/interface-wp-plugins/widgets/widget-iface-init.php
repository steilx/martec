<?php

/*
Initialise all the Interface Holdings widgets
*/

require_once('widget-iface-mini-cart.php');

function ifaceWidgetsInit()
{
	if (is_plugin_active('woocommerce/woocommerce.php') || is_plugin_active_for_network('woocommerce/woocommerce.php'))
		register_widget('IfaceWidgetMiniCart');
}

add_action('widgets_init','ifaceWidgetsInit');
