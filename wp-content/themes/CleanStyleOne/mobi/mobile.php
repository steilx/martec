<?php
 
 /* Enque styles and scripts - Front End*/
 
function mobile_options_enqueue() {
	wp_enqueue_script( 'CleanStyleOne-script', get_template_directory_uri() . '/mobi/functions.js', array( 'jquery' ), '2013-07-18', true );
}

if ( get_option(mobile_options_enabled) ) {
	add_action( 'wp_enqueue_scripts', 'mobile_options_enqueue' );
}
// End enqueue front end scripts

/* Enque styles and scripts - Admin interface */

function mobile_options_admin_enqueue() {
	wp_enqueue_style( 'farbtastic' );
	wp_enqueue_script( 'farbtastic' );
	
	wp_register_script( 'farbtastic-instance', get_template_directory_uri() . '/inc/farbtastic-instance.js',array( 'farbtastic', 'jquery' ),'0.1',true);	
	wp_enqueue_script( 'farbtastic-instance' );
}

add_action('admin_enqueue_scripts', 'mobile_options_admin_enqueue');
// End enqueue admin scripts

 /* Create items in left menu */

function mobile_options_add_admin() {
	$theme_page = add_theme_page(
		__( 'Mobile Options', 'CleanStyleOne' ),   	// Name of page
		__( 'Mobile Options', 'CleanStyleOne' ),   	// Label in menu
		'edit_theme_options',				// Capability required
		'mobile_options',                     // Menu slug, used to uniquely identify the page
		'mobile_options_admin' 						// Function that renders the options page
	);

	if ( ! $theme_page )
		return;
}

add_action('admin_menu' , 'mobile_options_add_admin');


function mobile_options_admin()  
{
	
	// ====== load defaults  ====== 
	
	if (!get_option('mobile_options_menuScreenWidth')) { update_option('mobile_options_menuScreenWidth','359'); }			
	if (!get_option('mobile_options_menuToggleOnSMwidth')) { update_option('mobile_options_menuToggleOnSMwidth','359'); }

	
    if (isset($_POST['options-submit']))	
    {
			// General
			$mobile_options_enabled = $_POST['mobile_options_enabled'];
			update_option('mobile_options_enabled', $mobile_options_enabled);
			
			// Menu
			$mobile_options_menuScreenWidth = $_POST['mobile_options_menuScreenWidth'];
			update_option('mobile_options_menuScreenWidth', $mobile_options_menuScreenWidth);			

			$mobile_options_menuToggleOnSMwidth = $_POST['mobile_options_menuToggleOnSMwidth'];
			update_option('mobile_options_menuToggleOnSMwidth', $mobile_options_menuToggleOnSMwidth);
	
			$mobile_options_menuText = $_POST['mobile_options_menuText'];
			update_option('mobile_options_menuText', $mobile_options_menuText);	
			
			$mobile_options_menuBgColor = $_POST['mobile_options_menuBgColor'];
			update_option('mobile_options_menuBgColor', $mobile_options_menuBgColor);	
			
			$mobile_options_menuTextColor = $_POST['mobile_options_menuTextColor'];
			update_option('mobile_options_menuTextColor', $mobile_options_menuTextColor);

			$mobile_options_menuTextHoverColor = $_POST['mobile_options_menuTextHoverColor'];
			update_option('mobile_options_menuTextHoverColor', $mobile_options_menuTextHoverColor);

			$mobile_options_menuTopBorderColor = $_POST['mobile_options_menuTopBorderColor'];
			update_option('mobile_options_menuTopBorderColor', $mobile_options_menuTopBorderColor);

			$mobile_options_menuBottomBorderColor = $_POST['mobile_options_menuBottomBorderColor'];
			update_option('mobile_options_menuBottomBorderColor', $mobile_options_menuBottomBorderColor);
			
						
			
			// Media queries 
			$mobile_options_mediaQuery_tablets_portrait = $_POST['mobile_options_mediaQuery_tablets_portrait'];
			update_option('mobile_options_mediaQuery_tablets_portrait', $mobile_options_mediaQuery_tablets_portrait);	

			$mobile_options_mediaquery_tablet_general = $_POST['mobile_options_mediaQuery_tablets_general'];
			update_option('mobile_options_mediaQuery_tablets_general', $mobile_options_mediaquery_tablet_general);	
	
			$mobile_options_mediaQuery_smart_phone = $_POST['mobile_options_mediaQuery_smart_phone'];
			update_option('mobile_options_mediaQuery_smart_phone', $mobile_options_mediaQuery_smart_phone);	
			
			echo '<div class="updated"><p>Your new options have been successfully saved.</p></div>';


		
	} // if options-submit
	
?>



<div class="wrap">
            <div id="icon-themes" class="icon32"></div>
            <h2>Mobile Options</h2>

            <div id="mypicker">
              <div id="picker"></div>
            </div>
                
                
            <form name="theform" id="theform" method="post" enctype="multipart/form-data" action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']);?>">
                
                
            <div class="optionheading">General</div>
            <div class="optionbox">
           	  <div class="optionitem">Mobile Options Enabled?</div> 
              <div class="optionitem"><input name="mobile_options_enabled" type="checkbox" value="1" <?php if (get_option('mobile_options_enabled') == 1 ) { echo checked; } ?>/></div>
           	  <div class="optionitem" style="float:right; text-align:right;"><input type="submit" class="button-primary" name="submit" value="Save Options" /></div>                                                       
        	</div>
 <!-- Menu settting -------------------------------------->         
            <div class="optionheading">Menu</div>
            <div class="optionbox">
            
           	  <div class="optionitem">Show on max screen width <b title="This is the width where the the mobile menu need to show.&#13;Default for mobile is 359px">&#9432;</b></div>
           	  <div class="optionitem"><input name="mobile_options_menuScreenWidth" type="text" value="<?php echo get_option('mobile_options_menuScreenWidth')?>" /> px</div>

           	  <div class="optionitem">Sub-menu toggle width <b title="This the width at which the sub menu toggle (click in stead of hover) is enabled.&#13;Default for mobile is 359px">&#9432;</b></div>
           	  <div class="optionitem"><input name="mobile_options_menuToggleOnSMwidth" type="text" value="<?php echo get_option('mobile_options_menuToggleOnSMwidth')?>" /> px</div>


              <div class="clear"></div>
            
           	  <div class="optionitem">Menu Text</div>
           	  <div class="optionitem"><input name="mobile_options_menuText" type="text" value="<?php echo get_option('mobile_options_menuText')?>" />
           	  </div>

           	  <div class="optionitem">Background colour</div>
           	  <div class="optionitem"><input name="mobile_options_menuBgColor" class="colorwell" type="text" onfocus="if (this.value==''){this.value='#';}" onblur="if (this.value=='#'){ this.value='';}" id="mobile_options_menuBgColor" value="<?php echo get_option('mobile_options_menuBgColor')?>" /></div>

              <div class="clear"></div>
              
           	  <div class="optionitem">Text colour</div>
           	  <div class="optionitem"><input name="mobile_options_menuTextColor" class="colorwell" type="text" onfocus="if (this.value==''){this.value='#';}" onblur="if (this.value=='#'){ this.value='';}" id="mobile_options_menuTextColor" value="<?php echo get_option('mobile_options_menuTextColor')?>" /></div>

           	  <div class="optionitem">Text hover colour</div>
           	  <div class="optionitem"><input name="mobile_options_menuTextHoverColor" class="colorwell" type="text" onfocus="if (this.value==''){this.value='#';}" onblur="if (this.value=='#'){ this.value='';}" id="mobile_options_menuTextHoverColor" value="<?php echo get_option('mobile_options_menuTextHoverColor')?>" /></div>
              
              
              <div class="clear"></div>              
              
           	  <div class="optionitem">Top border color:</div>
           	  <div class="optionitem"><input name="mobile_options_menuTopBorderColor" class="colorwell" type="text" onfocus="if (this.value==''){this.value='#';}" onblur="if (this.value=='#'){ this.value='';}" id="mobile_options_menuTopBorderColor" value="<?php echo get_option('mobile_options_menuTopBorderColor')?>" /></div>

           	  <div class="optionitem">Bottom border color:</div>
           	  <div class="optionitem"><input name="mobile_options_menuBottomBorderColor" class="colorwell" type="text" onfocus="if (this.value==''){this.value='#';}" onblur="if (this.value=='#'){ this.value='';}" id="mobile_options_menuBottomBorderColor" value="<?php echo get_option('mobile_options_menuBottomBorderColor')?>" /></div>

           	  <div class="optionitem" style="float:right; text-align:right;"><input type="submit" class="button-primary" name="submit" value="Save Options" /></div>                                                                   

            </div>            


 <!-- max width 979px (iPad / Large tablet - Portrait) -------------------------------------->         
           <div class="optionheading">Styles for max-width: 979px (iPad / Large tablet - Portrait)</div>
           <div class="optionbox">
              <div class="optionitem" style="width:99%">Type in the CCS code that you want to use. E.g body { font-size: 10px !important; }</div>
              <div class="clear"></div>              
              <div class="optionitem" style="width:99%">
                <textarea style="width:99%" name="mobile_options_mediaQuery_tablets_portrait" rows="5" id="mobile_options_mediaQuery_tablets_portrait"><?php echo get_option('mobile_options_mediaQuery_tablets_portrait')?></textarea>
             </div>
              <div class="optionitem" style="float:right; text-align:right;"><input type="submit" class="button-primary" name="submit" value="Save Options" /></div>
           </div>
           
 <!-- max-width: 767px (Small tablet tablets and big Smart Phones) -------------------------------------->         
           <div class="optionheading">Styles for max-width: 767px (Small tablet tablets and big Smart Phones) <em>iPad breakpoint</em></div>
           <div class="optionbox">
              <div class="optionitem" style="width:99%">Type in the CCS code that you want to use. E.g body { font-size: 10px !important; }</div>
              <div class="clear"></div>              
              <div class="optionitem" style="width:99%">
                <textarea style="width:99%" name="mobile_options_mediaQuery_tablets_general" rows="5" id="mobile_options_mediaQuery_tablets_general"><?php echo get_option('mobile_options_mediaQuery_tablets_general')?></textarea>
             </div>
              <div class="optionitem" style="float:right; text-align:right;"><input type="submit" class="button-primary" name="submit" value="Save Options" /></div>
           </div>

 <!-- max-width: 480px (Mobile, general Smart Phone) -------------------------------------->         
           <div class="optionheading">Styles for max-width: 480px (Mobile, general Smart Phone)</div>
           <div class="optionbox">
              <div class="optionitem" style="width:99%">Type in the CCS code that you want to use. E.g body { font-size: 10px !important; }</div>
              <div class="clear"></div>              
              <div class="optionitem" style="width:99%">
                <textarea style="width:99%" name="mobile_options_mediaQuery_smart_phone" rows="5" id="mobile_options_mediaQuery_smart_phone"><?php echo get_option('mobile_options_mediaQuery_smart_phone')?></textarea>
             </div>
              <div class="optionitem" style="float:right; text-align:right;"><input type="submit" class="button-primary" name="submit" value="Save Options" /></div>
           </div>
                           
            
            
           		<input type="hidden" name="options-submit" value="1" />

            </form> <!-- / theform -->
            
                
                
</div>
    
    
<?php            	
	
} // function mobile_options_admin

function get_mobile_options()
{
	if ( get_option('mobile_options_enabled') ){
		echo '<meta name="viewport" content="width=device-width,initial-scale=1">';
		echo'<!--[if lt IE 9]><script src="'. get_template_directory_uri() .'/mobi/html5.js"></script><![endif]-->';
		echo '<link rel="stylesheet" type="text/css" media="all" href="'. get_template_directory_uri() .'/mobi/mobile.css" />';
		echo '<link rel="stylesheet" type="text/css" media="all and (max-width: '.get_option('mobile_options_menuScreenWidth').'px)" href="'. get_template_directory_uri() .'/mobi/mobile-menu.css" />';
		echo '<script type="text/javascript">var subMenuToggle = "'.get_option('mobile_options_menuToggleOnSMwidth').'";</script>';
?>
<style type="text/css">
<?php 
	if (get_option('mobile_options_menuBgColor')): 
		echo '#menu .toggled-on ul li{background-color: '.get_option('mobile_options_menuBgColor').';}';
	endif;
?>

<?php if (get_option('mobile_options_menuTopBorderColor')) { ?>
#menu .toggled-on ul { 	border-bottom-color: <?php echo get_option('mobile_options_menuTopBorderColor'); ?>; }
#menu .toggled-on ul li, 
#menu .toggled-on ul li ul li { border-top-color: <?php echo get_option('mobile_options_menuTopBorderColor'); ?>; }
<?php } ?>
<?php if (get_option('mobile_options_menuBottomBorderColor')) { ?>
#menu .toggled-on ul { 	border-top-color: <?php echo get_option('mobile_options_menuBottomBorderColor'); ?>; }
#menu .toggled-on ul li, 
#menu .toggled-on ul li ul li,
#menu .toggled-on ul li:last-child  { border-bottom-color: <?php echo get_option('mobile_options_menuBottomBorderColor'); ?>; }
.toggled-on .nav-menu .sdrn_icon_par { border-right-color: <?php echo get_option('mobile_options_menuBottomBorderColor'); ?>; }
<?php } ?>
<?php if (get_option('mobile_options_menuTextColor')) { ?>
#menu .toggled-on ul li a,
.toggled-on .nav-menu .sdrn_icon_par { color:<?php echo get_option('mobile_options_menuTextColor'); ?>; }
<?php } ?>
<?php if (get_option('mobile_options_menuTextHoverColor')) { ?>
#menu .toggled-on ul li a:hover,
.toggled-on .nav-menu .sdrn_icon_par:hover { color:<?php echo get_option('mobile_options_menuTextHoverColor'); ?>; }
<?php } ?>
<?php // Write extra media queries //
	if (get_option('mobile_options_mediaQuery_tablets_portrait')) { 
		echo '@media (max-width: 979px) { '.get_option('mobile_options_mediaQuery_tablets_portrait').'}';
	}
	if (get_option('mobile_options_mediaQuery_tablets_general')) {
		echo '@media (max-width: 767px) { '.get_option('mobile_options_mediaQuery_tablets_general').'}';		
	}
	if (get_option('mobile_options_mediaQuery_smart_phone')) {
		echo '@media (max-width: 480px) { '.get_option('mobile_options_mediaQuery_smart_phone').'}';		
	}
?>
</style>

<?

	} // if mobile_options_enabled
	
	
} // get_mobile_options

add_action('wp_head', 'get_mobile_options');


function get_mobile_options_footer() {
	if ( get_option('mobile_options_enabled') ){
		echo ' <a href="#top" id="top-link">Back to Top</a>';
	} // if mobile_options_enabled
	
} // end get_mobile_options_footer()
add_action('wp_footer', 'get_mobile_options_footer');



	// Add specific CSS class by filter
	add_filter('body_class','my_class_names');
	function my_class_names($classes) {
		
		// Device detection
		$ua = trim(strtolower($_SERVER['HTTP_USER_AGENT']));
		$pattern = '/(android\s\d|blackberry|ip(hone|ad|od)|iemobile|webos|palm|symbian|kindle|windows|win64|wow64|macintosh|intel\smac\sos\sx|ppx\smac\sos\sx|googlebot|googlebot-mobile)/';
		if(preg_match($pattern,$ua,$matches)){$platform=$matches[0];}
		
		
		/* if the query_var 'platform' even exists */
		if(('platform')){
			
			switch($platform){
				
				/* phones */
				case 'mobile':$tag = 'mobile';break;
				case 'android 1':	
				case 'android 2':$tag = 'android-phone';break;
				case 'android 3':	
				case 'android 4':$tag = 'android-tablet'; break;
				case 'blackberry':$tag = 'blackBerry'; break;
				case 'iphone':		
				case 'ipod':$tag = 'iphone'; break;
				case 'ipad':$tag = 'ipad'; break;
				case 'iemobile':$tag = 'ie-mobile'; break;
				case 'webos':$tag = 'web-os'; break;
				case 'kindle':$tag = 'kindle'; break;
				case 'palm':$tag = 'palm'; break;
				
				/* desktop clients */
				case 'windows':
				case 'win64':
				case 'macintosh':
				case 'ppx mac os x':
				case 'intel mac os x':
				case 'googlebot':
				case 'desktop':$tag = 'desktop';break;
				
				/* this case will not happen - since the query_var defaults to desktop */
				default: $tag = $platform; break;
			}
			
			//echo $tag;
			
		}
		
		// add 'class-name' to the $classes array
		$classes[] = $tag;
		// return the $classes array
		return $classes;
	}


?>