/**
 * Functionality specific to theme enabling it to be responsive
 *
 * Provides helper functions to enhance the theme experience.
 * Originally from Wordpress.org Twenty Thirteen
 * Edited by: Paul Combrink
 * Date: 30 December 2013
 */

( function( $ ) {
	var body    = $( 'body' ),
	    _window = $( window );

	/**
	 * Adds a top margin to the footer if the sidebar widget area is higher
	 * than the rest of the page, to help the footer always visually clear
	 * the sidebar.
	 */
	$( function() {
		if ( body.is( '.sidebar' ) ) {
			var sidebar   = $( '#secondary .widget-area' ),
			    secondary = ( 0 == sidebar.length ) ? -40 : sidebar.height(),
			    margin    = $( '#tertiary .widget-area' ).height() - $( '#content' ).height() - secondary;

			if ( margin > 0 && _window.innerWidth() > 999 )
				$( '#colophon' ).css( 'margin-top', margin + 'px' );
		}
	} );

	/**
	 * Enables menu toggle for small screens.
	 */
	( function() {
		var nav = $( '#site-navigation' ), button, menu;
		if ( ! nav )
			return;

		button = nav.find( '.menu-toggle' );
		if ( ! button )
			return;

		// Hide button if menu is missing or empty.
		menu = nav.find( '.nav-menu' );
		if ( ! menu || ! menu.children().length ) {
			button.hide();
			return;
		}

		$( '.menu-toggle' ).on( 'click.twentythirteen', function() {
			nav.toggleClass( 'toggled-on' );
		} );		
		
	} )();


	function mobiResizerWMX()  {	
	
	//	if($(window).width() < 643) {
	//	if($(window).width() < 351) {
		if($(window).width() < subMenuToggle) {
		
		
			menu = $('.nav-menu'), //the menu div
			menu_a = menu.find('a'), //single mnu link
			widthwindow = $(window).width(),
		
		
			menu.find('ul.sub-menu').each(function() {
				var sub_ul = $(this),
					parent_a = sub_ul.prev('a'),
					parent_li = parent_a.parent('li').first();
		
				parent_a.addClass('sdrn_parent_item');
				parent_li.addClass('sdrn_parent_item_li');
				var expand = parent_a.before('<span class="sdrn_icon sdrn_icon_par" data-icon="t"></span> ').find('.sdrn_icon_par');
		
				sub_ul.hide();
			});
		} // end if
	
	}

	//============ Clickable Submenus ============ //
	//add arrow element to the parent li items and chide its child uls	
	( function () {	
	
		//mobiResizerWMX();
		//jQuery(window).resize(function(){ mobiResizerWMX(); });
		
		window.addEventListener('orientationchange', mobiResizerWMX() );
	
	} )();
	

	//expand / collapse action (SUBLEVELS)
	( function() {		
		$('.sdrn_icon_par').on('click',function() {
			var t = $(this),
				//child_ul = t.next('a').next('ul');
				child_ul = t.parent('li').find('ul.sub-menu').first();
			child_ul.slideToggle(100);
			t.toggleClass('sdrn_par_opened');
			t.parent('li').first().toggleClass('sdrn_no_border_bottom');
		});
		} )();
	// END Clickable Submenus 
		
	// ============ BACK TO TOP button ============ //

	( function() {			
		$(document).ready(function() {
			// Show or hide the sticky footer button
			$(window).scroll(function() {
				if ($(this).scrollTop() > 200) {
					$('#top-link').fadeIn(200);
				} else {
					$('#top-link').fadeOut(200);
				}
			});
			
			// Animate the scroll to top
			$('#top-link').click(function(event) {
				event.preventDefault();
				
				$('html, body').animate({scrollTop: 0}, "slow");
			})
		});
	} )(); 				
	// END BACK TO TOP button


	/**
	 * Makes "skip to content" link work correctly in IE9 and Chrome for better
	 * accessibility.
	 *
	 * @link http://www.nczonline.net/blog/2013/01/15/fixing-skip-to-content-links/
	 */
	_window.on( 'hashchange.twentythirteen', function() {
		var element = document.getElementById( location.hash.substring( 1 ) );

		if ( element ) {
			if ( ! /^(?:a|select|input|button|textarea)$/i.test( element.tagName ) )
				element.tabIndex = -1;

			element.focus();
		}
	} );

	/**
	 * Arranges footer widgets vertically.
	 */
	if ( $.isFunction( $.fn.masonry ) ) {
		var columnWidth = body.is( '.sidebar' ) ? 228 : 245;

		$( '#secondary .widget-area' ).masonry( {
			itemSelector: '.widget',
			columnWidth: columnWidth,
			gutterWidth: 20,
			isRTL: body.is( '.rtl' )
		} );
	}
} )( jQuery );