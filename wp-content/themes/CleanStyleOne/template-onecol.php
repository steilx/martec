<?php
/*
Template Name: One Column
*/
?>
<?php get_header(); ?>  
    <div id="main">
    <?php if ( !dynamic_sidebar('abovecontent') ) : endif; ?>
          	<div id="content">
            	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<h1 id="post-<?php the_ID(); ?>" class="<?php post_class(); ?>">
				<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a>
                </h1>
				<?php the_content('more...'); ?>
                <?php wp_link_pages(); ?>
                
  				<?php
					/* Check if comments block is anabled from theme extention */
                	if (get_option('custom_style_CommentEnabled') && get_option('custom_style_enabled') == 1 ) { 
						echo '<div class="commentblock">';
						comments_template();
						echo '</div>';							
					}
				?>
                <?php endwhile; ?>
	<?php /* Display navigation to next/previous pages when applicable */ ?>
    <?php if (  $wp_query->max_num_pages > 1 ) : ?>
                    <div id="nav-below" class="navigation">
                        <div class="nav-next"><?php next_posts_link( __( 'Next <span class="meta-nav">&rarr;</span>', 'CleanStyleOne' ) ); ?></div>
                        <div class="nav-prev"><?php previous_posts_link( __( '<span class="meta-nav">&larr;</span> Previous', 'CleanStyleOne' ) ); ?></div>
                    </div><!-- #nav-below -->
    <?php endif; ?>   
                <?php else: ?>
                <h2>Not Found</h2>
                <p>The posts you were looking for could not be found</p>
                <p>.</p>
                <?php endif; ?>
            </div>
    <?php if ( !dynamic_sidebar('belowcontent') ) : endif; ?>
    </div>
<?php get_footer(); ?>