<?php
/**
 * The main template file.
 *
 * @package WordPress
 * @subpackage CleanStyleOne
 */
?>
<?php get_header(); ?>  
    <div id="main">
    <?php if ( !dynamic_sidebar('abovecontent') ) : endif; ?>        
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
        <?php  if ( is_active_sidebar( 'leftsidebar' ) || get_option('custom_style_sideMenuEnabled') ) : ?>
          
          <td width="1%" valign="top" id="leftbar">
          	<div class="leftSideBar">

				<?php // ---------- Check if Side menu is enabled  ------
                if( get_option('custom_style_sideMenuEnabled') ) :    
                    wp_nav_menu( array( 'container_id' => 'sidemenu', 'theme_location' => 'side' ) ); 
                endif;
                ?>                   
            
            	<?php if ( !dynamic_sidebar('leftsidebar') ) : endif; ?>
            </div>
          </td>
		          
        <?php endif; ?>	
        
          <td width="99%" valign="top">
          	<div id="content">
            	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<h1 id="post-<?php the_ID(); ?>" class="<?php post_class(); ?>">
				<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a>
                </h1>
				<?php the_content('more...'); ?>
                <?php wp_link_pages(); ?>
                
  				<?php
					/* Check if comments block is anabled from theme extention */
                	if (get_option('custom_style_CommentEnabled') && get_option('custom_style_enabled') == 1 ) { 
						echo '<div class="commentblock">';
						comments_template();
						echo '</div>';							
					}
				?>
                <?php endwhile; ?>
	<?php /* Display navigation to next/previous pages when applicable */ ?>
    <?php if (  $wp_query->max_num_pages > 1 ) : ?>
                    <div id="nav-below" class="navigation">
                        <div class="nav-next"><?php next_posts_link( __( 'Next <span class="meta-nav">&rarr;</span>', 'CleanStyleOne' ) ); ?></div>
                        <div class="nav-prev"><?php previous_posts_link( __( '<span class="meta-nav">&larr;</span> Previous', 'CleanStyleOne' ) ); ?></div>
                    </div><!-- #nav-below -->
    <?php endif; ?>   
                <?php else: ?>
                <h2>Not Found</h2>
                <p>The posts you were looking for could not be found</p>
                <p>.</p>
                <?php endif; ?>
            </div>
          </td>
          
        <?php  if ( is_active_sidebar( 'rightsidebar' ) ) : ?>
          
          <td width="1%" valign="top" id="rightbar">
          	<div class="rightSideBar">
            	<?php if ( !dynamic_sidebar('rightsidebar') ) : endif; ?>
            </div>
          </td>
		          
        <?php endif; ?>	
          
        </tr>
      </table>
    <?php if ( !dynamic_sidebar('belowcontent') ) : endif; ?>          
    </div>
<?php get_footer(); ?>