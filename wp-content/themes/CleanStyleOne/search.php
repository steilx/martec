<?php
/**
 * The main template file.
 *
 * @package WordPress
 * @subpackage CleanStyleOne
 */
?>
<?php get_header(); ?>  
    <div id="main">
    <?php if ( !dynamic_sidebar('abovecontent') ) : endif; ?>    
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
        <?php  if ( is_active_sidebar( 'leftsidebar' ) || get_option('custom_style_sideMenuEnabled') ) : ?>
          
          <td width="1%" valign="top" id="leftbar">
          	<div class="leftSideBar">

				<?php // ---------- Check if Side menu is enabled  ------
                if( get_option('custom_style_sideMenuEnabled') ) :    
                    wp_nav_menu( array( 'container_id' => 'sidemenu', 'theme_location' => 'side' ) ); 
                endif;
                ?>                   
            
            	<?php if ( !dynamic_sidebar('leftsidebar') ) : endif; ?>
            </div>
          </td>
		          
        <?php endif; ?>	
        
          <td width="99%" valign="top">
          	<div id="content">
<?php if ( have_posts() ) : ?>
				<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'CleanStyleOne' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
            	<?php while (have_posts()) : the_post(); ?>
				<h2 id="post-<?php the_ID(); ?>" class="<?php post_class(); ?>">
				<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a>
                </h2>
                 <?php the_excerpt(); ?>
                <?php wp_link_pages(); ?>
                <div class="commentblock">
                <?php comments_template(); ?>
                </div><!--commentblock-->
                <?php endwhile; ?>
                
	<?php /* Display navigation to next/previous pages when applicable */ ?>
    <?php if (  $wp_query->max_num_pages > 1 ) : ?>
                    <div id="nav-below" class="navigation">
                        <div class="nav-next"><?php next_posts_link( __( 'Next <span class="meta-nav">&rarr;</span>', 'CleanStyleOne' ) ); ?></div>
                        <div class="nav-prev"><?php previous_posts_link( __( '<span class="meta-nav">&larr;</span> Previous', 'CleanStyleOne' ) ); ?></div>
                    </div><!-- #nav-below -->
    <?php endif; ?>              

<?php else : ?>
				<div id="post-0" class="post no-results not-found">
					<h2 class="entry-title"><?php _e( 'Nothing Found', 'CleanStyleOne' ); ?></h2>
					<div class="entry-content">
						<p><?php _e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'twentyten' ); ?></p>
						<?php get_search_form(); ?>
					</div><!-- .entry-content -->
				</div><!-- #post-0 -->
<?php endif; ?>
            </div>
          </td>
          
        <?php  if ( is_active_sidebar( 'rightsidebar' ) ) : ?>
          
          <td width="1%" valign="top" id="rightbar">
          	<div class="rightSideBar">
            	<?php if ( !dynamic_sidebar('rightsidebar') ) : endif; ?>
            </div>
          </td>
		          
        <?php endif; ?>	
          
        </tr>
      </table>
    <?php if ( !dynamic_sidebar('belowcontent') ) : endif; ?>      
    </div>
<?php get_footer(); ?>