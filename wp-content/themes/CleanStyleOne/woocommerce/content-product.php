<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

global $product, $woocommerce_loop;

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) )
	$woocommerce_loop['loop'] = 0;

// Paul C - get value from the catalogue options theme extention for the number of columns.
if( get_option('customWooStylePLcols') ) {
	$noCols = get_option('customWooStylePLcols');
	$woocommerce_loop['columns'] = $noCols;
}


// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) )
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );

// Ensure visibilty
if ( ! $product->is_visible() )
	return;

// Increase loop count
$woocommerce_loop['loop']++;
?>

<li class="product <?php
	if ( $woocommerce_loop['loop'] % $woocommerce_loop['columns'] == 0 )
		echo 'last';
	elseif ( ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] == 0 )
		echo 'first';
	?>">
    
	<?php do_action( 'woocommerce_before_shop_loop_item' ); ?>

	<?php if ($product->is_on_sale()) : ?>
        <?php echo apply_filters('woocommerce_sale_flash', '<span class="onsale">'.__('Sale!', 'woocommerce').'</span>', $post, $product); ?>
    <?php endif; ?>    

    <a href="<?php the_permalink(); ?>">

    <?php if (get_option('customWooStylePLShowThumbFirst') != 1 ) { ?>
        <h3><?php the_title(); ?></h3>
        <?php woocommerce_template_loop_product_thumbnail(); ?>
    <?php } else {?>
        <?php woocommerce_template_loop_product_thumbnail(); ?>
        <h3><?php the_title(); ?></h3>
    <?php } //end if  get_option('customWooStylePLShowThumbFirst') ?>            
    </a> 
    
    <div class="woo-short-desc">
    <?php echo apply_filters( 'woocommerce_short_description', $post->post_excerpt ) ?>
    </div>
    <?php woocommerce_get_template('loop/price.php'); ?>
    
	<?php if ( $product->is_type( array( 'simple', 'variable' ) ) && get_option('woocommerce_enable_sku') == 'yes' && $product->get_sku() ) : ?>
		<span style="margin-bottom:10px;" itemprop="productID" class="sku"><?php _e('SKU:', 'woocommerce'); ?> <?php echo $product->get_sku(); ?>.</span>

	<?php endif; ?>
             
      
    <?php woocommerce_get_template('loop/add-to-cart.php'); ?>

</li>