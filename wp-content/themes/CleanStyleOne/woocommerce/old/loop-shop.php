<?php

global $woocommerce_loop;

$woocommerce_loop['loop'] = 0;
$woocommerce_loop['show_products'] = true;

if( get_option('customWooStylePLcols') ) {
	$noCols = get_option('customWooStylePLcols');
	$woocommerce_loop['columns'] = $noCols;
}


if (!isset($woocommerce_loop['columns']) || !$woocommerce_loop['columns']) $woocommerce_loop['columns'] = apply_filters('loop_shop_columns', 4);

?>

<?php do_action('woocommerce_before_shop_loop'); ?>

<ul class="products">

	<?php 
	
	do_action('woocommerce_before_shop_loop_products');
	
	if ($woocommerce_loop['show_products'] && have_posts()) : while (have_posts()) : the_post(); 
	
		global $product;
		
		if (!$product->is_visible()) continue; 
		
		$woocommerce_loop['loop']++;
		
		?>
		<li class="product <?php if ($woocommerce_loop['loop']%$woocommerce_loop['columns']==0) echo ' last'; if (($woocommerce_loop['loop']-1)%$woocommerce_loop['columns']==0) echo ' first'; ?>">
			
			<?php do_action('woocommerce_before_shop_loop_item'); ?>
			
			<a href="<?php the_permalink(); ?>">

            <?php if (get_option('customWooStylePLShowThumbFirst') != 1 ) { ?>
				<h3><?php the_title(); ?></h3>
				<?php woocommerce_template_loop_product_thumbnail(); ?>
            <?php } else {?>
				<?php woocommerce_template_loop_product_thumbnail(); ?>
				<h3><?php the_title(); ?></h3>
            <?php } //end if  get_option('customWooStylePLShowThumbFirst') ?>            
			</a>   
            
            <div class="woo-short-desc">
                <?php echo apply_filters( 'woocommerce_short_description', $post->post_excerpt ) ?>
                <?php //woocommerce_template_single_excerpt(); ?>
             </div>
				<?php woocommerce_get_template('loop/price.php'); ?>                
			
			<?php woocommerce_get_template('loop/add-to-cart.php'); ?>
			
		</li><?php 
		
	endwhile; endif;
	
	if ($woocommerce_loop['loop']==0) echo '<li class="info">'.__('No products found which match your selection.', 'woocommerce').'</li>'; 

	?>

</ul>

<div class="clear"></div>

<?php do_action('woocommerce_after_shop_loop'); ?>