<?php
/**
 * The FOOTER for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage CleanStyleOne \
 * @since CleanStyleOne 1.0
 */
?>
    <div id="footer">
    
        <?php if ( !dynamic_sidebar('footer_widget') ) : endif; ?>
		<?php // ---------- Check if Footer menu is enabled  ------
        if( get_option('custom_style_footerMenuEnabled') ) :    
            wp_nav_menu( array( 'container_id' => 'footermenu', 'theme_location' => 'footer' ) ); 
        endif;
        ?>             
    
    
		<div id="copyright">
		  <?php bloginfo('name'); ?> &copy; <?php echo date('Y ');  ?>
            <div id="company-byline">
				<?php if (is_front_page()){ ?>
                    <a href="http://www.searchnetworx.co.za" target="_blank">Online marketing</a> by SearchNetworx
                <?php } else { ?>   
                    Online marketing by SearchNetworx
                <?php } ?> 
            </div>
      </div>
        <?php //if ( !dynamic_sidebar('footer_widget') ) : endif; ?>
        <div class="clear"></div>
    </div>
  </div>
</div>
<?php wp_footer(); ?>
</body>
</html>