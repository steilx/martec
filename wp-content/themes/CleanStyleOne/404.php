<?php
/*
 404 error page
*/
?>
<?php get_header(); ?>  
    <div id="main">
    <?php if ( !dynamic_sidebar('abovecontent') ) : endif; ?>        
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
        <?php  if ( is_active_sidebar( 'leftsidebar' ) || get_option('custom_style_sideMenuEnabled') ) : ?>
          
          <td width="1%" valign="top" id="leftbar">
          	<div class="leftSideBar">

				<?php // ---------- Check if Side menu is enabled  ------
                if( get_option('custom_style_sideMenuEnabled') ) :    
                    wp_nav_menu( array( 'container_id' => 'sidemenu', 'theme_location' => 'side' ) ); 
                endif;
                ?>                   
            
            	<?php if ( !dynamic_sidebar('leftsidebar') ) : endif; ?>
            </div>
          </td>
		          
        <?php endif; ?>	
        
          <td width="99%" valign="top">
          	<div id="content">

				<h1>Not found</h1>
                
                <p>Oops, seems like the page or post you are looking for no longer exists...</p>
                
                <p>Please use the navigation to locate the correct information.</p>
                
                <p>Or click here to go back to the <a href="<?php bloginfo('url'); ?>" title="<?php bloginfo('name'); ?>">home page</a> </p>

            </div>
          </td>
          
        <?php  if ( is_active_sidebar( 'rightsidebar' ) ) : ?>
          
          <td width="1%" valign="top" id="rightbar">
          	<div class="rightSideBar">
            	<?php if ( !dynamic_sidebar('rightsidebar') ) : endif; ?>
            </div>
          </td>
		          
        <?php endif; ?>	
          
        </tr>
      </table>
    <?php if ( !dynamic_sidebar('belowcontent') ) : endif; ?>          
    </div>
<?php get_footer(); ?>