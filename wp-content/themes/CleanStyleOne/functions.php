<?php

add_action( 'after_setup_theme', 'CleanStyleOne_setup' );


if ( ! function_exists( 'CleanStyleOne_setup' ) ):

function CleanStyleOne_setup() {

	// This theme styles the visual editor with editor-style.css to match the theme style.
	add_editor_style();
	
	// Load up our theme options page and related code.
	require( dirname( __FILE__ ) . '/inc/logo.php' );
	require( dirname( __FILE__ ) . '/inc/favicon.php' );
	require( dirname( __FILE__ ) . '/inc/styles.php' );	
	require( dirname( __FILE__ ) . '/mobi/mobile.php' );		
	

	// Check if WooCommerce is enabled to add WooCommerce theme extention
	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	if (is_plugin_active('woocommerce/woocommerce.php')) {
			require( dirname( __FILE__ ) . '/inc/woothemesettings.php' );	
	}

	// Post Format support. You can also use the legacy "gallery" or "asides" (note the plural) categories.
	add_theme_support( 'post-formats', array( 'aside', 'gallery' ) );

	// This theme uses post thumbnails
	add_theme_support( 'post-thumbnails' );

	// Add default posts and comments RSS feed links to head
	add_theme_support( 'automatic-feed-links' );
	
	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Navigation', 'CleanStyleOne' ),
		'side' => __( 'Side Navigation', 'CleanStyleOne' ),
		'footer' => __( 'Footer Navigation', 'CleanStyleOne' ),			
	) );

	// This theme allows users to set a custom background
	add_custom_background();

	// Your changeable header business starts here
	if ( ! defined( 'HEADER_TEXTCOLOR' ) )
		define( 'HEADER_TEXTCOLOR', '' );

	// No CSS, just IMG call. The %s is a placeholder for the theme template directory URI.
	if ( ! defined( 'HEADER_IMAGE' ) )
		define( 'HEADER_IMAGE', '%s/images/header.jpg' );

	// The height and width of your custom header. You can hook into the theme's own filters to change these values.
	// Add a filter to twentyten_header_image_width and twentyten_header_image_height to change these values.
	define( 'HEADER_IMAGE_WIDTH', apply_filters( 'CleanStyleOne_header_image_width', 960 ) );
	define( 'HEADER_IMAGE_HEIGHT', apply_filters( 'CleanStyleOne_header_image_height', 275 ) );

	// We'll be using post thumbnails for custom header images on posts and pages.
	// We want them to be 940 pixels wide by 198 pixels tall.
	// Larger images will be auto-cropped to fit, smaller ones will be ignored. See header.php.
	set_post_thumbnail_size( HEADER_IMAGE_WIDTH, HEADER_IMAGE_HEIGHT, true );

	// Don't support text inside the header image.
	if ( ! defined( 'NO_HEADER_TEXT' ) )
		define( 'NO_HEADER_TEXT', true );

	// Add a way for the custom header to be styled in the admin panel that controls
	// custom headers. See twentyten_admin_header_style(), below.
	add_custom_image_header( '', 'CleanStyleOne_admin_header_style' );

	// ... and thus ends the changeable header business.

	// Default custom headers packaged with the theme. %s is a placeholder for the theme template directory URI.
/*	
	register_default_headers( array(
		'default' => array(
			'url' => '%s/images/header.jpg',
			'thumbnail_url' => '%s/images/header_thumb.jpg',
			// translators: header image description
			'description' => __( 'Default', 'CleanStyleOne' )
		)
	) );
	*/
}
endif;

// remove feeds
remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
remove_action( 'wp_head', 'rsd_link' ); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action( 'wp_head', 'wlwmanifest_link' ); // Display the link to the Windows Live Writer manifest file.
remove_action( 'wp_head', 'index_rel_link' ); // index link
remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); // prev link
remove_action( 'wp_head', 'start_post_rel_link', 10, 0 ); // start link
remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 ); // Display relational links for the posts adjacent to the current post.
remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version

add_action('wp_head', 'addBackPostFeed');
function addBackPostFeed() {
    echo '<link rel="alternate" type="application/rss+xml" title="RSS 2.0 Feed" href="'.get_bloginfo('rss2_url').'" />'; 
}



if ( ! function_exists( 'CleanStyleOne_admin_header_style' ) ) :
/**
 * Styles the header image displayed on the Appearance > Header admin panel.
 *
 * Referenced via add_custom_image_header() in twentyten_setup().
 *
 * @since Twenty Ten 1.0
 */
function CleanStyleOne_admin_header_style() {
?>
<style type="text/css">
/* Shows the same border as on front end */
#headimg {
	border-bottom: 1px solid #000;
	border-top: 4px solid #000;
}
/* If NO_HEADER_TEXT is false, you would style the text with these selectors:
	#headimg #name { }
	#headimg #desc { }
*/
</style>
<?php
}
endif;

/**
 * Sets the post excerpt length to 40 characters.
 *
 * To override this length in a child theme, remove the filter and add your own
 * function tied to the excerpt_length filter hook.
 *
 * @since Twenty Ten 1.0
 * @return int
 */
function CleanStyleOne_excerpt_length( $length ) {
	return 40;
}
add_filter( 'excerpt_length', 'CleanStyleOne_excerpt_length' );


/**
 * Returns a "Continue Reading" link for excerpts
 *
 * @since Twenty Ten 1.0
 * @return string "Continue Reading" link
 */
function CleanStyleOne_continue_reading_link() {
	return '<p><a class="more-link" href="'. get_permalink() . '">' . __( 'more <span class="meta-nav">...</span>', 'CleanStyleOne' ) . '</a></p>';
}

/**
 * Replaces "[...]" (appended to automatically generated excerpts) with an ellipsis and twentyten_continue_reading_link().
 *
 * To override this in a child theme, remove the filter and add your own
 * function tied to the excerpt_more filter hook.
 *
 * @since Twenty Ten 1.0
 * @return string An ellipsis
 */
function CleanStyleOne_auto_excerpt_more( $more ) {
	return ' &hellip;' . CleanStyleOne_continue_reading_link();
}
add_filter( 'excerpt_more', 'CleanStyleOne_auto_excerpt_more' );

/**
 * Adds a pretty "Continue Reading" link to custom post excerpts.
 *
 * To override this link in a child theme, remove the filter and add your own
 * function tied to the get_the_excerpt filter hook.
 *
 * @since Twenty Ten 1.0
 * @return string Excerpt with a pretty "Continue Reading" link
 */
function CleanStyleOne_custom_excerpt_more( $output ) {
	if ( has_excerpt() && ! is_attachment() ) {
		$output .= CleanStyleOne_continue_reading_link();
	}
	return $output;
}
add_filter( 'get_the_excerpt', 'CleanStyleOne_custom_excerpt_more' );


function CleanStyleOne_widgets_init() {

register_sidebar( array(
'name' => __('Header widget','CleanStyleOne'),
'id' => 'header_widget',
'description' => __( 'Header widget','CleanStyleOne'),
'before_widget' => '<div id="%1$s" class="%2$s">',
'after_widget' => "</div>",
'before_title' => '',
'after_title' => '',
) );

register_sidebar( array(
'name' => __('Logo widget','CleanStyleOne'),
'id' => 'logo_widget',
'description' => __( 'Logo widget','CleanStyleOne'),
'before_widget' => '<div id="%1$s" class="%2$s">',
'after_widget' => "</div>",
'before_title' => '',
'after_title' => '',
) );

register_sidebar( array(
'name' => __('Header Image Widget','CleanStyleOne'),
'id' => 'header_image_widget',
'description' => __( 'Header Image widget','CleanStyleOne'),
'before_widget' => '<div id="%1$s" class="%2$s">',
'after_widget' => "</div>",
'before_title' => '',
'after_title' => '',
) );

register_sidebar( array(
'name' => __('Left Side Bar','CleanStyleOne'),
'id' => 'leftsidebar',
'description' => __( 'Left Side Bar','CleanStyleOne'),
'before_widget' => '<div id="%1$s" class="box %2$s">',
'after_widget' => "</div>",
'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
) );

register_sidebar( array(
'name' => __('Right Side Bar','CleanStyleOne'),
'id' => 'rightsidebar',
'description' => __( 'Right Side Bar','CleanStyleOne'),
'before_widget' => '<div id="%1$s" class="box %2$s">',
'after_widget' => "</div>",
'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
) );

register_sidebar( array(
'name' => __('Above content','CleanStyleOne'),
'id' => 'abovecontent',
'description' => __( 'Above Content','CleanStyleOne'),
'before_widget' => '<div id="%1$s" class="%2$s">',
'after_widget' => "</div>",
'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
) );

register_sidebar( array(
'name' => __('Below Content','CleanStyleOne'),
'id' => 'belowcontent',
'description' => __( 'Below content','CleanStyleOne'),
'before_widget' => '<div id="%1$s" class="%2$s">',
'after_widget' => "</div>",
'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
) );

register_sidebar( array(
'name' => 'Footer Widget',
'id' => 'footer_widget',
'description' => __( 'Footer Widget'),
'before_widget' => '<div id="%1$s" class="footer-item %2$s">',
'after_widget' => "</div>",
'before_title' => '<span class="footer-widget-title">',
'after_title' => '</span>',
) );

}


// Add the widget areas
add_action( 'widgets_init', 'CleanStyleOne_widgets_init' );


?>