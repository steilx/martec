<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage CleanStyleOne
 * @since CleanStyleOne Standard 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'CleanStyleOne' ), max( $paged, $page ) );

	?></title>
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />

<?php wp_head(); ?>

<?php 
	// Custom theme options

	//	Add Custom StyleSheet in uploaded
	if( get_option('enable_custom_style_sheet') && ( $styleSheetID = intval( get_option('custom_style_sheet_id')  ) ) && $styleSheetSrc = wp_get_attachment_url( $styleSheetID) ) :
			echo '<link rel="stylesheet" type="text/css" href="'.$styleSheetSrc.'" />';
	endif;
	
	//	Add custom FavIcon if uploaded
	if( get_option('custom_favicon_enabled') && ($faviconID = get_option('custom_favicon_image_id') ) ):
		if ( ( $faviconID = intval( $faviconID ) ) && $faviconSrc = wp_get_attachment_image_src( $faviconID, '') )  :
			echo '<link rel="shortcut icon" type="image/x-icon" href="'. $faviconSrc[0].'" />';
		endif;
	endif;	

	//	Add Custom CSS in header
	if( get_option('custom_style_enabled') ) :
		get_custom_styles();
	endif;
	
	//	Add Custom Woo Stylesheet
	if( function_exists ('get_custom_woo_stylesheet') && get_option('custom_woo_stylesheet') ) : 
		get_custom_woo_stylesheet();
	endif;
	
	//	Add Custom Woo CSS un header
	if( function_exists ('get_custom_woo_styles') && get_option('customWooEnabled')) : 
		get_custom_woo_styles();
	endif;		
?>

<!--[if lte IE 6]>
<style type="text/css">
    img, div, li, ul, body { behavior: url(<?php bloginfo( 'template_url' ); ?>/ie/iepngfix.htc) }
</style>
<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/ie/iepngfix_tilebg.js"></script>
<script>
sfHover = function() {
	var sfEls = document.getElementById("menu").getElementsByTagName("LI");
	for (var i=0; i<sfEls.length; i++) {
		sfEls[i].onmouseover=function() {
			this.className+=" sfhover";
		}
		sfEls[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" sfhover\\b"), "");
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", sfHover);
</script>
 
<![endif]-->


</head>
<body <?php body_class(); ?>>

<div id="wrapper">
  <div id="container">
    <div id="top">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td valign="middle">
        
<?php // ---------- Adding the custom LOGO image ------------------
if( get_option('custom_logo_enabled') && ($logoID = get_option('custom_logo_image_id')) ): 
	if ( ( $logoID = intval( $logoID ) ) && $logoSrc = wp_get_attachment_image_src( $logoID, '') ) :
?>
      	<div id="logo">
      		<a href="<?php bloginfo('url'); ?>">
			<img src="<?php echo $logoSrc[0]; ?>" width="<?php echo $logoSrc[1]; ?>" height="<?php echo $logoSrc[2]; ?>" alt="<?php bloginfo( 'name' )?>" />
            </a>
      	</div>
<?php 
	endif;
else :

?>
		<div id="logo">
		<h1><a href="<?php bloginfo('url'); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
        </div>
<?php endif; ?>
<?php  if ( is_active_sidebar( 'logo_widget' ) ) : ?>
  
  <div id="logo_widget">
		<?php if ( !dynamic_sidebar('logo_widget') ) : endif; ?>
	</div>
		  
<?php endif; ?>	      
        
        </td>
        <td align="right" valign="middle">

        <?php  if ( is_active_sidebar( 'header_widget' ) ) : ?>
          
       	  <div id="header_widget">
            	<?php if ( !dynamic_sidebar('header_widget') ) : endif; ?>
            </div>
		          
        <?php endif; ?>	          
        
        </td>
      </tr>
    </table>
</div>
    
<?php // ---------- Check if TOP is enabled and also if MOBILE setting is enabled ------ //
if( get_option('custom_style_topMenuEnabled') && !get_option('mobile_options_enabled')) :    
	wp_nav_menu( array( 'container_id' => 'menu', 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); 
	elseif ( get_option('custom_style_topMenuEnabled') && get_option('mobile_options_enabled')) : 
	?>
 
			<div id="menu" class="navbar">
				<nav id="site-navigation" class="navigation main-navigation" role="navigation">
					<h4 class="menu-toggle"><?php echo get_option('mobile_options_menuText'); ?></h4>
					<a class="screen-reader-text skip-link" href="#content" title="<?php esc_attr_e( 'Skip to content', 'twentythirteen' ); ?>"><?php _e( 'Skip to content', 'twentythirteen' ); ?></a>
					<?php wp_nav_menu( array( 'container_id' => 'nav-menu', 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
    
				</nav><!-- #site-navigation -->
			</div><!-- #navbar -->
    
    <?php endif; ?>
<?php // ---------- Check if Header is enabled  ------
if( get_option('custom_style_HeaderImage') || get_option('customWooShowHeader') ) : 
		if (is_page_template( 'page-no-header.php' )) {
			echo '<div id="header_small"></div>';
		} else {
?>

  	<div id="header">
		<?php
        // Check if this is a post or page, if it has a thumbnail, and if it's a big one
        if ( is_singular() && current_theme_supports( 'post-thumbnails' ) &&
                has_post_thumbnail( $post->ID ) &&
                ( /* $src, $width, $height */ $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumbnail' ) ) &&
                $image[1] >= HEADER_IMAGE_WIDTH ) :
            // Houston, we have a new header image!
            echo get_the_post_thumbnail( $post->ID );
			if( get_option('custom_style_HeaderShaddow') ) : 
				echo '<div class="shaddow"></div>';
			endif;
		elseif ( function_exists('easing_slider') && get_option('activation') == 'enable' ) : 
			easing_slider();
		elseif ( is_active_sidebar( 'header_image_widget' ) || is_active_sidebar( 'catalog_header_image_widget' )) : 
			if ( get_option('customWooShowHeader') && is_active_sidebar( 'catalog_header_image_widget' ) && function_exists(is_woocommerce) && is_woocommerce()) :
				!dynamic_sidebar('catalog_header_image_widget');
			else :
				!dynamic_sidebar('header_image_widget');
			endif;	
			
        else : ?>
            <img src="<?php header_image(); ?>" width="<?php echo HEADER_IMAGE_WIDTH; ?>" height="<?php echo HEADER_IMAGE_HEIGHT; ?>" alt="" />
		<?php	if( get_option('custom_style_HeaderShaddow') ) : 
				echo '<div class="shaddow"></div>';
			endif;
		?>
        <?php endif; ?>
    </div>
    <?php } ?>
<?php endif; ?>	