<?php
// ---------- "Child Theme Options" menu STARTS HERE
 

 /*
function logo_add_admin() {
	add_submenu_page('themes.php', 'Logo', 'Logo', 'edit_themes', basename(__FILE__), 'logo_admin');
}
/*/

function logo_add_admin() {
	$theme_page = add_theme_page(
		__( 'Custom logo', 'CleanStyleOne' ),   	// Name of page
		__( 'Custom logo', 'CleanStyleOne' ),   	// Label in menu
		'edit_theme_options',				// Capability required
		'logo_options',                     // Menu slug, used to uniquely identify the page
		'logo_admin' 						// Function that renders the options page
	);

	if ( ! $theme_page )
		return;
}

add_action('admin_menu' , 'logo_add_admin');
 
function logo_admin() {
 
	$custom_logo_image_id = get_option('custom_logo_image_id'); 
	$custom_logo_enabled = get_option('custom_logo_enabled');

	if (isset($_POST['delete-logo']))
	{
		wp_delete_attachment($custom_logo_image_id, true);
		update_option('custom_logo_image_id', false);	
	}
 
	if ($_POST['options-submit'] && !isset($_POST['delete-logo']))
	{
		$custom_logo_enabled = htmlspecialchars($_POST['custom_logo_enabled']);
		update_option('custom_logo_enabled', $custom_logo_enabled); 
		
                $file_type = $_FILES['logo_image']['type'];
                
                if($file_type=='image/gif' || $file_type=='image/jpeg' || $file_type=='image/pjpeg' || $file_type=='image/png')
		{
			$id = media_handle_upload('logo_image',0);
			if ( is_wp_error($id) ) 
			{
			 	 echo '<div class="error-div">
			        	<a class="dismiss" href="#" onclick="jQuery(this).parents(\'div.media-item\').slideUp(200, function(){jQuery(this).remove();});">' . __('Dismiss') . '</a>
        				<strong>' . sprintf(__('&#8220;%s&#8221; has failed to upload due to an error'), esc_html($_FILES['logo_image']['name']) ) . '</strong><br />' .
        				esc_html($id->get_error_message()) . '</div>';
        		}
			else
			{
				$custom_logo_image_id = $id;
				update_option('custom_logo_image_id', $id);
				echo '<div class="updated"><p>Your new options have been successfully saved.</p></div>';	
			}
		}
		/*
		$file_name = $_FILES['logo_image']['name'];
		$temp_file = $_FILES['logo_image']['tmp_name'];
		$file_type = $_FILES['logo_image']['type'];
		
		if($file_type=="image/gif" || $file_type=="image/jpeg" || $file_type=="image/pjpeg" || $file_type=="image/png"){
			if (file_exists($temp_file))
			{
				//$fd = fopen($temp_file,�rb�);
				$file_content=file_get_contents($temp_file);
				//fclose($fd);
			}
 
			$wud = wp_upload_dir();
 
			if (file_exists($wud[path].'/'.strtolower($file_name))){
				unlink ($wud[path].'/'.strtolower($file_name));
			}
 
			$upload = wp_upload_bits( $file_name, '', $file_content);
		//	echo $upload['error'];
 
			$custom_logo_image = $wud[url].'/'.strtolower($file_name);
			update_option('custom_logo_image', $custom_logo_image);
		}
		
 
		?>
			<div class="updated"><p>Your new options have been successfully saved.</p></div>
		<?php

		*/
 
	}
 
	if($custom_logo_enabled) $checked='checked="checked"';
 
	?>
		<div class="wrap">
			<div id="icon-themes" class="icon32"></div>
			<h2>Upload a logo</h2>
			<form name="theform" method="post" enctype="multipart/form-data" action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']);?>">
				<table class="form-table">
                
                
					<tr>
						<td width="200">Enabled?</td>
						<td><input name="custom_logo_enabled" type="checkbox" value="1" <?php echo $checked; ?>/></td>
					</tr>
                   
                    
					<tr>
						<td>Current image:</td>
						<td bgcolor="#e0e0e0">
						<?php
 						if ( ( $logoID = intval( $custom_logo_image_id ) ) && $logoSrc = wp_get_attachment_image_src( $logoID, '') ) :
						?>
                        				<img src="<?php echo $logoSrc[0]; ?>" width="<?php echo $logoSrc[1]; ?>" height="<?php echo $logoSrc[2]; ?>" alt="<?php bloginfo( 'name' )?>" />
							<input type="submit" name="delete-logo" value="Delete Logo" />
						<?php
						else :
						?>
							No image uploaded yet 
						<?php
		        			endif;
						?>
                                
                          		</td>
					</tr>
					<tr>
						<td>Logo image to use (gif/jpeg/png):</td>
						<td><input type="file" name="logo_image"><br />(you must have writing permissions for your uploads directory)</td>
					</tr>
				</table>
				<input type="hidden" name="options-submit" value="1" />
				<p class="submit"><input type="submit" name="submit" value="Save Options" /></p>
		  </form>
		</div>
<?php
}
 
// ---------- "Child Theme Options" menu ENDS HERE
