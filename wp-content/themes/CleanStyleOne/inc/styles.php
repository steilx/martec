<?php
/**
 * 	Child Theme: Styling options
 *	Author: Paul Combrink @ Interface
 *	Date: 2011-06-24
 */
 
 /* Enque styles and scripts */
 
function custom_styles_enqueue() {
	wp_enqueue_style( 'farbtastic' );
	wp_enqueue_script( 'farbtastic' );
	
	wp_register_script( 'farbtastic-instance', get_template_directory_uri() . '/inc/farbtastic-instance.js',array( 'farbtastic', 'jquery' ),'0.1',true);	
	wp_enqueue_script( 'farbtastic-instance' );
	
	wp_register_style( 'custom-options-style', get_template_directory_uri() . '/inc/theme-extention.css',__FILE__,'0.1');	
	wp_enqueue_style( 'custom-options-style' );		
}

add_action('admin_enqueue_scripts', 'custom_styles_enqueue');

function custom_styles_options_add_page() {
	$theme_page = add_theme_page(
		__( 'Theme Options', 'CleanStyleOne' ),   		// Name of page
		__( 'Theme Options', 'CleanStyleOne' ),   	// Label in menu
		'edit_theme_options',					// Capability required
		'styles_options',                       // Menu slug, used to uniquely identify the page
		'styles_admin' 							// Function that renders the options page
	);

	if ( ! $theme_page )
		return;
}

add_action('admin_menu' , 'custom_styles_options_add_page');

function styles_admin()  
{

       $custom_font_list = array (
                        0 =>    "",
                        1 =>    "Verdana, Geneva, sans-serif",
                        2 =>    "Georgia, 'Times New Roman', Times, serif",
                        3 =>    "'Courier New', Courier, monospace",
                        4 =>    "Arial, Helvetica, sans-serif",
                        5 =>    "'Trebuchet MS', Arial, Helvetica, sans-serif",
                        6 =>    "'Arial Black', Gadget, sans-serif",
                        7 =>    "'Times New Roman', Times, serif",
                        8 =>    "'Palatino Linotype', 'Book Antiqua', Palatino, serif",
                        9 =>    "'Lucida Sans Unicode', 'Lucida Grande', sans-serif",
                        10 =>   "'MS Serif', 'New York', serif",
                        11 =>   "'Century Gothic', Arial, sans-serif",
                        12 =>   "'Muli',Arial, sans-serif",
                        13 =>   "'Didact Gothic', arial, serif",
                        14 =>   "'Zeyada', arial, serif",
                        15 =>   "'Nunito', arial, serif",
                );

       $custom_font_size = array (
                        0 =>    '',
                        1 =>    '10px',
                        2 =>    '11px',
                        3 =>    '12px',
                        4 =>    '13px',
                        5 =>    '14px',
                        6 =>    '15px',
                        7 =>    '16px',
                        8 =>    '17px',
                        9 =>    '18px',
                        10 =>   '20px',
                        11 =>   '24px',
                        12 =>   '30px',
                        13 =>   '36px',
                        14 =>   '40px',
                        15 =>   '50px',						
                );

    $enable_custom_style_sheet = get_option('enable_custom_style_sheet');
    $enabled = get_option('custom_style_enabled');
    $custom_style_sheet_id = get_option('custom_style_sheet_id');

    if (isset($_POST['options-submit']))
    {

	if (!isset($_POST['enable_custom_style_sheet']) && !isset($_POST['enabled']))
	{
                update_option('enable_custom_style_sheet', false);
                update_option('custom_style_enabled', false);
	} 

        if (isset($_POST['delete-css-style-sheet']))
        {
                wp_delete_attachment($custom_style_sheet_id, true);
                update_option('custom_style_sheet_id', false);
        }


	if (isset($_POST['enable_custom_style_sheet']) || !empty($_FILES['custom_style_sheet_file']['tmp_name']))
	{
               if (is_uploaded_file($_FILES['custom_style_sheet_file']['tmp_name']))
               {
                        $file_type = $_FILES['custom_style_sheet_file']['type'];
                        
			if ($file_type == 'text/css')
                        {
                            $id = media_handle_upload('custom_style_sheet_file',0);
                            if ( is_wp_error($id) )
			    {
                                  echo '<div class="error-div">
                                       <a class="dismiss" href="#" onclick="jQuery(this).parents(\'div.media-item\').slideUp(200, function(){jQuery(this).remove();});">' . __('Dismiss') . '</a>
                                       <strong>' . sprintf(__('&#8220;%s&#8221; has failed to upload due to an error'), esc_html($_FILES['custom_style_sheet_file']['name']) ) . '</strong><br />' .
                                       esc_html($id->get_error_message()) . '</div>';
			    }
                            else
                            {
                                  $custom_style_sheet_id = $id;
                                  update_option('custom_style_sheet_id', $id);
                                  echo '<div class="updated"><p>Your new options have been successfully saved.</p></div>';
                            }
			}
			else
				$error = 'Please upload a valid CSS style sheet';
		}

		if (isset($_POST['enable_custom_style_sheet']))
		{		
			$enable_custom_style_sheet = htmlspecialchars($_POST['enable_custom_style_sheet']);
                      	update_option('enable_custom_style_sheet', $enable_custom_style_sheet);	
			update_option('custom_style_enabled', false);
                        $enabled = false;
		}
		
	}
	else
	{
		if (isset($_POST['enabled']))
		{
			update_option('enable_custom_style_sheet',false);
			$enable_custom_style_sheet = false;
		}

	
		// ---- Add arrays to DB  --------------------------
		update_option('custom_font_list', $custom_font_list);
		update_option('custom_font_size', $custom_font_size);
	
		$enabled = htmlspecialchars($_POST['enabled']);
		update_option('custom_style_enabled', $enabled);
		
		// ---- Menu opions --------------------------		

		$custom_style_topMenuEnabled = $_POST['custom_style_topMenuEnabled'];
		update_option('custom_style_topMenuEnabled', $custom_style_topMenuEnabled);
		
		$custom_style_sideMenuEnabled = $_POST['custom_style_sideMenuEnabled'];
		update_option('custom_style_sideMenuEnabled', $custom_style_sideMenuEnabled);
		
		$custom_style_footerMenuEnabled = $_POST['custom_style_footerMenuEnabled'];
		update_option('custom_style_footerMenuEnabled', $custom_style_footerMenuEnabled);		

		// ---- Header bg AND Comments --------------------------		
		$custom_style_CommentEnabled = $_POST['custom_style_CommentEnabled'];
		update_option('custom_style_CommentEnabled', $custom_style_CommentEnabled);

		$custom_style_HeaderImage = $_POST['custom_style_HeaderImage'];
		update_option('custom_style_HeaderImage', $custom_style_HeaderImage);	

		$custom_style_HeaderShaddow = $_POST['custom_style_HeaderShaddow'];
		update_option('custom_style_HeaderShaddow', $custom_style_HeaderShaddow);	
		
		// ---- Link --------------------------		
		$custom_style_aColor = $_POST['custom_style_aColor'];
		update_option('custom_style_aColor', $custom_style_aColor);
		
		// ---- Container background --------------------------
		$custom_style_ContainerBgColor = $_POST['custom_style_ContainerBgColor'];
		update_option('custom_style_ContainerBgColor', $custom_style_ContainerBgColor);		
		
		// ---- Menu --------------------------
		$custom_style_menuBgColor = $_POST['custom_style_menuBgColor'];
		update_option('custom_style_menuBgColor', $custom_style_menuBgColor);
		
		$custom_style_menuEffect = $_POST['custom_style_menuEffect'];
		update_option('custom_style_menuEffect', $custom_style_menuEffect);
		
		
		$custom_style_menuFontColor = $_POST['custom_style_menuFontColor'];
		update_option('custom_style_menuFontColor', $custom_style_menuFontColor);
		
		$custom_style_menuFontType = $_POST['custom_style_menuFontType'];
		update_option('custom_style_menuFontType', $custom_style_menuFontType);
		
		$custom_style_menuFontSize = $_POST['custom_style_menuFontSize'];
		update_option('custom_style_menuFontSize', $custom_style_menuFontSize);		
		
		
		// ---- Body & Content --------------------------
		
		$custom_style_BodyBgColor = $_POST['custom_style_BodyBgColor'];
		update_option('custom_style_BodyBgColor', $custom_style_BodyBgColor);
		
		$custom_style_bodyFontColor = $_POST['custom_style_bodyFontColor'];
		update_option('custom_style_bodyFontColor', $custom_style_bodyFontColor);
		
		$custom_style_bodyFontType = $_POST['custom_style_bodyFontType'];
		update_option('custom_style_bodyFontType', $custom_style_bodyFontType);		
		
		$custom_style_bodyFontSize = $_POST['custom_style_bodyFontSize'];
		update_option('custom_style_bodyFontSize', $custom_style_bodyFontSize);		

		// ---- Heading --------------------------	

		$custom_style_hFontType = $_POST['custom_style_hFontType'];
		update_option('custom_style_hFontType', $custom_style_hFontType);

		$custom_style_h1Color = $_POST['custom_style_h1Color'];
		update_option('custom_style_h1Color', $custom_style_h1Color);
		
		$custom_style_h1FontSize = $_POST['custom_style_h1FontSize'];
		update_option('custom_style_h1FontSize', $custom_style_h1FontSize);

		$custom_style_h2Color = $_POST['custom_style_h2Color'];
		update_option('custom_style_h2Color', $custom_style_h2Color);
		
		$custom_style_h2FontSize = $_POST['custom_style_h2FontSize'];
		update_option('custom_style_h2FontSize', $custom_style_h2FontSize);
		$custom_style_h3Color = $_POST['custom_style_h3Color'];
		update_option('custom_style_h3Color', $custom_style_h3Color);
		
		$custom_style_h3FontSize = $_POST['custom_style_h3FontSize'];
		update_option('custom_style_h3FontSize', $custom_style_h3FontSize);		
		
		// ---- Footer --------------------------		

		$custom_style_FooterBgColor = $_POST['custom_style_FooterBgColor'];
		update_option('custom_style_FooterBgColor', $custom_style_FooterBgColor);

		$custom_style_FooterFontColor = $_POST['custom_style_FooterFontColor'];
		update_option('custom_style_FooterFontColor', $custom_style_FooterFontColor);
		
		$custom_style_FooterFontType = $_POST['custom_style_FooterFontType'];
		update_option('custom_style_FooterFontType', $custom_style_FooterFontType);		
		
		$custom_style_FooterFontSize = $_POST['custom_style_FooterFontSize'];
		update_option('custom_style_FooterFontSize', $custom_style_FooterFontSize);		
		
		// ---- Widget Title --------------------------		

		$custom_style_WidgetTitleBgColor = $_POST['custom_style_WidgetTitleBgColor'];
		update_option('custom_style_WidgetTitleBgColor', $custom_style_WidgetTitleBgColor);
		
		$custom_style_TitleBgEffect = $_POST['custom_style_TitleBgEffect'];
		update_option('custom_style_TitleBgEffect', $custom_style_TitleBgEffect);		

		$custom_style_WidgetTitleFontColor = $_POST['custom_style_WidgetTitleFontColor'];
		update_option('custom_style_WidgetTitleFontColor', $custom_style_WidgetTitleFontColor);
		
		$custom_style_WidgetTitleFontType = $_POST['custom_style_WidgetTitleFontType'];
		update_option('custom_style_WidgetTitleFontType', $custom_style_WidgetTitleFontType);		
		
		$custom_style_WidgetTitleFontSize = $_POST['custom_style_WidgetTitleFontSize'];
		update_option('custom_style_WidgetTitleFontSize', $custom_style_WidgetTitleFontSize);	
		
		// ---- Widget Body --------------------------		

		$custom_style_WidgetBodyBgColor = $_POST['custom_style_WidgetBodyBgColor'];
		update_option('custom_style_WidgetBodyBgColor', $custom_style_WidgetBodyBgColor);

		$custom_style_WidgetBodyFontColor = $_POST['custom_style_WidgetBodyFontColor'];
		update_option('custom_style_WidgetBodyFontColor', $custom_style_WidgetBodyFontColor);
		
		$custom_style_WidgetBodyFontType = $_POST['custom_style_WidgetBodyFontType'];
		update_option('custom_style_WidgetBodyFontType', $custom_style_WidgetBodyFontType);		
		
		$custom_style_WidgetBodyFontSize = $_POST['custom_style_WidgetBodyFontSize'];
		update_option('custom_style_WidgetBodyFontSize', $custom_style_WidgetBodyFontSize);			

		// ---- Sidebar Body --------------------------				
		
		$custom_style_SidebarBodyBgColor = $_POST['custom_style_SidebarBodyBgColor'];
		update_option('custom_style_SidebarBodyBgColor', $custom_style_SidebarBodyBgColor);	

		$custom_style_SidebarLine = $_POST['custom_style_SidebarLine'];
		update_option('custom_style_SidebarLine', $custom_style_SidebarLine);	
		
		$custom_style_SidebarLineColor = $_POST['custom_style_SidebarLineColor'];
		update_option('custom_style_SidebarLineColor', $custom_style_SidebarLineColor);		

		// ---- Custom CSS --------------------------		
		
		$custom_style_CSS = $_POST['custom_style_CSS'];
		update_option('custom_style_CSS', $custom_style_CSS);

		?>
		<div class="updated"><p>Your new options have been successfully saved.</p></div>
		<?php
	} 
   }

   if($enabled) $checked='checked="checked"';
   if ($enable_custom_style_sheet) $custom_style_sheet_checked='checked="checked"';

   if (!empty($error))
   {
        echo '<div class="error-div">
              <a class="dismiss" href="#" onclick="jQuery(this).parents(\'div.media-item\').slideUp(200, function(){jQuery(this).remove();});">' . __('Dismiss') . '</a>
              ' . esc_html($error) . '</div>';
   }

?>


 
<link href="http://fonts.googleapis.com/css?family=Muli" rel="stylesheet" type="text/css">
<link href="http://fonts.googleapis.com/css?family=Didact Gothic" rel="stylesheet" type="text/css">
<link href='http://fonts.googleapis.com/css?family=Zeyada&v1' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Nunito&v1' rel='stylesheet' type='text/css'>
 
 
<link href="http://fonts.googleapis.com/css?family=Meddon" rel="stylesheet" type="text/css">
<link href="http://fonts.googleapis.com/css?family=Terminal Dosis Light" rel="stylesheet" type="text/css">
<link href='http://fonts.googleapis.com/css?family=Wire+One&v1' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Calligraffitti&v1' rel='stylesheet' type='text/css'>

<div class="wrap">
			<div id="icon-themes" class="icon32"></div>
            <h2>Theme Options</h2>
            
            <div id="mypicker">
              <div id="picker"></div>
            </div>
            
             
            <form name="theform" id="theform" method="post" enctype="multipart/form-data" action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']);?>">
			<!-- Submit button -------------------------------------->            
            <div style="float:left; clear:both;">             
            </div>
            <div class="optionheading">Menus</div>
            <div class="optionbox">
           	  <div class="optionitem">Top menu Enabled? <input name="custom_style_topMenuEnabled" type="checkbox" value="1" <?php if (get_option('custom_style_topMenuEnabled') == 1 ) { echo checked; } ?>/></div>
           	  <div class="optionitem">Side menu Enabled? <input name="custom_style_sideMenuEnabled" type="checkbox" value="1" <?php if (get_option('custom_style_sideMenuEnabled') == 1 ) { echo checked; } ?>/></div>   
           	  <div class="optionitem">Footer menu Enabled? <input name="custom_style_footerMenuEnabled" type="checkbox" value="1" <?php if (get_option('custom_style_footerMenuEnabled') == 1 ) { echo checked; } ?>/></div>
           	  <div class="optionitem" style="float:right; text-align:right;"><input type="submit" class="button-primary" name="submit" value="Save Options" /></div>                                                       
        	</div>
            
        <div class="optionheading">Header</div>
            <div class="optionbox">
           	  <div class="optionitem">Header image enabled?</div>
           	  <div class="optionitem"><input name="custom_style_HeaderImage" type="checkbox" value="1" <?php if (get_option('custom_style_HeaderImage') == 1 ) { echo checked; } ?>/></div>                                                                  
           	  <div class="optionitem">Show header shaddow</div>
           	  <div class="optionitem"><input name="custom_style_HeaderShaddow" type="checkbox" value="1" <?php if (get_option('custom_style_HeaderShaddow') == 1 ) { echo checked; } ?>/></div>
           	  <div class="optionitem" style="float:right; text-align:right;"><input type="submit" class="button-primary" name="submit" value="Save Options" /></div>                                                       
            </div>                 

      <div class="optionheading">Custom styles and colours</div>	       
      <div class="optionbox">      

	     <div class="optionheading">General</div>	 
            <div class="optionbox">
           	  <div class="optionitem"><strong>Enabled?</strong></div>
           	  <div class="optionitem"><input name="enabled" type="checkbox" value="1" <?php echo $checked; ?>/></div>
           	  <div class="optionitem">Show comment?</div>
           	  <div class="optionitem"><input name="custom_style_CommentEnabled" type="checkbox" value="1" <?php if (get_option('custom_style_CommentEnabled') == 1 ) { echo checked; } ?>/></div>
              <div class="clear"></div>
           	  <div class="optionitem">Link colour</div>
           	  <div class="optionitem"><input name="custom_style_aColor" class="colorwell "type="text" onfocus="if (this.value==''){this.value='#';}" onblur="if (this.value=='#'){ this.value='';}" id="custom_style_aColor" value="<?php echo (get_option('custom_style_aColor'))?>" /></div>              
           	  <div class="optionitem">Container background colour</div>
           	  <div class="optionitem"><input name="custom_style_ContainerBgColor" class="colorwell "type="text" onfocus="if (this.value==''){this.value='#';}" onblur="if (this.value=='#'){ this.value='';}" id="custom_style_ContainerBgColor" value="<?php echo (get_option('custom_style_ContainerBgColor'))?>" /></div>              
           	  <div class="optionitem" style="float:right; text-align:right;"><input type="submit" class="button-primary" name="submit" value="Save Options" /></div>                                                       
            </div>
            
            <div class="optionheading">Menu</div>
            <div class="optionbox">
           	  <div class="optionitem">Background colour</div>
           	  <div class="optionitem"><input name="custom_style_menuBgColor" class="colorwell" type="text" onfocus="if (this.value==''){this.value='#';}" onblur="if (this.value=='#'){ this.value='';}" id="custom_style_menuBgColor" value="<?php echo get_option('custom_style_menuBgColor')?>" /></div>
           	  <div class="optionitem">Gradient Effect? <input name="custom_style_menuEffect" type="checkbox" value="1" <?php if (get_option('custom_style_menuEffect') == 1 ) { echo checked; } ?>/></div>              
              <div class="clear"></div>
              <div class="optionitem">Font</div>              
              <div class="optionitem"><input name="custom_style_menuFontColor" class="colorwell" type="text" onfocus="if (this.value==''){this.value='#';}" onblur="if (this.value=='#'){ this.value='';}" id="custom_style_menuFontColor" value="<?php echo get_option('custom_style_menuFontColor')?>" /></div>
              <div class="optionitem">
              
                  <select name="custom_style_menuFontType" id="custom_style_menuFontType">
                    <option value="">-- please select --</option>
                    <?php 
							$selected = "";
							$arrCount = count($custom_font_list);
							$checker = get_option('custom_style_menuFontType');
							for ($i = 0;$i < $arrCount;$i++){
								
								if ($checker == $i ) {
									$selected = "selected";
								}
								?>
                    						<option <?php echo $selected; ?> style="font-family:<?php echo $custom_font_list[$i] ?>" value="<?php echo $i ?>"><?php echo $custom_font_list[$i] ?></option>
                    						<?php	
								$selected = "";
							} //end for
							unset($checker);
							?>
                  </select>                
              
              </div>
              <div class="optionitem">
              
                  <select name="custom_style_menuFontSize" id="custom_style_menuFontSize">
                    <option value="">-- please select --</option>
                    <?php 
						
							$selected = "";
							$arrCount = count($custom_font_size);
							$checker = get_option('custom_style_menuFontSize');
							for ($i = 0;$i < $arrCount;$i++){
								
								if ($checker == $i ) {
									$selected = "selected";
								}

						?>
                    <option <?php echo $selected; ?> style="font-size:<?php echo $custom_font_size[$i] ?>" value="<?php echo $i ?>"><?php echo $custom_font_size[$i] ?></option>
                    <?php	
								$selected = "";						
							} //end for
							unset($checker);
						?>
                  </select>               
              
              </div>
           	  <div class="optionitem" style="float:right; text-align:right;"><input type="submit" class="button-primary" name="submit" value="Save Options" /></div>                                                       
            </div>
            
            <div class="optionheading">Content</div>
            <div class="optionbox">
              <div class="optionitem">Background</div>              
              <div class="optionitem"><input name="custom_style_BodyBgColor" class="colorwell" type="text" onfocus="if (this.value==''){this.value='#';}" onblur="if (this.value=='#'){ this.value='';}" id="custom_style_BodyBgColor" value="<?php echo get_option('custom_style_BodyBgColor')?>" /></div>
              <div class="clear"></div>                          
              <div class="optionitem">Font</div>              
              <div class="optionitem"><input name="custom_style_bodyFontColor" class="colorwell" type="text" onfocus="if (this.value==''){this.value='#';}" onblur="if (this.value=='#'){ this.value='';}" id="custom_style_bodyFontColor" value="<?php echo get_option('custom_style_bodyFontColor')?>" /></div>
              <div class="optionitem">
                  <select name="custom_style_bodyFontType" id="custom_style_bodyFontType">
                    <option value="">-- please select --</option>
                    <?php 
							$selected = "";
							$arrCount = count($custom_font_list);
							$checker = get_option('custom_style_bodyFontType');
							for ($i = 0;$i < $arrCount;$i++){
								
								if ($checker == $i ) {
									$selected = "selected";
								}
						?>
                    <option <?php echo $selected; ?> style="font-family:<?php echo $custom_font_list[$i] ?>" value="<?php echo $i ?>"><?php echo $custom_font_list[$i] ?></option>
                    <?php	
								$selected = "";
							} //end for
							unset($checker);
						?>
                  </select>              
              </div>
              <div class="optionitem">
                  <select name="custom_style_bodyFontSize" id="custom_style_bodyFontSize">
                    <option value="">-- please select --</option>
                    <?php 
						
							$selected = "";
							$arrCount = count($custom_font_size);
							$checker = get_option('custom_style_bodyFontSize');
							for ($i = 0;$i < $arrCount;$i++){
								
								if ($checker == $i ) {
									$selected = "selected";
								}

						?>
                    <option <?php echo $selected; ?> style="font-size:<?php echo $custom_font_size[$i] ?>" value="<?php echo $i ?>"><?php echo $custom_font_size[$i] ?></option>
                    <?php	
								$selected = "";						
							} //end for
							unset($checker);
						?>
                  </select>              
              </div>
           	  <div class="optionitem" style="float:right; text-align:right;"><input type="submit" class="button-primary" name="submit" value="Save Options" /></div>                                                       
                            
            </div>
            
            <div class="optionheading">Headings</div>
            <div class="optionbox">            
              <div class="optionitem">H1</div>
              <div class="optionitem"><input name="custom_style_h1Color" class="colorwell" type="text" onfocus="if (this.value==''){this.value='#';}" onblur="if (this.value=='#'){ this.value='';}" id="custom_style_h1Color" value="<?php echo get_option('custom_style_h1Color')?>" /></div> 
              <div class="optionitem">
              
                  <select name="custom_style_hFontType" id="custom_style_hFontType">
                    <option value="">-- please select --</option>
                    <?php 
							$selected = "";	
							$arrCount = 0;
							$arrCount = count($custom_font_list);
							$checker = get_option('custom_style_hFontType');
							for ($i = 0;$i < $arrCount;$i++){
								
								if ($checker == $i ) {
									$selected = "selected";
								}								
								
						?>
                    <option <?php echo $selected; ?>  style="font-family:<?php echo $custom_font_list[$i] ?>" value="<?php echo $i ?>"><?php echo $custom_font_list[$i] ?></option>
                    <?php		
								$selected = "";							
							} //end for
							unset($checker);
						?>
                  </select>              
              
              </div>
              <div class="optionitem">
                  <select name="custom_style_h1FontSize" id="custom_style_h1FontSize">
                    <option value="">-- please select --</option>
                    <?php 
						
							$selected = "";
							$arrCount = count($custom_font_size);
							$checker = get_option('custom_style_h1FontSize');
							for ($i = 0;$i < $arrCount;$i++){
								
								if ($checker == $i ) {
									$selected = "selected";
								}

						?>
                    <option <?php echo $selected; ?> style="font-size:<?php echo $custom_font_size[$i] ?>" value="<?php echo $i ?>"><?php echo $custom_font_size[$i] ?></option>
                    <?php	
								$selected = "";						
							} //end for
							unset($checker);
						?>
                  </select>              
              </div>
              <div class="clear"></div>
              <div class="optionitem">H2</div>
              <div class="optionitem"><input name="custom_style_h2Color" class="colorwell" type="text" onfocus="if (this.value==''){this.value='#';}" onblur="if (this.value=='#'){ this.value='';}" id="custom_style_h2Color" value="<?php echo get_option('custom_style_h2Color')?>" /></div> 
              <div class="optionitem"><input name="custom_style_h2FontType" id="custom_style_h2FontType" type="text" value="Same as H1" disabled="disabled"/></div>
              <div class="optionitem">
                  <select name="custom_style_h2FontSize" id="custom_style_h2FontSize">
                    <option value="">-- please select --</option>
                    <?php 
						
							$selected = "";
							$arrCount = count($custom_font_size);
							$checker = get_option('custom_style_h2FontSize');
							for ($i = 0;$i < $arrCount;$i++){
								
								if ($checker == $i ) {
									$selected = "selected";
								}

						?>
                    <option <?php echo $selected; ?> style="font-size:<?php echo $custom_font_size[$i] ?>" value="<?php echo $i ?>"><?php echo $custom_font_size[$i] ?></option>
                    <?php	
								$selected = "";						
							} //end for
							unset($checker);
						?>
                  </select>              
              </div>                                                     
              <div class="clear"></div>
              <div class="optionitem">H3</div>
              <div class="optionitem"><input name="custom_style_h3Color" class="colorwell" type="text" onfocus="if (this.value==''){this.value='#';}" onblur="if (this.value=='#'){ this.value='';}" id="custom_style_h3Color" value="<?php echo get_option('custom_style_h3Color')?>" /></div> 
              <div class="optionitem"><input name="custom_style_h3FontType" id="custom_style_h3FontType" type="text" value="Same as H1" disabled="disabled"/></div>
              <div class="optionitem">
                  <select name="custom_style_h3FontSize" id="custom_style_h3FontSize">
                    <option value="">-- please select --</option>
                    <?php 
						
							$selected = "";
							$arrCount = count($custom_font_size);
							$checker = get_option('custom_style_h3FontSize');
							for ($i = 0;$i < $arrCount;$i++){
								
								if ($checker == $i ) {
									$selected = "selected";
								}

						?>
                    <option <?php echo $selected; ?> style="font-size:<?php echo $custom_font_size[$i] ?>" value="<?php echo $i ?>"><?php echo $custom_font_size[$i] ?></option>
                    <?php	
								$selected = "";						
							} //end for
							unset($checker);
						?>
                  </select>              
              </div>
           	  <div class="optionitem" style="float:right; text-align:right;"><input type="submit" class="button-primary" name="submit" value="Save Options" /></div>                                                       

              
			</div>
            
            <div class="optionheading">Footer</div>
            <div class="optionbox">
              <div class="optionitem">Background</div>
              <div class="optionitem"><input name="custom_style_FooterBgColor" class="colorwell" type="text" onfocus="if (this.value==''){this.value='#';}" onblur="if (this.value=='#'){ this.value='';}" id="custom_style_FooterBgColor" value="<?php echo get_option('custom_style_FooterBgColor')?>" /></div> 
              <div class="clear"></div>                          
              <div class="optionitem">Font</div>              
              <div class="optionitem"><input name="custom_style_FooterFontColor" class="colorwell" type="text" onfocus="if (this.value==''){this.value='#';}" onblur="if (this.value=='#'){ this.value='';}" id="custom_style_FooterFontColor" value="<?php echo get_option('custom_style_FooterFontColor')?>" /></div>
              <div class="optionitem">
                  <select name="custom_style_FooterFontType" id="custom_style_FooterFontType">
                    <option value="">-- please select --</option>
                    <?php 
							$selected = "";
							$arrCount = count($custom_font_list);
							$checker = get_option('custom_style_FooterFontType');
							for ($i = 0;$i < $arrCount;$i++){
								
								if ($checker == $i ) {
									$selected = "selected";
								}
						?>
                    <option <?php echo $selected; ?> style="font-family:<?php echo $custom_font_list[$i] ?>" value="<?php echo $i ?>"><?php echo $custom_font_list[$i] ?></option>
                    <?php	
								$selected = "";
							} //end for
							unset($checker);
						?>
                  </select>              
              </div>
              <div class="optionitem">
                  <select name="custom_style_FooterFontSize" id="custom_style_FooterFontSize">
                    <option value="">-- please select --</option>
                    <?php 
						
							$selected = "";
							$arrCount = count($custom_font_size);
							$checker = get_option('custom_style_FooterFontSize');
							for ($i = 0;$i < $arrCount;$i++){
								
								if ($checker == $i ) {
									$selected = "selected";
								}

						?>
                    <option <?php echo $selected; ?> style="font-size:<?php echo $custom_font_size[$i] ?>" value="<?php echo $i ?>"><?php echo $custom_font_size[$i] ?></option>
                    <?php	
								$selected = "";						
							} //end for
							unset($checker);
						?>
                  </select>              
              </div>
           	  <div class="optionitem" style="float:right; text-align:right;"><input type="submit" class="button-primary" name="submit" value="Save Options" /></div>                                                       
                         
            </div> 
            
  <!-- Side bar  -------------------------------------->
 
			<div class="optionheading">Side Bar</div>
            <div class="optionbox">
              <div class="optionitem">Sidebar Background</div>
              <div class="optionitem"><input name="custom_style_SidebarBodyBgColor" class="colorwell" type="text" onfocus="if (this.value==''){this.value='#';}" onblur="if (this.value=='#'){ this.value='';}" id="custom_style_SidebarBodyBgColor" value="<?php echo get_option('custom_style_SidebarBodyBgColor')?>" /></div> 
              <div class="optionitem">Sidebar Border 
              <input name="custom_style_SidebarLine" type="checkbox" value="1" <?php if (get_option('custom_style_SidebarLine') == 1 ) { echo checked; } ?>/> Colour?</div>
              <div class="optionitem"><input name="custom_style_SidebarLineColor" class="colorwell" type="text" onfocus="if (this.value==''){this.value='#';}" onblur="if (this.value=='#'){ this.value='';}" id="custom_style_SidebarLineColor" value="<?php echo get_option('custom_style_SidebarLineColor')?>" /></div> 
           	  <div class="optionitem" style="float:right; text-align:right;"><input type="submit" class="button-primary" name="submit" value="Save Options" /></div>                                                       
            </div>              
            
 <!-- Widget Title -------------------------------------->
 
			<div class="optionheading">Widget Title</div>
            <div class="optionbox">
              <div class="optionitem">Background</div>
              <div class="optionitem"><input name="custom_style_WidgetTitleBgColor" class="colorwell" type="text" onfocus="if (this.value==''){this.value='#';}" onblur="if (this.value=='#'){ this.value='';}" id="custom_style_WidgetTitleBgColor" value="<?php echo get_option('custom_style_WidgetTitleBgColor')?>" /></div>
           	  <div class="optionitem">Gradient Effect? <input name="custom_style_TitleBgEffect" type="checkbox" value="1" <?php if (get_option('custom_style_TitleBgEffect') == 1 ) { echo checked; } ?>/></div>
              <div class="clear"></div>                          
              <div class="optionitem">Font</div>              
              <div class="optionitem"><input name="custom_style_WidgetTitleFontColor" class="colorwell" type="text" onfocus="if (this.value==''){this.value='#';}" onblur="if (this.value=='#'){ this.value='';}" id="custom_style_WidgetTitleFontColor" value="<?php echo get_option('custom_style_WidgetTitleFontColor')?>" /></div>
              <div class="optionitem">
                  <select name="custom_style_WidgetTitleFontType" id="custom_style_WidgetTitleFontType">
                    <option value="">-- please select --</option>
                    <?php 
							$selected = "";
							$arrCount = count($custom_font_list);
							$checker = get_option('custom_style_WidgetTitleFontType');
							for ($i = 0;$i < $arrCount;$i++){
								
								if ($checker == $i ) {
									$selected = "selected";
								}
						?>
                    <option <?php echo $selected; ?> style="font-family:<?php echo $custom_font_list[$i] ?>" value="<?php echo $i ?>"><?php echo $custom_font_list[$i] ?></option>
                    <?php	
								$selected = "";
							} //end for
							unset($checker);
						?>
                  </select>              
              </div>
              <div class="optionitem">
                  <select name="custom_style_WidgetTitleFontSize" id="custom_style_WidgetTitleFontSize">
                    <option value="">-- please select --</option>
                    <?php 
						
							$selected = "";
							$arrCount = count($custom_font_size);
							$checker = get_option('custom_style_WidgetTitleFontSize');
							for ($i = 0;$i < $arrCount;$i++){
								
								if ($checker == $i ) {
									$selected = "selected";
								}

						?>
                    <option <?php echo $selected; ?> style="font-size:<?php echo $custom_font_size[$i] ?>" value="<?php echo $i ?>"><?php echo $custom_font_size[$i] ?></option>
                    <?php	
								$selected = "";						
							} //end for
							unset($checker);
						?>
                  </select>              
              </div>    
           	  <div class="optionitem" style="float:right; text-align:right;"><input type="submit" class="button-primary" name="submit" value="Save Options" /></div>                                                       
                        
            </div> 
            
 <!-- Widget Body -------------------------------------->
 
			<div class="optionheading">Widget Body</div>
            <div class="optionbox">
              <div class="optionitem">Background</div>
              <div class="optionitem"><input name="custom_style_WidgetBodyBgColor" class="colorwell" type="text" onfocus="if (this.value==''){this.value='#';}" onblur="if (this.value=='#'){ this.value='';}" id="custom_style_WidgetBodyBgColor" value="<?php echo get_option('custom_style_WidgetBodyBgColor')?>" /></div> 
              <div class="clear"></div>                          
              <div class="optionitem">Font</div>              
              <div class="optionitem"><input name="custom_style_WidgetBodyFontColor" class="colorwell" type="text" onfocus="if (this.value==''){this.value='#';}" onblur="if (this.value=='#'){ this.value='';}" id="custom_style_WidgetBodyFontColor" value="<?php echo get_option('custom_style_WidgetBodyFontColor')?>" /></div>
              <div class="optionitem">
                  <select name="custom_style_WidgetBodyFontType" id="custom_style_WidgetBodyFontType">
                    <option value="">-- please select --</option>
                    <?php 
							$selected = "";
							$arrCount = count($custom_font_list);
							$checker = get_option('custom_style_WidgetBodyFontType');
							for ($i = 0;$i < $arrCount;$i++){
								
								if ($checker == $i ) {
									$selected = "selected";
								}
						?>
                    <option <?php echo $selected; ?> style="font-family:<?php echo $custom_font_list[$i] ?>" value="<?php echo $i ?>"><?php echo $custom_font_list[$i] ?></option>
                    <?php	
								$selected = "";
							} //end for
							unset($checker);
						?>
                  </select>              
              </div>
              <div class="optionitem">
                  <select name="custom_style_WidgetBodyFontSize" id="custom_style_WidgetBodyFontSize">
                    <option value="">-- please select --</option>
                    <?php 
						
							$selected = "";
							$arrCount = count($custom_font_size);
							$checker = get_option('custom_style_WidgetBodyFontSize');
							for ($i = 0;$i < $arrCount;$i++){
								
								if ($checker == $i ) {
									$selected = "selected";
								}

						?>
                    <option <?php echo $selected; ?> style="font-size:<?php echo $custom_font_size[$i] ?>" value="<?php echo $i ?>"><?php echo $custom_font_size[$i] ?></option>
                    <?php	
								$selected = "";						
							} //end for
							unset($checker);
						?>
                  </select>              
              </div> 
           	  <div class="optionitem" style="float:right; text-align:right;"><input type="submit" class="button-primary" name="submit" value="Save Options" /></div>                                                       
                           
            </div>   
            
         </div>
         
 <!-- Custom Stylesheet  -------------------------------------->            
            <div class="optionheading">Upload Stylesheet</div> 
            <div class="optionbox">
                  <div class="optionitem">Enable?</div>
                  <div class="optionitem"><input name="enable_custom_style_sheet" type="checkbox" value="1" <?php echo $custom_style_sheet_checked; ?> /></div>
	    			<div class="clear"></div>
                  <div class="optionitem">Custom style sheet</div>
                  
						<?php
		                                if ( ( $styleSheetID = intval( $custom_style_sheet_id  ) ) && $styleSheetSrc = wp_get_attachment_url( $styleSheetID) ) :
                                                ?>
                                                	<div class="optionitem">
														<a href="<?php echo $styleSheetSrc; ?>" target="_blank" >View Style Sheet</a>
                                                    </div> 
                                                    <div class="optionitem">   
                                                        <input type="submit" name="delete-css-style-sheet" value="Delete Style Sheet" />
                                                    </div>
                                                <?php
                                                else :
                                                ?>
                                                	<div class="optionitem">No style sheet uploaded</div>
                                                    <div class="optionitem">
                                                     	<input type="file" name="custom_style_sheet_file" />
                                                    </div>
                                                      
                                                <?php
                                                endif;
                                                ?>

		            	  <div class="optionitem" style="float:right; text-align:right;"><input type="submit" class="button-primary" name="submit" value="Save Options" /></div>                                                       

	    </div>                   
           
 <!-- Custom CSS -------------------------------------->         
           <div class="optionheading">Custom css</div>
           <div class="optionbox">
              <div class="optionitem" style="width:99%">Type in the CCS code that you want to use. E.g body { font-size: 10px !important; }</div>
              <div class="clear"></div>              
              <div class="optionitem" style="width:99%">
                <textarea style="width:99%" name="custom_style_CSS" rows="8" id="custom_style_CSS"><?php echo stripslashes(get_option('custom_style_CSS'))?></textarea>
             </div>
              <div class="optionitem" style="float:right; text-align:right;"><input type="submit" class="button-primary" name="submit" value="Save Options" /></div>
           </div>                   
                 
                                 
 <!-- Submit button -------------------------------------->            
            <div style="float:left; clear:both;">             
            <input type="hidden" name="options-submit" value="1" />
			<p class="submit"><input type="submit" class="button-primary" name="submit" value="Save Options" /></p>
            </div>              
  </form>
</div>
<?php
} // end styles_admin()

function get_custom_styles()
{
	
	function getFontType($CustomFontId){
		$fonts_list = get_option('custom_font_list');
		$fonts_id = get_option($CustomFontId);
		return $fonts_list[$fonts_id];
		
	}
	
	function getFontSize($CustomFontId){
		$fonts_list = get_option('custom_font_size');
		$fonts_id = get_option($CustomFontId);
		return $fonts_list[$fonts_id];
		
	}	
	

	if ( get_option('custom_style_enabled') ){
		
		/* 
			12 =>	"'Muli',Arial, sans-serif",
			13 =>	"'Didact Gothic', arial, serif",
			14 =>	"'Zeyada', arial, serif",
			15 =>	"'Nunito', arial, serif",		
		*/
		
		if ( get_option('custom_style_bodyFontType') == 12 || 
			 get_option('custom_style_hFontType') == 12  || 
			 get_option('custom_style_menuFontType') == 12 || 
			 get_option('custom_style_WidgetTitleFontType') == 12 || 
			 get_option('custom_style_WidgetBodyFontType') == 12 ) {
			echo '<link href="http://fonts.googleapis.com/css?family=Muli" rel="stylesheet" type="text/css">';
		}
		
		if ( get_option('custom_style_bodyFontType') == 13 || 
			 get_option('custom_style_hFontType') == 13 || 
			 get_option('custom_style_menuFontType') == 13 || 
			 get_option('custom_style_WidgetTitleFontType') == 13 || 
			 get_option('custom_style_WidgetBodyFontType') == 13) {
			echo "<link href='http://fonts.googleapis.com/css?family=Didact+Gothic&v1' rel='stylesheet' type='text/css'>";
		}		
		
		if ( get_option('custom_style_bodyFontType') == 14 || 
			 get_option('custom_style_hFontType') == 14 || 
			 get_option('custom_style_menuFontType') == 14 || 
			 get_option('custom_style_WidgetTitleFontType') == 14 || 
			 get_option('custom_style_WidgetBodyFontType') == 14) {
			echo "<link href='http://fonts.googleapis.com/css?family=Zeyada&v1' rel='stylesheet' type='text/css'>";
		}	
		
		if ( get_option('custom_style_bodyFontType') == 15 || 
			 get_option('custom_style_hFontType') == 15 || 
			 get_option('custom_style_menuFontType') == 15 || 
			 get_option('custom_style_WidgetTitleFontType') == 15 || 
			 get_option('custom_style_WidgetBodyFontType') == 15) {
			echo "<link href='http://fonts.googleapis.com/css?family=Nunito&v1' rel='stylesheet' type='text/css'>";
		}			
?>

<style type="text/css">
html, body, #main {
  color: <?php echo get_option('custom_style_bodyFontColor'); ?>; 
  font-family: <?php echo getFontType('custom_style_bodyFontType') ?>;
  font-size: <?php echo getFontSize('custom_style_bodyFontSize') ?> ; 
}  

input, textarea {
  font-family: <?php echo getFontType('custom_style_bodyFontType') ?>;
}

a { 
  color: <?php echo get_option('custom_style_aColor'); ?>;
}

h1, h1 a {
  color: <?php echo get_option('custom_style_h1Color'); ?>; 
  font-family: <?php echo getFontType('custom_style_hFontType') ?>;
  font-size: <?php echo getFontSize('custom_style_h1FontSize') ?> ; 
  text-decoration:none;
}
	
h2, h2 a {
  color: <?php echo get_option('custom_style_h2Color'); ?>; 
  font-family: <?php echo getFontType('custom_style_hFontType') ?>;
  font-size: <?php echo getFontSize('custom_style_h2FontSize') ?> ; 
  text-decoration:none;
}	
	  
h3, h3 a {
  color: <?php echo get_option('custom_style_h3Color'); ?>; 
  font-family: <?php echo getFontType('custom_style_hFontType') ?>;
  font-size: <?php echo getFontSize('custom_style_h3FontSize') ?> ; 
  text-decoration:none;
}		  
      
#container {
  background-color: <?php echo get_option('custom_style_ContainerBgColor'); ?>;
}

#menu, #menu ul { 
  color: <?php echo get_option('custom_style_menuFontColor'); ?>;
  background-color: <?php echo get_option('custom_style_menuBgColor'); ?>;
  font-family: <?php echo getFontType('custom_style_menuFontType') ?>;
  font-size: <?php echo getFontSize('custom_style_menuFontSize') ?> ;
  <?php if (get_option('custom_style_menuEffect') != 1) { echo "background-image:none;"; }?>
}

#menu ul li a { 
  color: <?php echo get_option('custom_style_menuFontColor'); ?>;
}

#main {
   background-color:<?php echo get_option('custom_style_BodyBgColor'); ?>;
<?php  if (get_option('custom_style_SidebarBodyBgColor') == "") {  ?> 
	padding:10px 0px;
<?php } ?>	
}

#leftbar,
#rightbar 
{
   background-color:<?php echo get_option('custom_style_SidebarBodyBgColor'); ?>;
   padding-top:10px;
}

#leftbar {
<?php  if (get_option('custom_style_SidebarLine') != "1") {  ?> 
	border-right: none;
<?php } else { ?>
	border-right: 3px solid <?php echo get_option('custom_style_SidebarLineColor'); ?>
<?php } ?>	
}

#rightbar {
<?php  if (get_option('custom_style_SidebarLine') != "1") {  ?> 
	border-left: none;
<?php } else { ?>
	border-left: 3px solid <?php echo get_option('custom_style_SidebarLineColor'); ?>
<?php } ?>	
}

h3.widget-title {
  color: <?php echo get_option('custom_style_WidgetTitleFontColor'); ?>;
  background-color: <?php echo get_option('custom_style_WidgetTitleBgColor'); ?>;
  font-family: <?php echo getFontType('custom_style_WidgetTitleFontType') ?>;
  font-size: <?php echo getFontSize('custom_style_WidgetTitleFontSize') ?> ;
<?php if (get_option('custom_style_TitleBgEffect') == 1) {  ?>
  background-image: url(<?php bloginfo('template_url') ?>/images/menu_over.png);
  background-repeat: repeat-x; background-position: center center;
<?php } ?>  
}

.box {
  color: <?php echo get_option('custom_style_WidgetBodyFontColor'); ?>;
  background-color: <?php echo get_option('custom_style_WidgetBodyBgColor'); ?>;
  font-family: <?php echo getFontType('custom_style_WidgetBodyFontType') ?>;
  font-size: <?php echo getFontSize('custom_style_WidgetBodyFontSize') ?> ;	
}

.shaddow {
  <?php if (get_option('custom_style_HeaderShaddow') != 1) { echo "display:none;"; }?>	
}

#footer {
   background-color:<?php echo get_option('custom_style_FooterBgColor'); ?>;
   color: <?php echo get_option('custom_style_FooterFontColor'); ?>;
   font-family: <?php echo getFontType('custom_style_FooterFontType') ?>;
   font-size: <?php echo getFontSize('custom_style_FooterFontSize') ?> ;			   
}

#footer a, #footer li a{
   color: <?php echo get_option('custom_style_FooterFontColor'); ?>;
   font-family: <?php echo getFontType('custom_style_FooterFontType') ?>;
   font-size: <?php echo getFontSize('custom_style_FooterFontSize') ?> ;
}

<?php echo get_option('custom_style_CSS'); ?>

</style>
<?php

	} // end get_option('custom_style_enabled')
	
} //end get_custom_styles

?>
