<?php
// ---------- "Child Theme Options" menu STARTS HERE
$favicon_msg = ""; 

 
/*
function favicon_add_admin() {
	add_submenu_page('themes.php', 'Favourite Icon', 'Favourite Icon', 'edit_themes', basename(__FILE__), 'favicon_admin');
}
*/
 
function favicon_add_admin() {
	$theme_page = add_theme_page(
		__( 'Custom fav icon', 'CleanStyleOne' ),  	// Name of page
		__( 'Custom fav icon', 'CleanStyleOne' ), 	// Label in menu
		'edit_theme_options',					// Capability required
		'favicon_options',                  	// Menu slug, used to uniquely identify the page
		'favicon_admin' 						// Function that renders the options page
	);

	if ( ! $theme_page )
		return;
} 
 
add_action('admin_menu' , 'favicon_add_admin'); 
 
function favicon_admin() 
{
	$favicon_msg = ''; 
	$custom_favicon_image_id = get_option('custom_favicon_image_id');
	$enabled = get_option('custom_favicon_enabled');

        if (isset($_POST['delete-favicon']))
        {
                wp_delete_attachment($custom_favicon_image_id, true);
                update_option('custom_logo_image_id', false);
        }
 
	if ($_POST['options-submit'] && !isset($_POST['delete-favicon']))
	{
		$enabled = htmlspecialchars($_POST['enabled']);
		update_option('custom_favicon_enabled', $enabled);
 
        	if (is_uploaded_file($_FILES['favicon_image']['tmp_name']))
		{										  
			$file_type = $_FILES['favicon_image']['type'];
		
			list($width, $height) = getimagesize($_FILES['favicon_image']['tmp_name']);
			//echo "fuck" .  $width, $height;
			
			if ($width != 16 && $height != 16)
				$favicon_msg = 'File needs to be exactly 16 x 16 px.';
			else
			{		
				if ($file_type == "image/x-icon")
				{
		                        $id = media_handle_upload('favicon_image',0);
                        		if ( is_wp_error($id) )
                        		{
                                 		echo '<div class="error-div">
                                        		<a class="dismiss" href="#" onclick="jQuery(this).parents(\'div.media-item\').slideUp(200, function(){jQuery(this).remove();});">' . __('Dismiss') . '</a>
                                        		<strong>' . sprintf(__('&#8220;%s&#8221; has failed to upload due to an error'), esc_html($_FILES['favicon_image']['name']) ) . '</strong><br />' .
                                        		esc_html($id->get_error_message()) . '</div>';
                        		}
                        		else
                        		{
                                		$custom_favicon_image_id = $id;
                                		update_option('custom_favicon_image_id', $id);
                                		echo '<div class="updated"><p>Your new options have been successfully saved.</p></div>';
                        		}

					/*
					if (file_exists($temp_file))
					{
						//$fd = fopen($temp_file,�rb�);
						$file_content=file_get_contents($temp_file);
						//fclose($fd);
					}
		 
					$wud = wp_upload_dir();
		 
					if (file_exists($wud[path].'/'.strtolower($file_name))){
						unlink ($wud[path].'/'.strtolower($file_name));
					}
		 
					$upload = wp_upload_bits( $file_name, '', $file_content);
				//	echo $upload['error'];
		 
					$custom_favicon_image = $wud[url].'/'.strtolower($file_name);
					update_option('custom_favicon_image', $custom_favicon_image);
					*/
				} 
				else if ($file_type!=="image/x-icon") 
					$favicon_msg = 'File type needs to be .ico';
				
			}
		}
		if (!empty($favicon_msg)) 
		{
	                ?>
	                <div class="updated">
        	        <p>
                	<?php
 
                        echo '<div class="error-div">
                              <a class="dismiss" href="#" onclick="jQuery(this).parents(\'div.media-item\').slideUp(200, function(){jQuery(this).remove();});">' . __('Dismiss') . '</a>
                              <strong>' . sprintf(__('&#8220;%s&#8221; has failed to upload due to an error'), esc_html($_FILES['favicon_image']['name']) ) . '</strong><br />' .
                             esc_html($favicon_msg) . '</div>';
			?>
                	</p></div>
                	<?php
		}
	}
 
	if($enabled) $checked='checked="checked"';
 
	?>
		<div class="wrap">
			<div id="icon-themes" class="icon32"></div>
			<h2>Upload a favourite icon</h2>
			<div class="updated" style="background-color:#FC6;">
            	<p><strong>IMPORTANT</strong>: Your file needs to be a valid ico format for all browsers to read it. (Especially Internet Explorer).
            	<a href="http://www.telegraphics.com.au/sw/" target="_blank">Download the x-icon file extention for Photoshop</a></p>
           </div>           
            
	  <form name="theform" method="post" enctype="multipart/form-data" action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']);?>">
				<table class="form-table">
                
                
					<tr>
						<td width="200">Enabled?</td>
						<td><input name="enabled" type="checkbox" value="1" <?php echo $checked; ?>/></td>
					</tr>
                   
                    
					<tr>
						<td>Current image:</td>
						<td>
                                                <?php
                                                if ( ( $faviconID = intval( $custom_favicon_image_id ) ) && $faviconSrc = wp_get_attachment_image_src( $faviconID, '') ) :
                                                ?>
                                                        <img src="<?php echo $faviconSrc[0]; ?>" alt="<?php bloginfo( 'name' )?>" />
                                                        <input type="submit" name="delete-favicon" value="Delete Favicon" />
                                                <?php
                                                else :
                                                ?>
                                                        No image uploaded yet
                                                <?php
                                                endif;
                                                ?>

                       				 </td>
					</tr>
					<tr>
						<td>favicon image to use (gif/png):</td>
						<td><input type="file" name="favicon_image">
						(must be 16 x 16px and and .ico file format)<br />(you must have writing permissions for your uploads directory)</td>
					</tr>
				</table>
				<input type="hidden" name="options-submit" value="1" />
				<p class="submit"><input type="submit" name="submit" value="Save Options" /></p>
		  </form>
		</div>
<?php
}
 
// ---------- "Child Theme Options" menu ENDS HERE
