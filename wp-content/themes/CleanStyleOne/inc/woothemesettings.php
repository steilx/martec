<?php
/**
 * 	Child Theme: Styling options
 *	Author: Paul Combrink @ Interface
 *	Date: 2011-06-24
 *	Updated: 2012-04-18 - integration for WooCommerce
 *	Updated: 2012-06-22 - changed the way scripts are enqueing for WP3.4, calling from footer
 */
 
 /* Enqueue styles and scripts */
 
function custom_woo_enqueue() { 

	wp_enqueue_style( 'farbtastic' );
	wp_enqueue_script( 'farbtastic' );
	
	wp_register_script( 'farbtastic-instance', get_template_directory_uri() . '/inc/farbtastic-instance.js',array( 'farbtastic', 'jquery' ),'0.1',true);	
	wp_enqueue_script( 'farbtastic-instance' );	

	wp_register_style( 'woothemeoptions-style', get_template_directory_uri() . '/inc/theme-extention.css',__FILE__,'0.2');	
	wp_enqueue_style( 'woothemeoptions-style' );
}

add_action('admin_enqueue_scripts', 'custom_woo_enqueue');

function custom_catalogue_options_add_page() {
	$theme_page = add_theme_page(
		__( 'Catalogue Options', 'CleanStyleOne' ),  	// Name of page
		__( 'Catalogue Options', 'CleanStyleOne' ),  	// Label in menu
		'edit_theme_options',							// Capability required
		'catalogue_options',                       		// Menu slug, used to uniquely identify the page
		'catalogue_admin' 								// Function that renders the options page
	);

	if ( ! $theme_page )
		return;
}

add_action('admin_menu' , 'custom_catalogue_options_add_page'); 

// Add Calalogue Image Widget
if (is_plugin_active('woocommerce/woocommerce.php')) {
	
function CleanStyleOne_woo_widgets_init() {

	register_sidebar( array(
	'name' => __('Catalog Header Image','CleanStyleOne'),
	'id' => 'catalog_header_image_widget',
	'description' => __( 'Catalog Header Image','CleanStyleOne'),
	'before_widget' => '<div id="%1$s" class="%2$s">',
	'after_widget' => "</div>",
	'before_title' => '',
	'after_title' => '',
	) );
	}	
	
	add_action( 'widgets_init', 'CleanStyleOne_woo_widgets_init' );
}

// Check if WooCommerce AND WP Page Navi is enabled to enable pretty page navigation
if (is_plugin_active('woocommerce/woocommerce.php') && is_plugin_active('wp-pagenavi/wp-pagenavi.php')) {

		remove_action('woocommerce_pagination', 'woocommerce_pagination', 10);
			function woocommerce_pagination() {
				wp_pagenavi();
			}
		add_action( 'woocommerce_pagination', 'woocommerce_pagination', 10);
}	

// To sort out number of product per page
if ( get_option('customWooStylePLNumberOfProducts') ) {
	$NoOfProducts = "return ".get_option('customWooStylePLNumberOfProducts').";";
	add_filter('loop_shop_per_page', create_function('$cols', $NoOfProducts));	
}
 
// Redefine woocommerce_output_related_products()
function woocommerce_output_related_products() {
	if ( get_option('customWooStyleSPShowRelatedNoProducts') ) {
		woocommerce_related_products(get_option('customWooStyleSPShowRelatedNoProducts')); // Get custom	
	} else {
		woocommerce_related_products(3,3); // Display 3 products in rows of 3
	}
}

// ================= EMail Template and Subject Hooks ================


if ( get_option('customWooEmailSubjectCustomerOrderReceived') ) {
	
	add_filter('woocommerce_email_subject_customer_procesing_order','new_order_subject');
	
	function new_order_subject($subject) {
		$subject = get_option('customWooEmailSubjectCustomerOrderReceived');
		return $subject;
	}	
		
}

if ( get_option('customWooEmailSubjectAdminOrderReceived') ) {
	
	add_filter('woocommerce_email_subject_new_order','new_order_subject_admin');
	
	function new_order_subject_admin($admin_subject) {
		$admin_subject = get_option('customWooEmailSubjectAdminOrderReceived');
		return $admin_subject;
	}	
		
}



	
	
// ================= Check Out page (Review Order) Hooks ================	
/*	
	add_filter('woocommerce_order_button_text','new_button_text');
	
	function new_button_text($buttontext){
	$buttontext= 'Send Quotation Request';
	return $buttontext;
	}			
*/

 
function catalogue_admin()  
{
	
   $custom_font_size = array (
                        0 =>    '',
                        1 =>    '10px',
                        2 =>    '11px',
                        3 =>    '12px',
                        4 =>    '13px',
                        5 =>    '14px',
                        6 =>    '15px',
                        7 =>    '16px',
                        8 =>    '17px',
                        9 =>    '18px',
                        10 =>   '20px',
                        11 =>   '24px',
                        12 =>   '30px',
                        13 =>   '36px',
                        14 =>   '40px',
                        15 =>   '50px',						
			);	
	
	if ($_POST['submit'])
	{

		// ---- WooCommerce Options --------------------------
		
		$customWooEnabled = $_POST['customWooEnabled'];
		update_option('customWooEnabled', $customWooEnabled);	
		
		$custom_woo_stylesheet = $_POST['custom_woo_stylesheet'];
		update_option('custom_woo_stylesheet', $custom_woo_stylesheet);	
		
		$customWooShowBreadcrumb = $_POST['customWooShowBreadcrumb'];
		update_option('customWooShowBreadcrumb', $customWooShowBreadcrumb);		

		$customWooShowHeader = $_POST['customWooShowHeader'];
		update_option('customWooShowHeader', $customWooShowHeader);	
		
		//-- Product List  (abbreviated with PL in the variable) --
		
		$customWooStylePLcols = $_POST['customWooStylePLcols'];
		update_option('customWooStylePLcols', $customWooStylePLcols);

		$customWooStylePLNumberOfProducts = $_POST['customWooStylePLNumberOfProducts'];
		update_option('customWooStylePLNumberOfProducts', $customWooStylePLNumberOfProducts);

		$customWooStylePLShowThumbFirst = $_POST['customWooStylePLShowThumbFirst'];
		update_option('customWooStylePLShowThumbFirst', $customWooStylePLShowThumbFirst);
		
		$customWooStylePLFontColour = $_POST['customWooStylePLFontColour'];
		update_option('customWooStylePLFontColour', $customWooStylePLFontColour);

		$customWooStylePLFontSize = $_POST['customWooStylePLFontSize'];
		update_option('customWooStylePLFontSize', $customWooStylePLFontSize);		

		$customWooStylePLBtnLabel = $_POST['customWooStylePLBtnLabel'];
		update_option('customWooStylePLBtnLabel', $customWooStylePLBtnLabel);
		
		$customWooStylePLShowOrderby = $_POST['customWooStylePLShowOrderby'];
		update_option('customWooStylePLShowOrderby', $customWooStylePLShowOrderby);

		$customWooStylePLBoxHeight = $_POST['customWooStylePLBoxHeight'];
		update_option('customWooStylePLBoxHeight', $customWooStylePLBoxHeight);
		
		$customWooStylePLShowPrice = $_POST['customWooStylePLShowPrice'];
		update_option('customWooStylePLShowPrice', $customWooStylePLShowPrice);		

		$customWooStylePLShowButton = $_POST['customWooStylePLShowButton'];
		update_option('customWooStylePLShowButton', $customWooStylePLShowButton);	
		
		$customWooStylePLShowShortDescription = $_POST['customWooStylePLShowShortDescription'];
		update_option('customWooStylePLShowShortDescription', $customWooStylePLShowShortDescription);	

		$customWooStylePLShortDescriptionHeight = $_POST['customWooStylePLShortDescriptionHeight'];
		update_option('customWooStylePLShortDescriptionHeight', $customWooStylePLShortDescriptionHeight);
		
		$customWooStylePLPriceColour = $_POST['customWooStylePLPriceColour'];
		update_option('customWooStylePLPriceColour', $customWooStylePLPriceColour);	

		$customWooStylePLShowSKU = $_POST['customWooStylePLShowSKU'];
		update_option('customWooStylePLShowSKU', $customWooStylePLShowSKU);	
		
		
		
		//-- Single product (product detail / single product)  (abbreviated with SL in the variable) --

		$customWooStyleSPShowButton = $_POST['customWooStyleSPShowButton'];
		update_option('customWooStyleSPShowButton', $customWooStyleSPShowButton);	
		
		$customWooStyleSPBtnLabel = $_POST['customWooStyleSPBtnLabel'];
		update_option('customWooStyleSPBtnLabel', $customWooStyleSPBtnLabel);	

		$customWooStyleSPShowPrice = $_POST['customWooStyleSPShowPrice'];
		update_option('customWooStyleSPShowPrice', $customWooStyleSPShowPrice);		
		
		$customWooStyleSPPriceColour = $_POST['customWooStyleSPPriceColour'];
		update_option('customWooStyleSPPriceColour', $customWooStyleSPPriceColour);			

		$customWooStyleSPShowSKU = $_POST['customWooStyleSPShowSKU'];
		update_option('customWooStyleSPShowSKU', $customWooStyleSPShowSKU);	

		$customWooStyleSPShowCategory = $_POST['customWooStyleSPShowCategory'];
		update_option('customWooStyleSPShowCategory', $customWooStyleSPShowCategory);	
		
		//-- Related products on Single product (product detail / single product)  (abbreviated with SL in the variable) --		
		
		$customWooStyleSPShowRelated = $_POST['customWooStyleSPShowRelated'];
		update_option('customWooStyleSPShowRelated', $customWooStyleSPShowRelated);

		$customWooStyleSPShowRelatedNoProducts = $_POST['customWooStyleSPShowRelatedNoProducts'];
		update_option('customWooStyleSPShowRelatedNoProducts', $customWooStyleSPShowRelatedNoProducts);

		$customWooStyleSPShowRelatedColour = $_POST['customWooStyleSPShowRelatedColour'];
		update_option('customWooStyleSPShowRelatedColour', $customWooStyleSPShowRelatedColour);

		$customWooStyleSPShowRelatedFontSize = $_POST['customWooStyleSPShowRelatedFontSize'];
		update_option('customWooStyleSPShowRelatedFontSize', $customWooStyleSPShowRelatedFontSize);

		$customWooStyleSPShowRelatedShortDesc = $_POST['customWooStyleSPShowRelatedShortDesc'];
		update_option('customWooStyleSPShowRelatedShortDesc', $customWooStyleSPShowRelatedShortDesc);

		$customWooStyleSPShowRelatedShortDescHeight = $_POST['customWooStyleSPShowRelatedShortDescHeight'];
		update_option('customWooStyleSPShowRelatedShortDescHeight', $customWooStyleSPShowRelatedShortDescHeight);

		//-- Featured products on home page --
		
		$customWooStyleFeaturedHomeShow = $_POST['customWooStyleFeaturedHomeShow'];
		update_option('customWooStyleFeaturedHomeShow', $customWooStyleFeaturedHomeShow);

		$customWooStyleFeaturedHomeNoProducts = $_POST['customWooStyleFeaturedHomeNoProducts'];
		update_option('customWooStyleFeaturedHomeNoProducts', $customWooStyleFeaturedHomeNoProducts);
		
		$customWooStyleFeaturedHomeFontColour = $_POST['customWooStyleFeaturedHomeFontColour'];
		update_option('customWooStyleFeaturedHomeFontColour', $customWooStyleFeaturedHomeFontColour);

		$customWooStyleFeaturedHomeFontSize = $_POST['customWooStyleFeaturedHomeFontSize'];
		update_option('customWooStyleFeaturedHomeFontSize', $customWooStyleFeaturedHomeFontSize);

		$customWooStyleFeaturedHomeShowSortDesc = $_POST['customWooStyleFeaturedHomeShowSortDesc'];
		update_option('customWooStyleFeaturedHomeShowSortDesc', $customWooStyleFeaturedHomeShowSortDesc);

		$customWooStyleFeaturedHomeSortDescHeight = $_POST['customWooStyleFeaturedHomeSortDescHeight'];
		update_option('customWooStyleFeaturedHomeSortDescHeight', $customWooStyleFeaturedHomeSortDescHeight);
		
		$customWooStyleFeaturedHomeProdBoxHeight = $_POST['customWooStyleFeaturedHomeProdBoxHeight'];
		update_option('customWooStyleFeaturedHomeProdBoxHeight', $customWooStyleFeaturedHomeProdBoxHeight);

		$customWooStyleFeaturedHomeShowPrice = $_POST['customWooStyleFeaturedHomeShowPrice'];
		update_option('customWooStyleFeaturedHomeShowPrice', $customWooStyleFeaturedHomeShowPrice);
		
		$customWooStyleFeaturedHomePriceColour = $_POST['customWooStyleFeaturedHomePriceColour'];
		update_option('customWooStyleFeaturedHomePriceColour', $customWooStyleFeaturedHomePriceColour);

		$customWooStyleFeaturedHomeShowCartBtn = $_POST['customWooStyleFeaturedHomeShowCartBtn'];
		update_option('customWooStyleFeaturedHomeShowCartBtn', $customWooStyleFeaturedHomeShowCartBtn);

	
		//-- Email subject and template --		
		$customWooEmailSubjectCustomerOrderReceived = $_POST['customWooEmailSubjectCustomerOrderReceived'];
		update_option('customWooEmailSubjectCustomerOrderReceived', $customWooEmailSubjectCustomerOrderReceived);

		$customWooEmailHeadingCustomerOrderReceived = $_POST['customWooEmailHeadingCustomerOrderReceived'];
		update_option('customWooEmailHeadingCustomerOrderReceived', $customWooEmailHeadingCustomerOrderReceived);

		$customWooEmailSubjectAdminOrderReceived = $_POST['customWooEmailSubjectAdminOrderReceived'];
		update_option('customWooEmailSubjectAdminOrderReceived', $customWooEmailSubjectAdminOrderReceived);

		$customWooEmailHeadingAdminOrderReceived = $_POST['customWooEmailHeadingAdminOrderReceived'];
		update_option('customWooEmailHeadingAdminOrderReceived', $customWooEmailHeadingAdminOrderReceived);
				

		?>
		<div class="updated"><p>Your new options have been successfully saved.</p></div>
		<?php		
		
	} // post options submit
 
?>

<div class="wrap">
			<div id="icon-themes" class="icon32"></div>
            <h2>Catalogue Options</h2>
            
            <div id="mypicker">
              <div id="picker"></div>
            </div>            
            
            <form name="theform" id="theform" method="post" enctype="multipart/form-data" action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']);?>">
			<!-- Submit button -------------------------------------->            
            <div style="float:left; clear:both;"></div>
            
            <!-- General Setting -->
            <div class="optionheading">General Setting</div>
            <div class="optionbox">
           	  <div class="optionitem">Enabled</div>
           	  <div class="optionitem"><input name="customWooEnabled" type="checkbox" value="1" <?php if (get_option('customWooEnabled') == 1 ) { echo checked; } ?>/></div>                            
           	  <div class="optionitem">Stylesheet</div>
           	  <div class="optionitem">
              	<select name="custom_woo_stylesheet">
                    <option value="0" <?php if ( get_option('custom_woo_stylesheet') =="0") { echo 'selected="selected"'; }?>>Use WooCommerce</option>
                    <option value="1" <?php if ( get_option('custom_woo_stylesheet') =="1") { echo 'selected="selected"'; }?> >Default Catalogue</option>
       	      	</select>
              </div>
              <div class="clear"></div>          
           	  
              <div class="optionitem">Show breadcrumb</div>
           	  <div class="optionitem"><input name="customWooShowBreadcrumb" type="checkbox" value="1" <?php if (get_option('customWooShowBreadcrumb') == 1 ) { echo checked; } ?>/></div>                            
           	  <div class="optionitem">Catalog header image</div>
           	  <div class="optionitem"><input name="customWooShowHeader" type="checkbox" value="1" <?php if (get_option('customWooShowHeader') == 1 ) { echo checked; } ?>/> (on catalog only)</div>                            
           	  <div class="optionitem" style="float:right; text-align:right;"><input type="submit" class="button-primary" name="submit" value="Save Options" /></div>                                                       
 
            </div>                      
            
            
            <!-- Products Catalogue Page -->
            <div class="optionheading">Product Catalogue Page</div>
            <div class="optionbox">
           	  <div class="optionitem">Number of columns</div>
           	  <div class="optionitem"><input name="customWooStylePLcols" value="<?php echo (get_option('customWooStylePLcols'))?>" /></div>              
           	  <div class="optionitem">Number of product / page</div>
           	  <div class="optionitem"><input name="customWooStylePLNumberOfProducts" value="<?php echo (get_option('customWooStylePLNumberOfProducts'))?>" /></div>
           	  <div class="optionitem" style="float:right; text-align:right;"><input type="submit" class="button-primary" name="submit" value="Save Options" /></div>
              <div class="clear"></div>

           	  <div class="optionitem">Product name - font</div>
           	  <div class="optionitem"><input name="customWooStylePLFontColour" class="colorwell "type="text" onfocus="if (this.value==''){this.value='#';}" onblur="if (this.value=='#'){ this.value='';}" id="customWooStylePLFontColour" value="<?php echo (get_option('customWooStylePLFontColour'))?>" /></div>              
              <div class="optionitem">
                  <select name="customWooStylePLFontSize" id="customWooStylePLFontSize">
                    <option value="">-- please select --</option>
                    <?php 
						
							$selected = "";
							$arrCount = count($custom_font_size);
							$checker = get_option('customWooStylePLFontSize');
							for ($i = 0;$i < $arrCount;$i++){
								
								if ($checker == $i ) {
									$selected = "selected";
								}

						?>
                    <option <?php echo $selected; ?> style="font-size:<?php echo $custom_font_size[$i] ?>" value="<?php echo $i ?>"><?php echo $custom_font_size[$i] ?></option>
                    <?php	
								$selected = "";						
							} //end for
							unset($checker);
						?>
                  </select>              
              </div>                                                     
                           
              <div class="clear"></div>              

           	  <div class="optionitem">Show thumb before name </div>
           	  <div class="optionitem"><input name="customWooStylePLShowThumbFirst" type="checkbox" value="1" <?php if (get_option('customWooStylePLShowThumbFirst') == 1 ) { echo checked; } ?>/></div>                            
              <div class="clear"></div>

           	  <div class="optionitem">Show Cart Button </div>
           	  <div class="optionitem"><input name="customWooStylePLShowButton" type="checkbox" value="1" <?php if (get_option('customWooStylePLShowButton') == 1 ) { echo checked; } ?>/></div>                            
           	  <div class="optionitem">Button label</div>
           	  <div class="optionitem"><input name="customWooStylePLBtnLabel" value="<?php echo (get_option('customWooStylePLBtnLabel'))?>" /></div>              
              <div class="clear"></div>

           	  <div class="optionitem">Show Order by Sort </div>
           	  <div class="optionitem"><input name="customWooStylePLShowOrderby" type="checkbox" value="1" <?php if (get_option('customWooStylePLShowOrderby') == 1 ) { echo checked; } ?>/></div>                            
           	  <div class="optionitem">Product box height</div>
           	  <div class="optionitem"><input name="customWooStylePLBoxHeight" value="<?php echo (get_option('customWooStylePLBoxHeight'))?>" /> px</div>              
              <div class="clear"></div>

           	  <div class="optionitem">Show Price </div>
           	  <div class="optionitem"><input name="customWooStylePLShowPrice" type="checkbox" value="1" <?php if (get_option('customWooStylePLShowPrice') == 1 ) { echo checked; } ?>/></div>                            
           	  <div class="optionitem">Price Colour</div>
           	  <div class="optionitem"><input name="customWooStylePLPriceColour" class="colorwell "type="text" onfocus="if (this.value==''){this.value='#';}" onblur="if (this.value=='#'){ this.value='';}" id="customWooStylePLPriceColour" value="<?php echo (get_option('customWooStylePLPriceColour'))?>" /></div>              
              <div class="clear"></div>

           	  <div class="optionitem">Show Short Description </div>
           	  <div class="optionitem"><input name="customWooStylePLShowShortDescription" type="checkbox" value="1" <?php if (get_option('customWooStylePLShowShortDescription') == 1 ) { echo checked; } ?>/></div>                            
           	  <div class="optionitem">Short Description Height</div>
           	  <div class="optionitem"><input name="customWooStylePLShortDescriptionHeight" value="<?php echo (get_option('customWooStylePLShortDescriptionHeight'))?>" /> px</div>              
              <div class="clear"></div>
           	  <div class="optionitem">Show SKU (Product Code)</div>
           	  <div class="optionitem"><input name="customWooStylePLShowSKU" type="checkbox" value="1" <?php if (get_option('customWooStylePLShowSKU') == 1 ) { echo checked; } ?>/></div>                            

           	  <div class="optionitem" style="float:right; text-align:right;"><input type="submit" class="button-primary" name="submit" value="Save Options" /></div>                                                       
            </div>
            
            <div style="float:left; clear:both;"></div>            

            <!-- Products Detail Page -->            
            <div class="optionheading">Product Detail Page</div>
            <div class="optionbox">  
           	  <div class="optionitem">Show Cart Button </div>
           	  <div class="optionitem"><input name="customWooStyleSPShowButton" type="checkbox" value="1" <?php if (get_option('customWooStyleSPShowButton') == 1 ) { echo checked; } ?>/></div>                            
           	  <div class="optionitem">Button label</div>
           	  <div class="optionitem"><input name="customWooStyleSPBtnLabel" value="<?php echo (get_option('customWooStyleSPBtnLabel'))?>" /></div>              
              <div class="clear"></div>

           	  <div class="optionitem">Show Price </div>
           	  <div class="optionitem"><input name="customWooStyleSPShowPrice" type="checkbox" value="1" <?php if (get_option('customWooStyleSPShowPrice') == 1 ) { echo checked; } ?>/></div>                            
           	  <div class="optionitem">Price Colour</div>
           	  <div class="optionitem"><input name="customWooStyleSPPriceColour" class="colorwell "type="text" onfocus="if (this.value==''){this.value='#';}" onblur="if (this.value=='#'){ this.value='';}" id="customWooStyleSPPriceColour" value="<?php echo (get_option('customWooStyleSPPriceColour'))?>" /></div>              
              <div class="clear"></div>
              
           	  <div class="optionitem">Show SKU (Product Code)</div>
           	  <div class="optionitem"><input name="customWooStyleSPShowSKU" type="checkbox" value="1" <?php if (get_option('customWooStyleSPShowSKU') == 1 ) { echo checked; } ?>/></div>                            
           	  <div class="optionitem">Show Category</div>
           	  <div class="optionitem"><input name="customWooStyleSPShowCategory" type="checkbox" value="1" <?php if (get_option('customWooStyleSPShowCategory') == 1 ) { echo checked; } ?>/></div>                            
           	  <div class="optionitem" style="float:right; text-align:right;"><input type="submit" class="button-primary" name="submit" value="Save Options" /></div>

            </div>
            
            <div style="float:left; clear:both;"></div>
            
            <!-- Featured Products - Home -->   
            
            <div style="float:left; clear:both;"></div>
            <div class="optionheading">Featured Products - Home Page</div>
            <div class="optionbox">
           	  <div class="optionitem">Show Featured Products</div>
           	  <div class="optionitem"><input name="customWooStyleFeaturedHomeShow" type="checkbox" value="1" <?php if (get_option('customWooStyleFeaturedHomeShow') == 1 ) { echo checked; } ?>/></div>                            
           	  <div class="optionitem">No. of columns</div>
           	  <div class="optionitem"><input name="customWooStyleFeaturedHomeNoProducts" value="<?php echo (get_option('customWooStyleFeaturedHomeNoProducts'))?>" /></div>              

              <div class="clear"></div>
              
           	  <div class="optionitem">Featured - Font</div>
           	  <div class="optionitem"><input name="customWooStyleFeaturedHomeFontColour" class="colorwell "type="text" onfocus="if (this.value==''){this.value='#';}" onblur="if (this.value=='#'){ this.value='';}" id="customWooStyleFeaturedHomeFontColour" value="<?php echo (get_option('customWooStyleFeaturedHomeFontColour'))?>" /></div>              
              <div class="optionitem">
                  <select name="customWooStyleFeaturedHomeFontSize" id="customWooStyleFeaturedHomeFontSize">
                    <option value="">-- please select --</option>
                    <?php 
						
							$selected = "";
							$arrCount = count($custom_font_size);
							$checker = get_option('customWooStyleFeaturedHomeFontSize');
							for ($i = 0;$i < $arrCount;$i++){
								
								if ($checker == $i ) {
									$selected = "selected";
								}

						?>
                    <option <?php echo $selected; ?> style="font-size:<?php echo $custom_font_size[$i] ?>" value="<?php echo $i ?>"><?php echo $custom_font_size[$i] ?></option>
                    <?php	
								$selected = "";						
							} //end for
							unset($checker);
						?>
                  </select>              
              </div>
              <div class="clear"></div>              
           	  <div class="optionitem">Show Featured Short Desc.</div>
           	  <div class="optionitem"><input name="customWooStyleFeaturedHomeShowSortDesc" type="checkbox" value="1" <?php if (get_option('customWooStyleFeaturedHomeShowSortDesc') == 1 ) { echo checked; } ?>/></div>                            
           	  <div class="optionitem">Short Description Height</div>
           	  <div class="optionitem"><input name="customWooStyleFeaturedHomeSortDescHeight" value="<?php echo (get_option('customWooStyleFeaturedHomeSortDescHeight'))?>" /> px</div>              

              <div class="clear"></div>

           	  <div class="optionitem">Show Cart Button </div>
           	  <div class="optionitem"><input name="customWooStyleFeaturedHomeShowCartBtn" type="checkbox" value="1" <?php if (get_option('customWooStyleFeaturedHomeShowCartBtn') == 1 ) { echo checked; } ?>/></div>                            
           	  <div class="optionitem">Product box height</div>
           	  <div class="optionitem"><input name="customWooStyleFeaturedHomeProdBoxHeight" value="<?php echo (get_option('customWooStyleFeaturedHomeProdBoxHeight'))?>" /> px</div>              

              <div class="clear"></div>

           	  <div class="optionitem">Show Price </div>
           	  <div class="optionitem"><input name="customWooStyleFeaturedHomeShowPrice" type="checkbox" value="1" <?php if (get_option('customWooStyleFeaturedHomeShowPrice') == 1 ) { echo checked; } ?>/></div>                            
           	  <div class="optionitem">Price Colour</div>
           	  <div class="optionitem"><input name="customWooStyleFeaturedHomePriceColour" class="colorwell "type="text" onfocus="if (this.value==''){this.value='#';}" onblur="if (this.value=='#'){ this.value='';}" id="customWooStyleFeaturedHomePriceColour" value="<?php echo (get_option('customWooStyleFeaturedHomePriceColour'))?>" /></div>              

           	  <div class="optionitem" style="float:right; text-align:right;"><input type="submit" class="button-primary" name="submit" value="Save Options" /></div>

            </div>                      

            <!-- Related - Products Detail Page -->
               
            <div class="optionheading">Related / Upsell - Product Detail Page</div>
            <div class="optionbox">               
           	  <div class="optionitem">Show related products</div>
           	  <div class="optionitem"><input name="customWooStyleSPShowRelated" type="checkbox" value="1" <?php if (get_option('customWooStyleSPShowRelated') == 1 ) { echo checked; } ?>/></div>                            
           	  <div class="optionitem">No. of related products</div>
           	  <div class="optionitem"><input name="customWooStyleSPShowRelatedNoProducts" value="<?php echo (get_option('customWooStyleSPShowRelatedNoProducts'))?>" /></div>              

              <div class="clear"></div>

           	  <div class="optionitem">Related products - font</div>
           	  <div class="optionitem"><input name="customWooStyleSPShowRelatedColour" class="colorwell "type="text" onfocus="if (this.value==''){this.value='#';}" onblur="if (this.value=='#'){ this.value='';}" id="customWooStyleSPShowRelatedColour" value="<?php echo (get_option('customWooStyleSPShowRelatedColour'))?>" /></div>              
              <div class="optionitem">
                  <select name="customWooStyleSPShowRelatedFontSize" id="customWooStyleSPShowRelatedFontSize">
                    <option value="">-- please select --</option>
                    <?php 
						
							$selected = "";
							$arrCount = count($custom_font_size);
							$checker = get_option('customWooStyleSPShowRelatedFontSize');
							for ($i = 0;$i < $arrCount;$i++){
								
								if ($checker == $i ) {
									$selected = "selected";
								}

						?>
                    <option <?php echo $selected; ?> style="font-size:<?php echo $custom_font_size[$i] ?>" value="<?php echo $i ?>"><?php echo $custom_font_size[$i] ?></option>
                    <?php	
								$selected = "";						
							} //end for
							unset($checker);
						?>
                  </select>              
              </div>                                                     
                           
              <div class="clear"></div>              
           	  <div class="optionitem">Show Related Short Desc.</div>
           	  <div class="optionitem"><input name="customWooStyleSPShowRelatedShortDesc" type="checkbox" value="1" <?php if (get_option('customWooStyleSPShowRelatedShortDesc') == 1 ) { echo checked; } ?>/></div>                            
           	  <div class="optionitem">Short Description Height</div>
           	  <div class="optionitem"><input name="customWooStyleSPShowRelatedShortDescHeight" value="<?php echo (get_option('customWooStyleSPShowRelatedShortDescHeight'))?>" /> px</div>              

           	  <div class="optionitem" style="float:right; text-align:right;"><input type="submit" class="button-primary" name="submit" value="Save Options" /></div>
            </div>        
            
            <!-- Email template and subject settings -->
            <div class="optionheading">Email template and subject settings</div>
            <div class="optionbox">
           	  <div class="optionitem-wide">Subject - Customer: Customer Order Received</div>
           	  <div class="optionitem-wide"><input name="customWooEmailSubjectCustomerOrderReceived" value="<?php echo (get_option('customWooEmailSubjectCustomerOrderReceived'))?>" /></div>              

              <div class="clear"></div>

           	  <div class="optionitem-wide">Email Heading - Customer: Customer Order Received</div>
           	  <div class="optionitem-wide"><input name="customWooEmailHeadingCustomerOrderReceived" value="<?php echo (get_option('customWooEmailHeadingCustomerOrderReceived'))?>" /></div>              

              <div class="clear"></div>

           	  <div class="optionitem-wide">Subject - Admin: Customer Order Received</div>
           	  <div class="optionitem-wide"><input name="customWooEmailSubjectAdminOrderReceived" value="<?php echo (get_option('customWooEmailSubjectAdminOrderReceived'))?>" /></div>              

              <div class="clear"></div>

           	  <div class="optionitem-wide">Email Heading - Admin: Customer Order Received</div>
           	  <div class="optionitem-wide"><input name="customWooEmailHeadingAdminOrderReceived" value="<?php echo (get_option('customWooEmailHeadingAdminOrderReceived'))?>" /></div>              

              <div class="clear"></div>


           	  <div class="optionitem" style="float:right; text-align:right;"><input type="submit" class="button-primary" name="submit" value="Save Options" /></div>
            </div> <!-- /optionbox -->            
             <!-- /Email template and subject settings -->
               
            
            
			</form> 
</div>                       

<?php 
 
} // catalogue admin


function get_custom_woo_stylesheet() {
	switch (get_option('custom_woo_stylesheet')) {
		case "0":break;		
		case "1": echo '<link rel="stylesheet" type="text/css" media="all" href="'.get_template_directory_uri().'/woocommerce/woo-default.css" />'; break;
	}	
}

function get_custom_woo_styles() {

?>
<style type="text/css">
<?php
	switch (get_option('customWooStylePLcols')) {
		case "1": echo "ul.products li.product { width:95%; }"; break;		
		case "2": echo "ul.products li.product { width:45%; }"; break;		
		case "3": echo "ul.products li.product { width:28.5%; }"; break;
		case "4": echo "ul.products li.product { width:20%; }"; break;
		case "5": echo "ul.products li.product { width:15%; }"; break;			
	}
	

  if (get_option('customWooStylePLcols') == "1") {
  	echo "ul.products li a img  { width:35%; float:left; margin-right: 15px; }";
	echo "ul.products li.product h3 {display: inline;} ";
	echo "div.related ul.products li.product {width:28.5%; margin-right: 1.5%;} ";
	echo "div.related ul.products li.first {clear:none;} ";	
  }	  

  
  if (get_option('customWooShowBreadcrumb') != 1) { 
  	echo "div#breadcrumb { display:none; }";
  }

// ---------------- Product Catalog / Product List ---------------- 

  if (get_option('customWooStylePLShowOrderby') != 1) { 
  	echo "form.woocommerce_ordering { display:none; }";
  } 
  
  if (get_option('customWooStylePLShowPrice') != 1) { 
  	echo "ul.products li.product span.price { display:none; }";
  } 
  
  if (get_option('customWooStylePLShowButton') != 1) { 
  	echo "ul.products li.product a.button { display:none; }";
  } 
  
  if (get_option('customWooStylePLPriceColour')) { 
  	echo "ul.products li.product .price { color:".get_option('customWooStylePLPriceColour')."; }";
  } 
  if (get_option('customWooStylePLBoxHeight')) { 
  	echo "ul.products li.product { height:".get_option('customWooStylePLBoxHeight')."px; }";
  }  
  
  if (get_option('customWooStylePLShowShortDescription') != 1) { 
  	echo "ul.products li.product div.woo-short-desc { display:none; }";
  }  
  if (get_option('customWooStylePLShortDescriptionHeight')) { 
  	echo "ul.products li.product div.woo-short-desc { height:".get_option('customWooStylePLShortDescriptionHeight')."px; overflow:hidden;}";
  } 
  
  if (get_option('customWooStylePLFontColour')) { 
  	echo "ul.products li.product h3 { color:".get_option('customWooStylePLFontColour')."; }";
  } 
  
  if (get_option('customWooStylePLFontSize')) { 
  	echo "ul.products li.product h3, ul.products li.product h3 a { font-size:".getFontSize('customWooStylePLFontSize')."; }";
  }
  
  if (get_option('customWooStylePLShowSKU') != 1) { 
  	echo "li.product span.sku { display:none; }";
  } 
  
  
// ---------------- Single Product ---------------- 
  if (get_option('customWooStyleSPShowButton') != 1) { 
  	echo "div.product form.cart { display:none; }";
  } 
  if (get_option('customWooStyleSPShowPrice') != 1) { 
  	echo "div.product p.price { display:none; }";
  } 
  if (get_option('customWooStyleSPPriceColour')) { 
  	echo "div.product p.price { color:".get_option('customWooStyleSPPriceColour')."; }";
  } 
  if (get_option('customWooStyleSPShowSKU') != 1) { 
  	echo "div.product span.sku { display:none; }";
  } 
  if (get_option('customWooStyleSPShowCategory') != 1) { 
  	echo "div.product span.posted_in { display:none; }";
  } 
  if (get_option('customWooStyleSPShowCategory') != 1 && get_option('customWooStyleSPShowSKU') != 1) { 
  	echo "div.product div.product_meta { display:none; }";
  } 
  
  // ---------------- Related products ---------------- 
  
  if (get_option('customWooStyleSPShowRelated') != 1) { 
  	echo "div.related.products { display:none; }";
  }
  
  if (get_option('customWooStylePLcols') == "1") {  
	switch (get_option('customWooStyleSPShowRelatedNoProducts')) {
		case "1": echo "div.related ul.products li.product, div.upsells ul.products li.product  { width:95%; }"; break;		 
		case "2": echo "div.related ul.products li.product, div.upsells ul.products li.product  { width:45%; }"; break;		
		case "3": echo "div.related ul.products li.product, div.upsells ul.products li.product  { width:28.5%; }"; break;
		case "4": echo "div.related ul.products li.product, div.upsells ul.products li.product  { width:20%; }"; break;
		case "5": echo "div.related ul.products li.product, div.upsells ul.products li.product  { width:15%; }"; break;			
	} //switch
  } //end if
  
  if (get_option('customWooStyleSPShowRelatedColour')) { 
  	echo "div.related ul.products li.product h3 { color:".get_option('customWooStyleSPShowRelatedColour')."; }";
  	echo "div.upsells ul.products li.product h3 { color:".get_option('customWooStyleSPShowRelatedColour')."; }";
  } 
  
  if (get_option('customWooStyleSPShowRelatedFontSize')) { 
  	echo "div.related ul.products li.product h3, div.related ul.products li.product h3 a { font-size:".getFontSize('customWooStyleSPShowRelatedFontSize')."; }";
  	echo "div.upsells ul.products li.product h3, div.related ul.products li.product h3 a { font-size:".getFontSize('customWooStyleSPShowRelatedFontSize')."; }";	
  }  
  
  if (get_option('customWooStyleSPShowRelatedShortDesc') != 1) { 
  	echo "div.related ul.products li.product div.woo-short-desc { display:none; }";
  	echo "div.upsells ul.products li.product div.woo-short-desc { display:none; }";
	
  }  
  if (get_option('customWooStyleSPShowRelatedShortDescHeight')) { 
  	echo "div.related ul.products li.product div.woo-short-desc { height:".get_option('customWooStyleSPShowRelatedShortDescHeight')."px; overflow:hidden;}";
  	echo "div.upsells ul.products li.product div.woo-short-desc { height:".get_option('customWooStyleSPShowRelatedShortDescHeight')."px; overflow:hidden;}";	
  } 
  
  // ---------------- Featured products on Home Page ---------------- 
  
  if (get_option('customWooStyleFeaturedHomeShow') != 1) { 
  	echo "body.home ul.products { display:none; }";
  } 
  
  if (get_option('customWooStyleFeaturedHomeNoProducts')) { 
  echo "body.home ul.products li.first { clear:none;}";
  echo "body.home ul.products li:nth-child(".get_option('customWooStyleFeaturedHomeNoProducts')."n+1) { clear:left;}";
  echo "body.home ul.products li.last, body.home ul.products li.product {margin-right: 1.5%}";
	switch (get_option('customWooStyleFeaturedHomeNoProducts')) {
		case "1": echo "body.home ul.products li.product { width:95%; }"; break;		 
		case "2": echo "body.home ul.products li.product { width:45%; }"; break;		
		case "3": echo "body.home ul.products li.product { width:28.5%; }"; break;
		case "4": echo "body.home ul.products li.product { width:20%; }"; break;
		case "5": echo "body.home ul.products li.product  { width:15%; }"; break;			
	} //switch
  } //end if  
  
  if (get_option('customWooStyleFeaturedHomeFontColour')) { 
  	echo "body.home ul.products li.product h3 { color:".get_option('customWooStyleFeaturedHomeFontColour')."; }";
  } 
  
  if (get_option('customWooStyleFeaturedHomeFontSize')) { 
  	echo "body.home ul.products li.product h3, body.home ul.products li.product h3 a { font-size:".getFontSize('customWooStyleFeaturedHomeFontSize')."; }";
  }  
  
  if (get_option('customWooStyleFeaturedHomeShowSortDesc') != 1) { 
  	echo "body.home ul.products li.product div.woo-short-desc { display:none; }";
  }  else {
  	echo "body.home ul.products li.product div.woo-short-desc { display:block; }";	  
  }
  if (get_option('customWooStyleFeaturedHomeSortDescHeight')) { 
  	echo "body.home ul.products li.product div.woo-short-desc { height:".get_option('customWooStyleFeaturedHomeSortDescHeight')."px; overflow:hidden;}";
  } 
  
  if (get_option('customWooStyleFeaturedHomeShowCartBtn') != 1) { 
  	echo "body.home ul.products li.product a.button { display:none; }";
  } 
  
  if (get_option('customWooStyleFeaturedHomeProdBoxHeight')) { 
  	echo "body.home ul.products li.product { height:".get_option('customWooStyleFeaturedHomeProdBoxHeight')."px; }";
  }  
  
  if (get_option('customWooStyleFeaturedHomeShowPrice') != 1) { 
  	echo "body.home ul.products li.product span.price { display:none; }";
  } 

  if (get_option('customWooStyleFeaturedHomePriceColour')) { 
  	echo "body.home ul.products li.product span.price{ color:".get_option('customWooStyleFeaturedHomePriceColour')."; }";
  } 

  
 ?>   
</style>	
<?php	
}  // end get_custom_woo_styles



?>