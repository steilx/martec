<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'martec_dev');

/** MySQL database username */
define('DB_USER', '281268363cd6');

/** MySQL database password */
define('DB_PASSWORD', 'c017a4ea31fe70b1');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'yTgMA~WCX|awcyo.>FH,*sh)xsH<<wv>l%_(pB^&c?=-/]M`v!Lc@p#R`;9q! (u');
define('SECURE_AUTH_KEY',  'U7FSfAs7*c:a*z3t9lO%)t>}~W^EA|y Gtw&kF>X=b_&!$0}&n5x5~+Fn*|56Em=');
define('LOGGED_IN_KEY',    'VOv8+]);g$9mZS-6+}T3+k:sUt/m#v`@|&<59+/`stJl$C?<M$ wAflQK2YouV[W');
define('NONCE_KEY',        '24~<pcwa-_04rm(O8_$|]|4AgVER}o0~=gUWHA#w0I(QQ3tW-2L*BUG>!s,:d`k3');
define('AUTH_SALT',        '6y=o/L#_B3^O+2+1P3<`Z5su **nF<6TzF7!D+i(BZKtZQR3aj|Y]H0&t ?Dhn>T');
define('SECURE_AUTH_SALT', '0[)bN11W^xAgCRa(QXXs!nSLg,&)=9A5jG3t26!9`,u]Jmy;PGn^ qkaf>B||FG}');
define('LOGGED_IN_SALT',   'Tj2gE~:-I~O_4bd,;ishw0<A@zcbI1`DO_91js|RK+{ks}+8bjXm/03yXKZK}?m]');
define('NONCE_SALT',       '%hqTV)0)+nIe1r%wS r;hd9Y)MS{[G+K3*:lus|Q8l2wR6$Y6E|1[`i/ZII(Oi,0');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
